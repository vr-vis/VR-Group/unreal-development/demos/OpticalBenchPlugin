// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"

#include "OptiXLensComponent.h"

#include "OptiXLensActor.generated.h"

/**
 * 
 */
UCLASS()
class OPTIX_API AOptiXLensActor : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	AOptiXLensActor(const FObjectInitializer& ObjectInitializer);

	//virtual void BeginPlay() override;
	//virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiX")
	UOptiXLensComponent* OptiXLensComponent;
};

