// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "OptiXGlassDefinitions.h"

#include "OptiXGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class OPTIX_API AOptiXGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOptiXGameModeBase(const FObjectInitializer& ObjectInitializer);

	virtual void InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<FSceneData>& GetOptiXSceneDataArray();
};
