#pragma once

#include "PickupActorInterface.generated.h"

UINTERFACE(BlueprintType)
class OPTIX_API UNewPickupActorInterface : public UInterface
{
	GENERATED_BODY()
};

class OPTIX_API INewPickupActorInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OptiXActor")
	void Pickup(USceneComponent* AttachTo);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "OptiXActor")
	void Drop();
};