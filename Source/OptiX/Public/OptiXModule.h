#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

#include "OptiXContextManager.h"
#include "OptiXGlassDefinitions.h"


DECLARE_LOG_CATEGORY_EXTERN(OptiXPlugin, Log, All);


class OPTIX_API FOptiXModule : public IModuleInterface
{
public:

	// Trying something alongside the Flex implementation, I have no clue what this could be used for for now.
	// If this works, I should move the Context from the singleton away somehow into the scene to make it accessible via blueprints etc.
	// Maybe have 1 context per level? That would make most sense and allow for different scenes in a world, not that this is needed anyway.
	// But the singleton thing seems really limiting.
	static FOptiXModule& Get();

	FOptiXContextManager* GetOptiXContextManager();

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("OptiX");
	}

	/* IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	FString GetDir();

	void Init();

	TMap<FString, FGlassDefinition>& GetGlassDefinitions()
	{
		return GlassDefinitionsMap;
	}

	const TArray<FSceneData>& GetSceneDataArray()
	{
		return SceneDataArray;
	}

public:
	FString OptiXPTXDir;

private:


	//TWeakObjectPtr<UOptiXContext> RenderThreadOptiXContext;

	// This is only kept so we can destroy the context LAST on module shutdown, not somewhere inbetween.
	//optix::Context NativeContext;
	
	void LoadDLLs();
	void UnloadDLLs();

	void LoadGlassDefinitions();
	void LoadSceneData();

	void* CudaRtHandle;
	void* OptixHandle;
	void* OptixUHandle;

	// Pointer probably doesn't need to be thread safe in this case, check this
	TSharedPtr<FOptiXContextManager, ESPMode::ThreadSafe> OptiXContextManager;

	TMap<FString, FGlassDefinition> GlassDefinitionsMap;

	TArray<FSceneData> SceneDataArray;

	static FOptiXModule* Singleton;
};