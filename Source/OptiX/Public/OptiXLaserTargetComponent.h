// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OptiXObjectComponent.h"
#include "OptiXLaserTargetComponent.generated.h"

/**
 *  
 */
UCLASS(Blueprintable, hidecategories = (Object), meta = (BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXLaserTargetComponent : public UOptiXObjectComponent
{
	GENERATED_BODY()

public:
	UOptiXLaserTargetComponent(const FObjectInitializer& ObjectInitializer);
	
	virtual void BeginPlay() override;

	/*OptiXObjectComponent Interface End*/

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	//float GetMaxFromBuffer();

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	//void UpdateBufferData();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void ClearOptiXBuffer();


public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "OptiX")
	bool bIsColorMode;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiX")
	float TargetRes;

	// Think about this later a bit - is there even a reason to save it like this?
	// It seems much easier to have optix write the already correct colors in the buffer and then just do a FMemory:MemCpy?
	TArray<FColor, TFixedAllocator<512 * 512>> ColorData; // Needs to be FColor so we can do an easy mem copy

	float HighlightColorValue = 0.8f;

	FColor HighlightColor;

	TArray<FColor, TFixedAllocator<256>> ColorLUT;
	UTexture2D* LUTTexture;
};
