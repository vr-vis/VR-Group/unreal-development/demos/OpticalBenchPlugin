#pragma once

#include "CoreMinimal.h"
#include "EngineUtils.h"

#include "Runtime/Engine/Public/SceneViewExtension.h"

#include "Runtime/Engine/Classes/Engine/Texture2D.h"

#include "MaterialShared.h"
#include "Materials/MaterialInstance.h"
#include "Delegates/Delegate.h"

//#include "OptiXContext.h"
#include "OptiXIncludes.h"

#include "OptiXObjectComponent.h"
#include "OptiXLaserComponent.h"
#include "OptiXLaserActor.h"
#include "OptiXCameraActor.h"
#include "StatsDefines.h"

DECLARE_LOG_CATEGORY_EXTERN(OptiXContextManagerLog, Log, All);

// Let's try some events!

DECLARE_EVENT(FOptiXContextManager, FLaserTraceFinishedEvent)
DECLARE_EVENT_OneParam(FOptiXContextManager, FWavelengthChangedEvent, const float)
DECLARE_MULTICAST_DELEGATE(FOnSceneChangedDelegate);

// DX

#if PLATFORM_WINDOWS
#include "AllowWindowsPlatformTypes.h"
#endif
#include <d3d11.h>
#if PLATFORM_WINDOWS
#include "HideWindowsPlatformTypes.h"
#endif

/**
* Cuda error print function. Polls the latest cuda error and logs it. 
*/
inline void PrintLastCudaError(FString Msg)
{
	cudaError_t Err = cudaGetLastError();
	if (cudaSuccess != Err) {
		UE_LOG(LogTemp, Fatal, TEXT("Cuda Error: %s. "), *Msg, static_cast<int>(Err), cudaGetErrorString(Err));
	}
}

/**
* Struct storing the optix objects that typically form an unreal optix object component.
*/
struct FOptiXObjectData
{
	optix::Acceleration			OptiXAcceleration;
	optix::Geometry				OptiXGeometry;
	optix::GeometryGroup		OptiXGeometryGroup;
	optix::GeometryInstance		OptiXGeometryInstance;
	optix::Transform			OptiXTransform;
	optix::Material				OptiXMaterial;
	/**
	* First and second program correspond to perspective and iterative trace for the optix geometry, while the third and fourth correspond to perspective and iterative
	* programs for the optix material. Anyhit programs need to be added differently.
	*/
	TTuple<optix::Program, optix::Program, optix::Program, optix::Program>	OptiXPrograms;
};

/**
* Struct storing the data needed to create an optix buffer. Used to prevent overlong constructors.
*/
struct FOptiXBufferData
{
	FString Name;
	unsigned int Type;
	RTformat Format;
	RTsize BufferWidth = 0;
	RTsize BufferHeight = 0;
	RTsize BufferDepth = 0;
};

/**
* Typdef for the callback function to update corresponding optix variables from a game-thread owned UObject in the scene.
*/
typedef TFunction<void(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)> OptiXObjectUpdateFunction;

/**
* Typdef for the callback function to update corresponding optix variables from a game-thread owned UObject in the scene. Additionally contains the RHICmdList as a parameter
* so that textures and materials can be updated directly.
*/
typedef TFunction<void(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler, FRHICommandListImmediate & RHICmdList)> OptiXObjectUpdateFunctionRHI;

/**
* Typdef for the callback function to initialize optix objects from the corresponding gamethread UObject.
*/
typedef TFunction<void(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)> OptiXObjectInitFunction;

/**
* Typdef for the callback function to update a cubemap for a lens. Additionally contains the RHICmdList as a parameter
* so that textures and materials can be updated directly.
*/
typedef TFunction<void(optix::Buffer CubemapBuffer, FRHICommandListImmediate& RHICmdList)> CubemapUpdateFunction;

/**
* Typdef for the callback function to update general optix context variables.
*/
typedef TFunction<void(optix::Context Context)> OptiXContextUpdateFunction;

/**
* Typdef for the callback function executed after the laser trace finished.
*/
typedef TFunction<void(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler, FRHICommandListImmediate & RHICmdList)> LaserTraceFinishedCallback;

/**
* Unique Id for a UObject in the scene. Used to map a UObject to a corresponding set of optix objects.
*/
typedef uint32 UniqueId;


class OPTIX_API FOptiXContextUpdateManager : public FSceneViewExtensionBase
{

public:

	// The auto register parameter is used to make sure this constructor is only called via the NewExtension function
	FOptiXContextUpdateManager(const FAutoRegister& AutoRegister);

	virtual void SetupViewFamily(FSceneViewFamily& InViewFamily) override;
	virtual void SetupView(FSceneViewFamily& InViewFamily, FSceneView& InView) override;
	virtual void BeginRenderViewFamily(FSceneViewFamily& InViewFamily) override;
	virtual void PreRenderView_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneView& InView) override;
	virtual void PreRenderViewFamily_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily) override;
	virtual bool IsActiveThisFrame(class FViewport* InViewport) const override;
	virtual int32 GetPriority() const override;

	// ISceneViewExtension interface end
	bool bIsActive = false;
};



class OPTIX_API FOptiXContextManager : public FSceneViewExtensionBase
{

public:

	// The auto register parameter is used to make sure this constructor is only called via the NewExtension function
	FOptiXContextManager(const FAutoRegister& AutoRegister);

	~FOptiXContextManager() 
	{
		// TODO
	}

	// ISceneViewExtension interface start, called by the render thread:
public:
	virtual void SetupViewFamily(FSceneViewFamily& InViewFamily) override;
	virtual void SetupView(FSceneViewFamily& InViewFamily, FSceneView& InView) override;
	virtual void BeginRenderViewFamily(FSceneViewFamily& InViewFamily) override;
	virtual void PreRenderView_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneView& InView) override;
	virtual void PreRenderViewFamily_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily) override;
	virtual void PostRenderViewFamily_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily) override;
	virtual bool IsActiveThisFrame(class FViewport* InViewport) const override;
	virtual void PostRenderView_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneView& InView) override;
	virtual int32 GetPriority() const override;

	// ISceneViewExtension interface end

	void RenderOrthoPass();
	
	// Initialization methods, called by the GAME thread	
	void Init();

	void EndPlay()
	{
		bEndPlayReceived.AtomicSet(true);
		Cleanup_GameThread();
	}

	UMaterialInstanceDynamic* GetOptiXMID() // Used to set up the post process
	{
		return DynamicMaterial.Get();
	}

	UMaterialInstanceDynamic* GetOptiXMIDOrtho() // Used to set up the post process
	{
		return DynamicMaterialOrtho.Get();
	}

	void SceneChangedCallback();


	void SetActiveLaserActor(AOptiXLaserActor* Laser)
	{
		LaserActor = Laser;
		if (LaserMaterialDynamic.IsValid())
		{
			LaserActor->SetLaserMaterial(LaserMaterialDynamic.Get());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("LaserMaterialDynamic is invalid!"));
		}
	}

	void SetActiveCameraActor(AOptiXPlayerCameraManager* Cam)
	{
		CameraActor = Cam;
	}

	FIntRect GetViewRectanglePerEye()
	{
		return FIntRect(0, 0, Width, Height);
	}

	int32 RequestCubemapId();

	void DeleteCubemapId(int32 Id);

	void AddCubemapToBuffer(int32 CubemapId, int32 SamplerId);

	void BroadcastWavelengthChange(float WL)
	{
		WavelengthChangedEvent.Broadcast(WL);
		//UE_LOG(LogTemp, Error, TEXT("LaserMaterialDynamic is invalid!"));
	}

	void ExecuteOptiXUpdate_PreLateUpdate(FRHICommandListImmediate & RHICmdList);

public:
	
	FThreadSafeBool bEndPlayReceived = false;

	FThreadSafeBool bValidCubemap = false;
	FThreadSafeBool bIsInitializedCuda = false;
	FThreadSafeBool bIsInitializedLaser = false;
	FThreadSafeBool bIsInitializedAll = false;

	FThreadSafeBool bSceneChanged = true;
	FThreadSafeBool bRequestOrthoPass = false;

	//FThreadSafeBool bStartTracing = false;
	//FThreadSafeBool bIsInitialized = false;
	//FThreadSafeBool bIsTracing = false;
	//FThreadSafeBool bClearToLaunch = true;
	//FThreadSafeBool bCleanup = false;
	//FThreadSafeBool bRequestOrthoPass = false;
	//FThreadSafeBool bEndPlay = false;

	/**
	* Delegate that broadcasts every time the laser trace finishes. Required to update the detector texture.
	*/
	FLaserTraceFinishedEvent LaserTraceFinishedEvent;

	/**
	* Delegate that broadcasts every time the wavelength changes. Required for recomputation of the refractive indices of the lenses. 
	*/
	FWavelengthChangedEvent WavelengthChangedEvent;

	/**
	* Delegate that broadcasts every time the scene changes, e.g. when a lens is moved. This should imply a new laser trace.
	*/
	FOnSceneChangedDelegate OnSceneChangedDelegate;

	/**
	* Path to the pre-compiled ptx files that contains the optix shaders.
	*/
	FString OptiXPTXDir;


public:	

	FMatrix OrthoMatrix;

private:
	void InitContext();
	void InitRendering();
	void InitBuffers();
	void InitPrograms();
	void InitLaser();

	void LaunchLaser(FRHICommandListImmediate & RHICmdList);

	void LaunchStandardTrace(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily);

	void LaunchMonoscopicTrace(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily);

	void CopyLaserCudaTexture(FRHICommandListImmediate & RHICmdList);

	void InitCubemap();

	void CleanupCuda();

	void CleanupLocalOptiXObjects();

	void Cleanup_RenderThread();

	void Cleanup_GameThread();

	void UpdateCubemapBuffer(FRHICommandListImmediate & RHICmdList);
	
private:

	TSharedPtr<FOptiXContextUpdateManager, ESPMode::ThreadSafe> OptiXContextUpdateManager;

	TWeakObjectPtr<AOptiXLaserActor> LaserActor;

	TWeakObjectPtr<AOptiXPlayerCameraManager> CameraActor;

	TQueue<int32> UnallocatedCubemapIds;

	TArray<TArray<FColor>> SurfaceDataCube;

	/**
	* Viewport width and height
	*/
	int32 Width;
	int32 Height;

	/**
	* Right eye color texture used for post processing. Cuda copies the optix results into this texture.
	*/
	TWeakObjectPtr<UTexture2D> OutputTexture;
	/**
	* Right eye depth texture used for post processing. Cuda copies the optix results into this texture.
	*/
	TWeakObjectPtr<UTexture2D> DepthTexture;

	/**
	* Left eye color texture used for post processing. Cuda copies the optix results into this texture.
	*/
	TWeakObjectPtr<UTexture2D> OutputTexture2;
	/**
	* Left eye depth texture used for post processing. Cuda copies the optix results into this texture.
	*/
	TWeakObjectPtr<UTexture2D> DepthTexture2;

	/**
	* Ortho pass texture used for post processing. Cuda copies the optix results into this texture.
	* The ortho pass is used for single eye orthogonal rendering.
	*/
	TWeakObjectPtr<UTexture2D> OutputTextureOrtho;
	TWeakObjectPtr<UTexture2D> DepthTextureOrtho;

	/**
	* Pointers to the post processing materials used to blend the optix results with the unreal scene.
	*/
	TWeakObjectPtr<UMaterialInstanceDynamic> DynamicMaterial;
	TWeakObjectPtr<UMaterialInstanceDynamic> DynamicMaterialOrtho;
	TWeakObjectPtr<UMaterial> MonoMaterial;
	TWeakObjectPtr<UMaterial> VRMaterial;
	bool bWithHMD;


	// ---------------------------------------------------------------------------------------------------
	// Laser stuff
	// ---------------------------------------------------------------------------------------------------

	TWeakObjectPtr<UTexture2D> LaserIntersectionTexture;
	TWeakObjectPtr<UMaterial> LaserMaterial;
	TWeakObjectPtr<UMaterialInstanceDynamic> LaserMaterialDynamic;




public:

	TQueue<TPair<uint32, TArray<FVector>>> LaserIntersectionQueue;

	TArray<TArray<FVector>> PreviousLaserResults;


private:

	TArray<FVector4> IntersectionData;
	TArray<FVector4> OldIntersectionData;

	FThreadSafeBool bTracingLaser;

	int32 LaserMaxDepth;
	int32 LaserEntryPoint;
	int32 LaserBufferSize;

	int32 LaserBufferWidth;
	int32 LaserBufferHeight;


	// ---------------------------------------------------------------------------------------------------
	// DX <-> CUDA stuff
	// ---------------------------------------------------------------------------------------------------

private:
	/**
	* Initialization function for Cuda. Needs to be called from the gamethread.
	*/
	void InitCUDADX();

	/**
	* Cuda graphic resources associated with the corresponding unreal textures.
	*/
	cudaGraphicsResource* CudaResourceDepthLeft;
	cudaGraphicsResource* CudaResourceDepthRight;
	cudaGraphicsResource* CudaResourceColorLeft;
	cudaGraphicsResource* CudaResourceColorRight;
	cudaGraphicsResource* CudaResourceIntersections;
	cudaGraphicsResource* CudaResourceColorOrtho;
	cudaGraphicsResource* CudaResourceDepthOrtho;

	/**
	* Cuda memory allocated and used to copy the optix results into the direct x textures.
	*/
	float* CudaLinearMemoryDepth;
	float4* CudaLinearMemoryColor;
	void* CudaLinearMemoryIntersections;

	size_t Pitch; // fix me
	size_t PitchLaser;

	/**
	* Cuda resource array used for easy mapping. Duplicate to above direct pointers.
	*/
	cudaGraphicsResource *Resources[7];

	// Refactor stuff

public:

	/*
	* Request functions used by UObjects to request the creation/destruction of the corresponding optix object.
	* The objects are stored here as accessing them directly is not threadsafe. 
	*/

	////////////////////////////////////////////////////////////////
	//  Request functions used by UObjects to request the creation/destruction of the corresponding optix object.
	//	The objects are stored here as accessing them directly is not threadsafe.
	////////////////////////////////////////////////////////////////

	/**
	* Requests a new optix::Buffer object to be created and associated with the calling object's Id. 
	*
	* @param ObjectId - The unique object Id this buffer should be associated with.
	* @param BufferData - The FOptiXBufferData storing the parameters with which the buffer should be initialized.
	*/
	void RequestNewOptiXBuffer(UniqueId ObjectId, FOptiXBufferData BufferData);

	/**
	* Requests a new optix::Group object to be created and associated with the calling object's Id.
	*
	* @param ObjectId - The unique object Id this Group should be associated with.
	* @warning Not Implemented yet!
	*/
	void RequestNewOptiXGroup(UniqueId ObjectId);

	/**
	* Requests a new optix::TextureSampler object to be created and associated with the calling object's Id.
	*
	* @param ObjectId - The unique object Id this TextureSampler should be associated with.
	*/
	void RequestNewOptiXTextureSampler(UniqueId ObjectId);

	/**
	* Requests the creation of a fully usable optix object used for targets and lenses. 
	* The object consists of a optix::Geometry and optix::Material with respective perspective and iterative programs.
	* Geometry and Material are combined in an optix::GeometryInstance which is added to a optix::GeometryGroup.
	* A optix::Transform is created for the GeometryGroup and attached to the top object. This function only requests
	* the creation of this object, which will then be executed on the next render thread tick.
	*
	* @param ObjectId - The unique object Id these objects should be associated with.
	* @param Program - The name of the programs associated with the Geometry and Material. 
	*/
	void RequestNewOptiXObject(UniqueId ObjectId, FString Program);

	/**
	* Requests the destruction of all optix objects associated with the Id. 
	*
	* @param ObjectId - The unique object Id.
	*/
	void RequestDestroyOptiXObjects(UniqueId ObjectId);


	////////////////////////////////////////////////////////////////
	//   Enqueue functions used by UObjects to enqueue an update lambda function that is then executed by the rendering thread for threadsafety.
	////////////////////////////////////////////////////////////////

	/**
	* Requests and enqueues an update of the cubemap for a lens. 
	*
	* @param ObjectId - The unique object Id for the lens which needs to be updated.
	* @param UpdateFunction - The callback function that is called from the render thread to execute the cubemap update.
	*/
	void RequestLensCubemapUpdate(UniqueId ObjectId, CubemapUpdateFunction UpdateFunction);

	/**
	* Requests and enqueues the initialization of an optix object component.
	*
	* @param ObjectId - The unique object Id for the object component which needs to be initialized.
	* @param InitFuntion - The callback function that is called from the render thread to execute the initialization.
	*/
	void EnqueueInitFunction(UniqueId ObjectId, OptiXObjectInitFunction InitFuntion);

	/**
	* Requests and enqueues the update of an optix object.
	*
	* @param ObjectId - The unique object Id for the object requesting the update.
	* @param UpdateFunction - The callback function that is called from the render thread to execute the update.
	*/
	void EnqueueUpdateFunction(UniqueId ObjectId, OptiXObjectUpdateFunction UpdateFunction);

	/**
	* Requests and enqueues a general optix context update.
	*
	* @param UpdateFunction - The callback function that is called from the render thread to execute the update.
	*/
	void EnqueueContextUpdateFunction(OptiXContextUpdateFunction UpdateFunction);

	/**
	* Requests and enqueues the update of an optix object. The update function gets called with the RHICmdList for texture manipulation.
	*
	* @param ObjectId - The unique object Id for the object requesting the update.
	* @param UpdateFunction - The callback function that is called from the render thread with the RHICmdList to execute the update.
	*/
	void EnqueueUpdateFunctionRHI(UniqueId ObjectId, OptiXObjectUpdateFunctionRHI UpdateFunction);

	/**
	* Registers a callback to be called each time after a laser trace has finished
	*
	* @param ObjectId - The unique object Id for the object registering the callback.
	* @param Callback - The callback function that is called from the render thread with the RHICmdList and respective laser buffers.
	*/
	void RegisterLaserTraceCallback(UniqueId ObjectId, LaserTraceFinishedCallback Callback);

	/**
	* Updates the laser position/orientations variables that get modified by the controller late update in case the laser is attached to the motion controller
	*
	* @param LateUpdateTransform - The newly updated transform.
	*/
	void LaserPositionLateUpdate_RenderThread(const FTransform LateUpdateTransform);

	/**
	* Updates the laser position/orientations variables that get modified by the controller late update in case the laser is attached to the motion controller
	*
	* @param LateUpdateTransform - The newly updated transform.
	*/
	void ObjectPositionLateUpdate_RenderThread(UniqueId ObjectId, const FMatrix LateUpdateTransform);

private:

	/*
	Creation/Destruction/Update functions that are called by the context manager if a UObject previously requested them. 
	*/
	void CreateNewOptiXObject(UniqueId ObjectId, FString Program);
	void CreateNewOptiXBuffer(UniqueId ObjectId, FOptiXBufferData BufferData);
	void CreateNewOptiXTextureSampler(UniqueId ObjectId);
	void DestroyOptiXObjects(UniqueId ObjectId);

	void ExecuteOptiXUpdate(UniqueId ObjectId, OptiXObjectUpdateFunction UpdateFunction);
	void ExecuteOptiXUpdateRHI(UniqueId ObjectId, FRHICommandListImmediate& RHICmdList, OptiXObjectUpdateFunctionRHI UpdateFunction);
	void ExecuteContextUpdate(OptiXContextUpdateFunction UpdateFunction);
	   
	void InitializeOptiXObject(UniqueId ObjectId, OptiXObjectInitFunction InitFunction);

	void UpdateCubemap(UniqueId ObjectId, FRHICommandListImmediate & RHICmdList, CubemapUpdateFunction UpdateFunction);

	/**
	* Functions that parse the respective queues to see if any request has been queued by a game-thread UObject. If there are any in the queues, 
	* the respective Creation/Destruction/Update functions are executed.
	*/
	void ParseCreationQueue();
	void ParseInitQueue();
	void ParseUpdateQueue(FRHICommandListImmediate & RHICmdList);
	void ParseDestroyQueue();
	void ParseCubemapUpdateQueue(FRHICommandListImmediate & RHICmdList);


private:
	/**
	* Queues to guarantee thread-safety between the optix native objects and the corresponding UObjects that represent them in the unreal scene. 
	*/
	TQueue<UniqueId>					OptiXObjectsToDestroy;
	TQueue<TPair<UniqueId, FString>>	OptiXObjectsToCreate;
	TQueue<TPair<UniqueId, OptiXObjectInitFunction>> OptiXObjectInitFunctions;
	TQueue<TPair<UniqueId, OptiXObjectUpdateFunction>>	OptiXObjectUpdateFunctions;
	TQueue<TPair<UniqueId, OptiXObjectUpdateFunctionRHI>>	OptiXObjectUpdateFunctionsRHI;

	TQueue<TPair<UniqueId, FOptiXBufferData>>	OptiXBuffersToCreate;
	TQueue<UniqueId>	OptiXTextureSamplersToCreate;
	TQueue<TPair<UniqueId, CubemapUpdateFunction>>	CubemapsToUpdate;
	TQueue<OptiXContextUpdateFunction>	OptiXContextUpdateQueue;

	/**
	* Map storing callbacks that are executed as soon as the laser trace finishes.
	* Could probably be used via delegates/events as well.
	*/
	TMap<UniqueId, LaserTraceFinishedCallback> LaserTraceFinishedCallbacks;


	////////////////////////////////////////////////////////////////
	//   OptiX API objects
	////////////////////////////////////////////////////////////////

	/**
	* Maps which store the optix objects mapped to a unique object id as keys.
	* Currently, multiple named buffers can be associated with one UObject, while only one group, sampler and object collection is supported.
	*/
	TMap<UniqueId, TMap<FString, optix::Buffer>>	OptiXBuffers;
	TMap<UniqueId, optix::Group>					OptiXGroups;
	TMap<UniqueId, optix::TextureSampler>			OptiXTextureSamplers;
	TMap<UniqueId, FOptiXObjectData>				OptiXObjectData;

	// Context Manager local objects

	/**
	* The optix context is the core handle on the whole optix framework. It's used to create and manipulate all other objects and
	* is the main entry point into the optix trace.
	*/
	optix::Context NativeContext;

	/**
	* The ray generation program is the main optix device shader program. 
	* It generates the rays and executes the trace, writing the result in the respective buffers.
	*/
	optix::Program RayGenerationProgram;

	/**
	* The miss program is executed on the device in case a ray misses any optix object. Generally, a cubemap is used to look up
	* environment data.
	*/
	optix::Program MissProgram;

	/**
	* The exception program is executed when optix exceptions are enabled and is used only for debugging. Printing needs to be explicitly enabled as well
	* to actually print the exceptions. 
	*
	* @warning The rtPrints executed by optix will NOT print to UE_LOG! They will be streamed to the standard cout, which requires executing the unreal executable
	* via the command line!
	*/
	optix::Program ExceptionProgram;

	/**
	* The top group of the optix scenegraph. This is the group that will be traced on, so all other scene objects need to be children.
	*/
	optix::Group TopObject;

	/**
	* The main acceleration of the top-level scenegraph. Each group can have a custom acceleration, this is the initial acceleration structure.
	*/
	optix::Acceleration TopAcceleration;

	/**
	* The main output buffer for color values. OptiX writes the color result into the first 3 float components, followed by a 4th component denoting the hit value.
	* If nothing has been hit for a given pixel, the post process material picks the unreal scene layer. 
	*/
	optix::Buffer OutputBuffer;

	/**
	* The main output buffer for the depth value, storing the depth as a single float per pixel.
	*/
	optix::Buffer OutputDepthBuffer;

	// Cubemap stuff

	/**
	* ContextManager local optix buffer and sampler that handle the initial cubemap. 
	*/
	optix::TextureSampler	CubemapSampler;
	optix::Buffer			CubemapBuffer;

	/**
	* optix buffer storing the texture sampler ids (integers) corresponding to a certain lens. 
	* The number of cubemaps is limited to 10, so ids have to be recycled. The ids are used as indices to the buffer
	* for quick and easy access in the optix shaders.
	*/
	optix::Buffer			CubemapsInputBuffer;


	// Laser Part

	optix::Buffer  LaserOutputBuffer;
	optix::Program LaserRayGenerationProgram;
	optix::Program LaserMissProgram;
	optix::Program LaserExceptionProgram;
};