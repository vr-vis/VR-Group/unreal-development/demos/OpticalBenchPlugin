#pragma once

//#define MALLOC_LEAKDETECTION 1
//#include "MallocLeakDetection.h"

THIRD_PARTY_INCLUDES_START

#if PLATFORM_WINDOWS
#include "AllowWindowsPlatformTypes.h"
#endif

#include "CUDALibrary/include/cuda_d3d11_interop.h"
#include "CUDALibrary/include/cuda.h"
#include "CUDALibrary/include/cuda_runtime_api.h"

#include "optix.h"
#include "optix_world.h"

#if PLATFORM_WINDOWS
#include "HideWindowsPlatformTypes.h"
#endif
THIRD_PARTY_INCLUDES_END