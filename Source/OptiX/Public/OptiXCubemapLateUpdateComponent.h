// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h"
#include "OptiXContextManager.h"

#include "OptiXCubemapLateUpdateComponent.generated.h"

/**
 * Literally only here to update the lenses on applylateupdate, as the scene capture component is a USceneCompoenent and NOT 
 * a UPrimitiveComponent and can therefore not receive the lateupdate. This is incredibly annoying. 
 */
UCLASS(Blueprintable, hidecategories = (Object), meta = (BlueprintSpawnableComponent))
class OPTIX_API UOptiXCubemapLateUpdateComponent : public UPrimitiveComponent
{
	GENERATED_BODY()
	
public:

	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXCubemapLateUpdateComponent")
	void LinkOptiXComponent(UOptiXCubemapComponent* OptiXComponent);

	//UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXCubemapLateUpdateComponent")
	const uint32* GetOptiXComponentId() const
	{
		return &OptiXComponentId;
	}

	uint32 OptiXComponentId;
};
