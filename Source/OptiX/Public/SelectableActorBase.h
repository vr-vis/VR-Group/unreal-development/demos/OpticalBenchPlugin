// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Actor.h"

#include "Components/WidgetComponent.h"
#include "OptiXVRPawn.h"

#include "SelectableActorBase.generated.h"

/**
 * 
 */
UCLASS(hidecategories = (Input), showcategories = ("Input|MouseInput", "Input|TouchInput"), ConversionRoot, ComponentWrapperClass, meta = (ChildCanTick))
class OPTIX_API ASelectableActorBase : public AActor
{
	GENERATED_BODY()

public:

	ASelectableActorBase(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	virtual void Serialize(FArchive& Ar) override;

	/** Returns StaticMeshComponent subobject **/
	class UStaticMeshComponent* GetStaticMeshComponent() const { return StaticMeshComponent; }

	/** Function to change mobility type */
	UFUNCTION(BlueprintCallable, Category = Mobility)
	void SetMobility(EComponentMobility::Type InMobility);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void SetScaleV(FRotator Rot);
	
	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void SetScaleH(FRotator Rot);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void EnableTranslation();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void EnableRotation();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void Deselect();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void DeselectActor();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void DeleteActor();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void RequestRemoval();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	bool CanBeRemoved();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void AdjustRod();

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXActor")
	void SetRodPosition(FVector TablePosition);

	UFUNCTION(BlueprintNativeEvent)
	void OnOverlapBegin(class UPrimitiveComponent* OverlapComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	UFUNCTION(BlueprintNativeEvent)
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	AOptiXVRPawn* OptiXVRPawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* Gizmo;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWidgetComponent* SupportWidget;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* TranslationSupport;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* TranslateX;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWidgetComponent* TranslateWidgetX;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* TranslateY;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWidgetComponent* TranslateWidgetY;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* TranslateZ;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWidgetComponent* TranslateWidgetZ;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	USceneComponent* RotationSupport;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ScaleH;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ScaleV;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* SupportH;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWidgetComponent* DegreeWidgetH;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* SupportV;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UWidgetComponent* DegreeWidgetV;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* SupportSphereV;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* SupportSphereH;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ArrowX;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ArrowY;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ArrowZ;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ConnectorV;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* ConnectorH;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* Sphere;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UStaticMeshComponent* Socket;

private:
	UPROPERTY(Category = StaticMeshActor, VisibleAnywhere, BlueprintReadOnly, meta = (ExposeFunctionCategories = "Mesh,Rendering,Physics,Components|StaticMesh", AllowPrivateAccess = "true"))
	class UStaticMeshComponent* StaticMeshComponent;


protected:

	//~ Begin UObject Interface.
	virtual FString GetDetailedInfoInternal() const override;


};
