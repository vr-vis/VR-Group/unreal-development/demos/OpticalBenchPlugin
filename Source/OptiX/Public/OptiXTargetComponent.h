// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OptiXObjectComponent.h"
#include "OptiXTargetComponent.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class ETexturePattern : uint8
{
	TP_BLACK	UMETA(DisplayName = "Black"),
	TP_CIRCLES	UMETA(DisplayName = "Circles"),
	TP_CHECKER	UMETA(DisplayName = "Checker"),
	TP_GRID		UMETA(DisplayName = "Grid"),
};


UCLASS(Blueprintable, hidecategories = (Object), meta = (BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXTargetComponent : public UOptiXObjectComponent
{
	GENERATED_BODY()

public:

	UOptiXTargetComponent(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTargetComponent")
	void SetSize(FVector NewSize);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXTargetComponent")
	FVector GetSize();


public:

	FIntPoint TextureSize;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "OptiXTargetComponent")
	FVector TargetSize;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	ETexturePattern CurrentTexturePattern;

private:

	UPROPERTY()
	UTexture2D* Texture;
};
