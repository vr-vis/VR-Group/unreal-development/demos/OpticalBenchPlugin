// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "OptiXObjectComponent.h"
#include "OptiXGlassDefinitions.h"

#include "OptiXLensComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLensRadiusChanged, float, Radius);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLensThicknessChanged, float, Thickness);



///**
// * Glass Type Enum
// */
//UENUM(BlueprintType)
//enum class EGlassType : uint8 // TODO fix descriptions
//{
//	BK7 = 0					UMETA(DisplayName = "BK7"),
//	SF5 = 1					UMETA(DisplayName = "SF5"),
//	SF11 = 2				UMETA(DisplayName = "SF11"),
//	FUSED_SILICA = 3		UMETA(DisplayName = "Fused silicia"),
//	SK16 = 4				UMETA(DisplayName = "SK16"),
//	F2 = 5					UMETA(DisplayName = "F2")
//};

/**
 * Lens Component
 */
UCLASS(Blueprintable, hidecategories = (Object), meta = (BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXLensComponent : public UOptiXCubemapComponent
{
	GENERATED_BODY()
	
public:

	UOptiXLensComponent(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	// UOptiXObjectComponent Interface End

	// This sadly only works in the editor itself, leave it for now
	//virtual void PostEditChangeProperty(FPropertyChangedEvent & PropertyChangedEvent) override;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
	void InitFromData(const FLensData& Data);

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetThickness(float Thickness);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetThickness() const;

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetThicknessForCylinderLength(float Length) const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetRadius1(float Radius);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetRadius1() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetRadius2(float Radius);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetRadius2() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetLensRadius(float Radius);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetLensRadius() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetLensType1(ELensSideType Type);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		ELensSideType GetLensType1() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetLensType2(ELensSideType Type);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		ELensSideType GetLensType2() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
	void SetGlassType(FString Type);

	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
	FString GetGlassType() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		void SetWavelength(float WL);
	UFUNCTION(BlueprintPure, BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent")
		float GetWavelength() const;

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXLensComponent") // TODO
	TArray<FString> GetGlassDefinitionNames();

	UFUNCTION()
	void OnWavelengthChangedEvent(float WL);

	// Event callbacks:
	UPROPERTY(BlueprintAssignable, Category = "OptiXLensComponent")
	FOnLensRadiusChanged OnLensRadiusChanged;

	UPROPERTY(BlueprintAssignable, Category = "OptiXLensComponent")
	FOnLensThicknessChanged OnLensThicknessChanged;

public:

	// Lens Properties
	// Let's try some more Unreal magic with the BlueprintGetter=GetterFunctionName and BlueprintSetter=SetterFunctionName
	// functions!

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetThickness, BlueprintSetter=SetThickness) // todo
	float LensThickness;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetRadius1, BlueprintSetter=SetRadius1)
	float Radius1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetRadius2, BlueprintSetter=SetRadius2)
	float Radius2; // todo fix name

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetLensRadius, BlueprintSetter=SetLensRadius)
	float LensRadius; // todo fix name

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetLensType1, BlueprintSetter=SetLensType1)
	ELensSideType LensType1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetLensType2, BlueprintSetter=SetLensType2)
	ELensSideType LensType2; // todo fix names again gah

	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetGlassType, BlueprintSetter=SetGlassType)
	FString GlassType;

	// Wavelength in NM
	UPROPERTY(BlueprintReadWrite, EditAnywhere, BlueprintGetter=GetWavelength, BlueprintSetter=SetWavelength)
	float CurrentWavelength;

	// Helper functions
	float GetCylinderLength(float Thickness) const;

};
