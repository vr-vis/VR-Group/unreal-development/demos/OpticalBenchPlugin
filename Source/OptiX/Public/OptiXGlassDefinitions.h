#pragma once
#include "CoreMinimal.h"

#include "OptiXGlassDefinitions.generated.h"


// todo move this to the right place:

/**
 * Lens Type Enum
 */
UENUM(BlueprintType)
enum class ELensSideType : uint8 // Unreal seems to only support uint8 for blueprints, adjust values 
{
	PLANE = 0 		UMETA(DisplayName = "Plane"),
	CONVEX = 1		UMETA(DisplayName = "Convex"),
	CONCAVE = 2 	UMETA(DisplayName = "Concave")
};


USTRUCT(BlueprintType)
struct OPTIX_API FLensData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	float Position; // Along the main axis

	UPROPERTY(BlueprintReadOnly)
	float LensRadius;

	UPROPERTY(BlueprintReadOnly)
	float Radius1;

	UPROPERTY(BlueprintReadOnly)
	float Radius2;

	UPROPERTY(BlueprintReadOnly)
	ELensSideType LensTypeSide1;

	UPROPERTY(BlueprintReadOnly)
	ELensSideType LensTypeSide2;

	UPROPERTY(BlueprintReadOnly)
	float Thickness;

	UPROPERTY(BlueprintReadOnly)
	FString GlassType;
};


USTRUCT(BlueprintType)
struct FSceneData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FString SceneName;

	UPROPERTY(BlueprintReadOnly)
	float LaserPosition = 0.0f; // Along main axis

	UPROPERTY(BlueprintReadOnly)
	float Wavelength = 500.0f;

	UPROPERTY(BlueprintReadOnly)
	TArray<FLensData> LensData;
};


UCLASS(BlueprintType, Blueprintable)
class OPTIX_API USceneDataObject : public UObject
{
	GENERATED_BODY()


public:

	UPROPERTY(BlueprintReadWrite)
	FString SceneName;

	UPROPERTY(BlueprintReadWrite)
	int32 Index;

	UPROPERTY(BlueprintReadWrite)
	int32 NumberLenses;
};

// Okay, for some crazy reason glass definitions have been originally saved here... TODOOOOO
USTRUCT(BlueprintType)
struct FGlassDefinition
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FVector B;

	UPROPERTY(BlueprintReadOnly)
	FVector C;

	FGlassDefinition(FVector b, FVector c) : B(b), C(c)
	{}
	FGlassDefinition() {}
};

USTRUCT()
struct FRawGlassDefinition
{
	GENERATED_BODY()


	UPROPERTY()
	FString Name;

	UPROPERTY()
	float B1;

	UPROPERTY()
	float B2;

	UPROPERTY()
	float B3;

	UPROPERTY()
	float C1;

	UPROPERTY()
	float C2;

	UPROPERTY()
	float C3;
};


