# pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Interface.h"
#include "UObject/ScriptMacros.h"

#include "OptiXComponentInterface.generated.h"


UINTERFACE(Blueprintable)
class UOptiXComponentInterface : public UInterface
{
	GENERATED_BODY()

public:

	// Apparently there must be something here

};
class OPTIX_API IOptiXComponentInterface
{
	GENERATED_BODY()
public:

	virtual void InitOptiXComponent(FRHICommandListImmediate & RHICmdList)
	{
		InitOptiXGeometry();
		InitOptiXMaterial();
		InitOptiXGroups();
	}

	virtual void RegisterOptiXComponent() {}

	virtual void UpdateOptiXComponentVariables() {}

	virtual void QueueOptiXContextUpdate() {}

	//UFUNCTION(/*meta = (BlueprintProtected)*/)
	virtual void CleanOptiXComponent() = 0;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void UpdateOptiXComponent() = 0;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGeometry() = 0;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXMaterial() = 0;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	virtual void InitOptiXGroups() = 0;

	virtual void SetUpdateQueued(bool UpdateQueued) = 0;

};