// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "OptiXCameraActor.h"

#include "OptiXPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class OPTIX_API AOptiXPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	AOptiXPlayerController(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	AOptiXPlayerCameraManager* OptiXPlayerCameraManager;

	// APlayerController interface start

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void SetupInputComponent() override;


	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
	void ToggleLaserDetector();

	UFUNCTION()
	void ToggleLaserTrace();

	// APlayerController interface end
	
};
