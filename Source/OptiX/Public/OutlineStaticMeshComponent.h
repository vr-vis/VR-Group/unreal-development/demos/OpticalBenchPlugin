// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "OutlineStaticMeshComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Rendering, Common), hidecategories = (Object, Activation, "Components|Activation"), ShowCategories = (Mobility), editinlinenew, meta = (BlueprintSpawnableComponent))
class OPTIX_API UOutlineStaticMeshComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

};
