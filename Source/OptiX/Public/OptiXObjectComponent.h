# pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/Components/SceneCaptureComponentCube.h"

//#include "OptiXContext.h"
//#include "OptiXComponentInterface.h"

#include "OptiXObjectComponent.generated.h"


// The interface is sadly useless as unreal doesn't let me do what's possible in standard c++.

UCLASS(abstract) //Blueprintable, hidecategories = (Object), meta=(BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXObjectComponent : public UPrimitiveComponent
{
	GENERATED_BODY()

public:

	// UObject interface

	UOptiXObjectComponent(const FObjectInitializer& ObjectInitializer);

	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;


	// End of UObject interface

	// Begin UActorComponent interface
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	// End UActorComponent interface

		//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void CleanOptiXComponent();

	// USceneComponent interface
	virtual void OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport = ETeleportType::None) override;
};


UCLASS(abstract) //Blueprintable, hidecategories = (Object), meta=(BlueprintSpawnableComponent)) // TODO Many things
class OPTIX_API UOptiXCubemapComponent : public USceneCaptureComponentCube
{
	GENERATED_BODY()

public:

	// UObject interface

	UOptiXCubemapComponent(const FObjectInitializer& ObjectInitializer);

	// End of UObject interface

	// Begin UActorComponent interface
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	// End UActorComponent interface

	// USceneComponent interface
	virtual void OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport = ETeleportType::None) override;

	//UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXComponent")
	void CleanOptiXComponent();	

	UFUNCTION(BlueprintCallable, /*meta = (BlueprintProtected)*/ Category = "OptiXCubemapComponent")
	virtual void RequestCubemapUpdate();

public:

	UPROPERTY(BlueprintReadOnly)
	int32 OptiXCubemapId;

	// Cubemap Unreal Stuff

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UTextureRenderTargetCube* CubeRenderTarget;

	FThreadSafeBool bCubemapCaptured = false;
};




