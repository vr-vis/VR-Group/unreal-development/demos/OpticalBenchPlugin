// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"


#include "LineInstancedStaticMeshComponent.generated.h"



/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), Blueprintable)
class OPTIX_API ULineInstancedStaticMeshComponent : public UInstancedStaticMeshComponent
{
	GENERATED_BODY()

public:

	ULineInstancedStaticMeshComponent(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;	
	//virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	void InitLineSegments(TArray<int32> LaserIndices, int32 NumberOfSegmentsPerLine, float LineW = 0.01);

	void UpdateLines();

	void UpdateLUT(TArray<FColor> ColorMap);

	void SetLaserMaterial(UMaterialInstanceDynamic* Mat);

public:

	UPROPERTY(BlueprintReadWrite, Category = OptiX)
	float LineWidth;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	UMaterialInstanceDynamic* DynamicLaserMaterial;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	int32 LineNumber;

	UPROPERTY(BlueprintReadOnly, Category = OptiX)
	int32 SegmentsPerLine;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	UTexture2D* ColorLUT;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	UTexture2D* IndexMap;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	TArray<FColor> ColorArray;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = OptiX)
	TArray<int32> LaserIndices;
	TArray<float> LaserIndicesFloat;

	TUniquePtr<FUpdateTextureRegion2D> TextureRegion;

//protected:
//
//	void BuildRenderData(FStaticMeshInstanceData& OutData, TArray<TRefCountPtr<HHitProxy>>& OutHitProxies);



};
