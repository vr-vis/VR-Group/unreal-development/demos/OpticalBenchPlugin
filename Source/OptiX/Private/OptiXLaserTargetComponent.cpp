// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserTargetComponent.h"

#include "OptiXModule.h"

#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "StatsDefines.h"


UOptiXLaserTargetComponent::UOptiXLaserTargetComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Target Component Constructor"));

	TargetRes = 512;
	bIsColorMode = true;
	HighlightColor = FLinearColor(1.0f, 0.0f, 0.0f).ToFColor(false);

	ColorLUT.SetNumZeroed(256);
	ColorData.SetNumZeroed(512*512);
	
	static ConstructorHelpers::FObjectFinder<UTexture2D> LT(TEXT("Texture2D'/OptiX/Targets/Target_LUT.Target_LUT'"));
	if (LT.Object != NULL)
	{
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Target Component: Initialized LUT Texture"));

		LUTTexture = LT.Object;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("OptiX Laser Target Component Constructor: LUT TEXTURE ASSET NOT FOUND"));
	}

}

void UOptiXLaserTargetComponent::BeginPlay()
{
	Super::BeginPlay();

	FTexture2DMipMap& IndexMip = LUTTexture->PlatformData->Mips[0];

	FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_ONLY));

	for (uint32 i = 0; i < 256; i++)
	{
		ColorLUT[i] = TextureData[i];
	}
	IndexMip.BulkData.Unlock();
	UE_LOG(LogTemp, Display, TEXT("Finished filling color LUT for the laser target"));
		   
	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXObject(GetUniqueID(), "laser_target");

	FOptiXBufferData TargetBufferData;
	TargetBufferData.Name = "target_buffer";
	TargetBufferData.Type = RT_BUFFER_INPUT_OUTPUT;
	TargetBufferData.Format = RT_FORMAT_FLOAT;
	TargetBufferData.BufferWidth = TargetRes;
	TargetBufferData.BufferHeight = TargetRes;

	FOptiXBufferData TargetBufferMaxData;
	TargetBufferMaxData.Name = "target_buffer_max";
	TargetBufferMaxData.Type = RT_BUFFER_INPUT_OUTPUT;
	TargetBufferMaxData.Format = RT_FORMAT_UNSIGNED_INT;
	TargetBufferMaxData.BufferWidth = 1;

	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXBuffer(GetUniqueID(), TargetBufferData);
	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXBuffer(GetUniqueID(), TargetBufferMaxData);

	FMatrix Transform = GetComponentToWorld().ToMatrixNoScale();
	FMatrix Inverse = Transform.Inverse();

	OptiXObjectInitFunction InitFunction =
		[Transform, Inverse, &TargetRes = TargetRes]
	(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		//FVector P1 = FVector(2.89f, 0.0f, 0.0f);
		//FVector P2 = FVector(-0.129f, 0.0f, 0.08f) * 1;

		Data->OptiXGeometryInstance["p1"]->setFloat(2.89f, 0.0f, 0.0f); // Detector
		Data->OptiXGeometryInstance["p2"]->setFloat(-0.129f * 1000, 0.0f * 1000, 0.08f * 1000);

		Data->OptiXGeometryInstance["stretchXY1"]->setFloat(0.05f * 100.0f, 0.05f * 100.0f); // Detector
		Data->OptiXGeometryInstance["stretchXZ2"]->setFloat(0.05f * 10.0f, 0.05f * 10.0f); // Corresponds to P2

		//OptiXGeometryInstance->SetInt("targetBufferWrite", 1);

		Data->OptiXGeometryInstance["targetBuffer"]->setBuffer(*Buffers->Find("target_buffer"));
		Data->OptiXGeometryInstance["targetBufferMax"]->setBuffer(*Buffers->Find("target_buffer_max"));

		Data->OptiXGeometryInstance["targetBufferDim"]->setFloat(TargetRes, TargetRes);
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueInitFunction(GetUniqueID(), InitFunction);
}

//float UOptiXLaserTargetComponent::GetMaxFromBuffer()
//{
//	//check(IsInRenderingThread());
//
//
//	//float Max = (float)*static_cast<unsigned int*>(TargetBufferMax->MapNative(0, RT_BUFFER_MAP_READ));
//	//TargetBufferMax->Unmap();
//	//if (Max == 0)
//	//{
//	//	Max = 1; // TODO WHY?
//	//}
//	//return Max;
//}

void UOptiXLaserTargetComponent::ClearOptiXBuffer()
{
	OptiXObjectUpdateFunction UpdateFunction =
		[&TargetRes =TargetRes](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		optix::Buffer Buffer = *Buffers->Find("target_buffer");
		float* DataPtr = static_cast<float*>(Buffer->map(0, RT_BUFFER_MAP_WRITE));
		FMemory::Memset(DataPtr, 0u, TargetRes * TargetRes * 4);
		Buffer->unmap();


		optix::Buffer BufferMax = *Buffers->Find("target_buffer_max");
		*static_cast<unsigned int*>(BufferMax->map(0, RT_BUFFER_MAP_WRITE)) = 0;
		BufferMax->unmap();
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
}

//void UOptiXLaserTargetComponent::UpdateBufferData()
//{


	//TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("UOptiXLaserTargetComponent::UpdateBufferData"))

	//check(IsInRenderingThread());

	//float Max = GetMaxFromBuffer();
	//UE_LOG(LogTemp, Warning, TEXT("UPDATING DEPRECATED BUFFER DATA"));


	//float* Data = static_cast<float*>(TargetBuffer->MapNative(0, RT_BUFFER_MAP_READ));

	//// Just use the previous algorithm for now without thinking too much about it
	//// But honestly doing this on the cpu hurts so much...
	//// TODO: Either
	//// 1) Make optix just output the correct colors directly OR
	//// 2) move the LUT stuff into the actual material...

	//float CurrentVal = 0;
	//FColor CurrentColor;
	//bool Tmp = false;

	//if (!bIsColorMode)
	//{
	//	for (uint32 i = 0; i < TargetRes * TargetRes; i++)
	//	{
	//		CurrentVal = Data[i] / Max;
	//		Tmp = (CurrentVal >= HighlightColorValue);
	//		ColorData[i] = (Tmp) ? HighlightColor : FLinearColor(CurrentVal, CurrentVal, CurrentVal).ToFColor(false);
	//	}
	//}
	//else
	//{
	//	for (uint32 i = 0; i < TargetRes * TargetRes; i++)
	//	{
	//		CurrentVal = Data[i] / Max;

	//		CurrentColor = ColorLUT[(int)(255.0f * CurrentVal)]; // Uh oh todo
	//		ColorData[i] = CurrentColor;
	//	}

	//}
	//TargetBuffer->Unmap();
//}

