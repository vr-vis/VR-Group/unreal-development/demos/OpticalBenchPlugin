// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXPlayerController.h"

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "OptiXLaserActor.h"
#include "OptiXLaserDetectorActor.h"
#include "OptiXModule.h"


AOptiXPlayerController::AOptiXPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerCameraManagerClass = AOptiXPlayerCameraManager::StaticClass();
	
	//bAutoManageActiveCameraTarget = false;
}

void AOptiXPlayerController::BeginPlay()
{
	UE_LOG(LogTemp, Warning, TEXT("OptiX Player Controller Begin Play!"));
	Super::BeginPlay();
	OptiXPlayerCameraManager = Cast<AOptiXPlayerCameraManager>(PlayerCameraManager);

	// Debug UI todo

	// Do one initial trace
	//OptiXPlayerCameraManager->Trace();
	//OptiXPlayerCameraManager->DisplayOutput();

	//PrimaryActorTick.bCanEverTick = true;
	//PrimaryActorTick.TickGroup = TG_PrePhysics;


}

void AOptiXPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	OptiXPlayerCameraManager = nullptr;
}

void AOptiXPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("ToggleLaserTrace", IE_Pressed, this, &AOptiXPlayerController::ToggleLaserTrace);
	InputComponent->BindAction("ToggleLaserDetector", IE_Pressed, this, &AOptiXPlayerController::ToggleLaserDetector);
}

void AOptiXPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);


	//UE_LOG(LogTemp, Display, TEXT("Player Controller: HasActivePawnControlCameraComponent: %i"), static_cast<int32>(HasActivePawnControlCameraComponent()));
	//UE_LOG(LogTemp, Display, TEXT("Player Controller: HasActiveCameraComponent: %i"), static_cast<int32>(HasActiveCameraComponent()));

	//double start = FPlatformTime::Seconds();
	//OptiXPlayerCameraManager->UpdateOptiXCamera();
	//double camera = FPlatformTime::Seconds();

	//OptiXPlayerCameraManager->Trace();
	//double trace = FPlatformTime::Seconds();

	//OptiXPlayerCameraManager->DisplayOutput();

	/*double display = FPlatformTime::Seconds();
	UE_LOG(LogTemp, Warning, TEXT("camera update executed in %f seconds."), camera - start);
	UE_LOG(LogTemp, Warning, TEXT("trace executed in %f seconds."), trace - camera);
	UE_LOG(LogTemp, Warning, TEXT("display update executed in %f seconds."), display - trace);*/


}

void AOptiXPlayerController::ToggleLaserDetector()
{
	TArray<AActor*> FoundLaserDetectorActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AOptiXLaserDetectorActor::StaticClass(), FoundLaserDetectorActors);

	for (AActor* Actor : FoundLaserDetectorActors)
	{
		bool bNew = !Cast<AOptiXLaserDetectorActor>(Actor)->bIsEnabled;
		Cast<AOptiXLaserDetectorActor>(Actor)->bIsEnabled = bNew;
		UE_LOG(LogTemp, Warning, TEXT("Set Laser Detector Update to: %i"), static_cast<int32>(bNew));
	}
}

void AOptiXPlayerController::ToggleLaserTrace()
{
	TArray<AActor*> FoundLaserActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AOptiXLaserActor::StaticClass(), FoundLaserActors);

	for (AActor* Actor : FoundLaserActors)
	{
		bool bNew = !Cast<AOptiXLaserActor>(Actor)->bLaserTraceEnabled;
		Cast<AOptiXLaserActor>(Actor)->bLaserTraceEnabled = bNew;
		UE_LOG(LogTemp, Warning, TEXT("Set Laser Trace Update to: %i"), static_cast<int32>(bNew));
		Cast<AOptiXLaserActor>(Actor)->LineInstancedStaticMeshComponent->SetVisibility(bNew, true);
	}
}
