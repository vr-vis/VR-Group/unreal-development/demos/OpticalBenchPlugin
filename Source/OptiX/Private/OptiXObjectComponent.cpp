#include "OptiXObjectComponent.h"

#include "Runtime/Engine/Classes/Engine/TextureRenderTargetCube.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectIterator.h"



#include "OptiXLaserActor.h"
#include "OptiXTargetComponent.h"
#include "OptiXModule.h"
#include "StatsDefines.h"

UOptiXObjectComponent::UOptiXObjectComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Register the component with OptiX and get the currently singular context. 
	// If this works, I should move the Context from the singleton away somehow into the scene to make it accessible via blueprints etc.
	// Maybe have 1 context per level? That would make most sense and allow for different scenes in a world, not that this is needed anyway.
	// But the singleton thing seems really limiting.

	// Only as a safety measure because I have no idea how the editor factory actually sets this stuff.

	UE_LOG(LogTemp, Display, TEXT("OptiX Component Constructor"));
	PrimaryComponentTick.bCanEverTick = false; // Don't need to tick 
	bWantsOnUpdateTransform = false;

}

FPrimitiveSceneProxy* UOptiXObjectComponent::CreateSceneProxy()
{
	class FOptiXObjectComponentSceneProxy final : public FPrimitiveSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		/** Initialization constructor. */
		FOptiXObjectComponentSceneProxy(const UOptiXObjectComponent* InComponent)
			: FPrimitiveSceneProxy(InComponent)
		{
			UniqueId = InComponent->GetUniqueID();
		}

		// FPrimitiveSceneProxy interface.

		virtual void ApplyLateUpdateTransform(const FMatrix& LateUpdateTransform) override
		{
			//FTransform UpdatedTransform = FTransform(LocalToWorld * LateUpdateTransform);
			//UE_LOG(LogTemp, Display, TEXT("Applying late update to laser"));
			FMatrix CachedTransform = GetLocalToWorld();
			FPrimitiveSceneProxy::ApplyLateUpdateTransform(LateUpdateTransform);			
			CachedTransform.SetOrigin(GetLocalToWorld().GetOrigin());
			//UE_LOG(LogTemp, Display, TEXT("Transform on late update: %s"), *UpdatedTransform.ToString());
			FOptiXModule::Get().GetOptiXContextManager()->ObjectPositionLateUpdate_RenderThread(UniqueId, CachedTransform.GetMatrixWithoutScale());
		}

		virtual uint32 GetMemoryFootprint(void) const override { return(sizeof(*this) + GetAllocatedSize()); }
		uint32 GetAllocatedSize(void) const { return(FPrimitiveSceneProxy::GetAllocatedSize()); }
	private:
		int32 UniqueId;
	};

	return new FOptiXObjectComponentSceneProxy(this);
}

void UOptiXObjectComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Display, TEXT("OptiX Component BeginPlay"));

	bWantsOnUpdateTransform = true;
	FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
}

void UOptiXObjectComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	UE_LOG(LogTemp, Display, TEXT(" ---------------- OptiX Component EndPlay, starting cleanup."));

	CleanOptiXComponent();
}

void UOptiXObjectComponent::OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport/* = ETeleportType::None*/)
{
	Super::OnUpdateTransform(EUpdateTransformFlags::SkipPhysicsUpdate, Teleport);

	FMatrix T = GetComponentToWorld().ToMatrixNoScale();
	FMatrix Inverse = T.Inverse();
	OptiXObjectUpdateFunction UpdateFunction =
		[Transform = T, Inverse](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		//OptiXGeometryInstance->SetFloat3DVector("size", TargetSize);
		Data->OptiXTransform->setMatrix(true, &Transform.M[0][0], &Inverse.M[0][0]);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
}


void UOptiXObjectComponent::CleanOptiXComponent()
{
	FOptiXModule::Get().GetOptiXContextManager()->RequestDestroyOptiXObjects(GetUniqueID());
}


UOptiXCubemapComponent::UOptiXCubemapComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Register the component with OptiX and get the currently singular context. 
	// If this works, I should move the Context from the singleton away somehow into the scene to make it accessible via blueprints etc.
	// Maybe have 1 context per level? That would make most sense and allow for different scenes in a world, not that this is needed anyway.
	// But the singleton thing seems really limiting.

	// Only as a safety measure because I have no idea how the editor factory actually sets this stuff.

	//bIsInitialized = false;

	UE_LOG(LogTemp, Display, TEXT("OptiX Cubemap Component Constructor"));
	PrimaryComponentTick.bCanEverTick = false; // Don't need to tick 
	bWantsOnUpdateTransform = false;


	bCaptureEveryFrame = false;
	bCaptureOnMovement = false;
}

void UOptiXCubemapComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Display, TEXT("OptiX Component BeginPlay"));

	OptiXCubemapId = FOptiXModule::Get().GetOptiXContextManager()->RequestCubemapId();

	// Create and Init the optix stuff here

	TArray<AActor*> FoundLaserActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AOptiXLaserActor::StaticClass(), FoundLaserActors);

	for (AActor* Actor : FoundLaserActors)
	{
		HideComponent(Cast<AOptiXLaserActor>(Actor)->LineInstancedStaticMeshComponent);
	}


	for (TObjectIterator<UOptiXCubemapComponent> Itr; Itr; ++Itr)
	{
		// Filter out objects not contained in the target world.
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}
		HideActorComponents(Itr->GetOwner());
	}

	for (TObjectIterator<UOptiXTargetComponent> Itr; Itr; ++Itr)
	{
		// Filter out objects not contained in the target world.
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}
		HideActorComponents(Itr->GetOwner());
	}

	for (TObjectIterator<UWidgetComponent> Itr; Itr; ++Itr)
	{
		// Filter out objects not contained in the target world.
		if (Itr->GetWorld() != GetWorld())
		{
			continue;
		}

		if (Itr->GetWidgetClass() != NULL && Itr->GetWidgetClass()->GetName().Contains("ScreenWidget"))
		{
			HideActorComponents(Itr->GetOwner());
		}
	}


	HideActorComponents(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	HideActorComponents(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));

	CubeRenderTarget = NewObject<UTextureRenderTargetCube>();
	CubeRenderTarget->Init(1024, PF_B8G8R8A8);
	CubeRenderTarget->UpdateResource();
	TextureTarget = CubeRenderTarget;

	CaptureScene();
	CaptureSceneDeferred();
	CubeRenderTarget->UpdateResource();
	bCubemapCaptured.AtomicSet(true);

	//InitOptiXComponent();	
	//RegisterOptiXComponent();
	//QueueOptiXContextUpdate();

	bWantsOnUpdateTransform = true;
	FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
}

void UOptiXCubemapComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	UE_LOG(LogTemp, Display, TEXT(" ---------------- OptiX Component EndPlay, starting cleanup."));

	CleanOptiXComponent();
}

void UOptiXCubemapComponent::OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport/* = ETeleportType::None*/)
{
	Super::OnUpdateTransform(EUpdateTransformFlags::SkipPhysicsUpdate, Teleport);


	FMatrix T = GetComponentToWorld().ToMatrixNoScale();
	FMatrix Inverse = T.Inverse();
	OptiXObjectUpdateFunction UpdateFunction =
		[Transform = T, Inverse](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		//OptiXGeometryInstance->SetFloat3DVector("size", TargetSize);
		Data->OptiXTransform->setMatrix(true, &Transform.M[0][0], &Inverse.M[0][0]);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
}

void UOptiXCubemapComponent::CleanOptiXComponent()
{
	FOptiXModule::Get().GetOptiXContextManager()->DeleteCubemapId(OptiXCubemapId);
	FOptiXModule::Get().GetOptiXContextManager()->RequestDestroyOptiXObjects(GetUniqueID());
}

void UOptiXCubemapComponent::RequestCubemapUpdate()
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("UOptiXCubemapComponent::RequestCubemapUpdate"))

	CaptureScene();
	
	UE_LOG(LogTemp, Display, TEXT("Requesting Cubemap Update"));


	//int32 X = 1024; // todo hardcoded
	//int32 Y = X;

	CubemapUpdateFunction UpdateFunction =
		[&CubeRenderTarget = CubeRenderTarget]
	(optix::Buffer CubemapBuffer, FRHICommandListImmediate & RHICmdList)
	{
		TArray<TArray<FColor>> SurfaceDataCube;
		SurfaceDataCube.SetNumZeroed(6);
		//TArray<FLinearColor> SD;

		int32 X = 1024; // todo hardcoded
		int32 Y = X;

		optix::uchar4* BufferData = static_cast<optix::uchar4*>(CubemapBuffer->map());

		FTextureRenderTargetCubeResource* RenderTargetCube = static_cast<FTextureRenderTargetCubeResource*>(CubeRenderTarget->GetRenderTargetResource());

		FIntRect InRectCube = FIntRect(0, 0, RenderTargetCube->GetSizeXY().X, RenderTargetCube->GetSizeXY().Y);
		FReadSurfaceDataFlags FlagsCube0(RCM_UNorm, CubeFace_PosX);
		FReadSurfaceDataFlags FlagsCube1(RCM_UNorm, CubeFace_NegX);
		FReadSurfaceDataFlags FlagsCube2(RCM_UNorm, CubeFace_PosY);
		FReadSurfaceDataFlags FlagsCube3(RCM_UNorm, CubeFace_NegY);
		FReadSurfaceDataFlags FlagsCube4(RCM_UNorm, CubeFace_PosZ);
		FReadSurfaceDataFlags FlagsCube5(RCM_UNorm, CubeFace_NegZ);

		RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[0], FlagsCube0);
		RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[1], FlagsCube1);
		RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[2], FlagsCube2);
		RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[3], FlagsCube3);
		RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[4], FlagsCube4);
		RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[5], FlagsCube5);

		uint32 MemSize = (X * Y * sizeof(FColor));
		FMemory::Memcpy(BufferData, SurfaceDataCube[0].GetData(), MemSize); // front
		FMemory::Memcpy(BufferData + X * Y * 1, SurfaceDataCube[1].GetData(), MemSize); // back
		FMemory::Memcpy(BufferData + X * Y * 2, SurfaceDataCube[2].GetData(), MemSize); // 
		FMemory::Memcpy(BufferData + X * Y * 3, SurfaceDataCube[3].GetData(), MemSize); // 
		FMemory::Memcpy(BufferData + X * Y * 4, SurfaceDataCube[4].GetData(), MemSize); // 
		FMemory::Memcpy(BufferData + X * Y * 5, SurfaceDataCube[5].GetData(), MemSize); //

		CubemapBuffer->unmap();

		UE_LOG(LogTemp, Display, TEXT("Finished Updating Cubemap"));
	};

	FOptiXModule::Get().GetOptiXContextManager()->RequestLensCubemapUpdate(GetUniqueID(), UpdateFunction);
}
