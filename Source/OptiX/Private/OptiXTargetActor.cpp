// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXTargetActor.h"

#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"

AOptiXTargetActor::AOptiXTargetActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Target Actor Constructor"));

	PrimaryActorTick.bCanEverTick = false;

	//static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/OptiX/target.target'"));
	//UStaticMesh* Asset = MeshAsset.Object;

	//GetStaticMeshComponent()->SetStaticMesh(Asset);
	//SetActorEnableCollision(false);

	OptiXTargetComponent = CreateDefaultSubobject<UOptiXTargetComponent>(TEXT("TargetComponent"));
	OptiXTargetComponent->SetupAttachment(RootComponent);
	OptiXTargetComponent->SetRelativeLocation({ 0, 0, OptiXTargetComponent->TargetSize.Z / 2});

	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//RootComponent = OptiXTargetComponent;
	//SM->SetupAttachment(OptiXTargetComponent);
}

