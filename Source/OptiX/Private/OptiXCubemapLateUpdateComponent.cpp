// Fill out your copyright notice in the Description page of Project Settings.


#include "OptiXCubemapLateUpdateComponent.h"

#include "PrimitiveSceneProxy.h"
#include "OptiXModule.h"

FPrimitiveSceneProxy* UOptiXCubemapLateUpdateComponent::CreateSceneProxy()
{
	class FOptiXCubemapComponentSceneProxy final : public FPrimitiveSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		/** Initialization constructor. */
		FOptiXCubemapComponentSceneProxy(const UOptiXCubemapLateUpdateComponent* InComponent)
			: FPrimitiveSceneProxy(InComponent), UniqueId(InComponent->GetOptiXComponentId())
		{
		}

		// FPrimitiveSceneProxy interface.

		virtual void ApplyLateUpdateTransform(const FMatrix& LateUpdateTransform) override
		{

			FMatrix NewTransform = GetLocalToWorld() * LateUpdateTransform;
			FVector OriginChange = NewTransform.GetOrigin() - GetLocalToWorld().GetOrigin();
			FMatrix ReducedLateUpdate = FMatrix::Identity;
			ReducedLateUpdate.SetOrigin(OriginChange);

			FPrimitiveSceneProxy::ApplyLateUpdateTransform(ReducedLateUpdate);


			//FMatrix CachedTransform = GetLocalToWorld();
			//FPrimitiveSceneProxy::ApplyLateUpdateTransform(LateUpdateTransform);
			//CachedTransform.SetOrigin(GetLocalToWorld().GetOrigin());
			////UE_LOG(LogTemp, Display, TEXT("Transform on late update: %s"), *UpdatedTransform.ToString());
			FOptiXModule::Get().GetOptiXContextManager()->ObjectPositionLateUpdate_RenderThread(*UniqueId, GetLocalToWorld().GetMatrixWithoutScale());
		}

		virtual uint32 GetMemoryFootprint(void) const override { return(sizeof(*this) + GetAllocatedSize()); }
		uint32 GetAllocatedSize(void) const { return(FPrimitiveSceneProxy::GetAllocatedSize()); }
	private:
		const uint32* UniqueId;
	};

	return new FOptiXCubemapComponentSceneProxy(this);
}

void UOptiXCubemapLateUpdateComponent::LinkOptiXComponent(UOptiXCubemapComponent* OptiXComponent)
{
	OptiXComponentId = OptiXComponent->GetUniqueID();
	UE_LOG(LogTemp, Display, TEXT("Setting Unique id on UOptiXCubemapComponent: %i"), OptiXComponentId);
}
