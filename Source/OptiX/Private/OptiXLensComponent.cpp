// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLensComponent.h"

#include "Runtime/Engine/Classes/Engine/TextureRenderTargetCube.h"


#include "OptiXModule.h"
#include "StatsDefines.h"

UOptiXLensComponent::UOptiXLensComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Radius1 = 0.8 * 100; // todo default values
	Radius2 = 1.0 * 100;
	LensRadius = 0.1 * 100;

	LensType1 = ELensSideType::CONVEX;
	LensType2 = ELensSideType::CONVEX;
	LensThickness = 0.025 * 100;
	CurrentWavelength = 450.0f;

	// A little hacky but w/e
	for (const TPair<FString, FGlassDefinition>& Pair : FOptiXModule::Get().GetGlassDefinitions())
	{
		GlassType = Pair.Key;
		break;
	}
	
}


void UOptiXLensComponent::BeginPlay()
{
	Super::BeginPlay();


	// do this on the game thread
	// hook into WL update:
	//UE_LOG(LogTemp, Display, TEXT("Begin Play on LensComponent, GameThread"));

	FOptiXModule::Get().GetOptiXContextManager()->WavelengthChangedEvent.AddUFunction(this, "OnWavelengthChangedEvent");


	// New functions

	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXObject(GetUniqueID(), "lens_parametric");

	// Create the required buffer and sampler 
	FOptiXBufferData BufferData;
	BufferData.Name = "texture_buffer";
	BufferData.Type = RT_BUFFER_INPUT | RT_BUFFER_CUBEMAP;
	BufferData.Format = RT_FORMAT_UNSIGNED_BYTE4;
	BufferData.BufferWidth = 1024;
	BufferData.BufferHeight = 1024;
	BufferData.BufferDepth = 6;

	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXBuffer(GetUniqueID(), BufferData);
	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXTextureSampler(GetUniqueID());

	FMatrix T = GetComponentToWorld().ToMatrixNoScale();
	FMatrix I = T.Inverse();

	double WL2 = FMath::Pow(CurrentWavelength / 1000.0, 2.0f);
	FGlassDefinition Def = FOptiXModule::Get().GetGlassDefinitions()[GlassType];
	//UE_LOG(LogTemp, Display, TEXT("Glass Def: %f, %f, %F"), Def.B.X, Def.B.Y, Def.B.Z);

	float Index = FMath::Sqrt(1 +
		Def.B.X * WL2 / (WL2 - Def.C.X) +
		Def.B.Y * WL2 / (WL2 - Def.C.Y) +
		Def.B.Z * WL2 / (WL2 - Def.C.Z));

	float HalfCylinderHeight = GetCylinderLength(LensThickness) / 2.0f;

	// Could just capture this as well
	OptiXObjectInitFunction InitFunction =
		[&OptiXCubemapId = OptiXCubemapId, Transform = T, Inverse = I, 
			Index, &Radius1 = Radius1, &Radius2 = Radius2, &LensRadius = LensRadius, 
			HalfCylinderHeight, &LensType1 = LensType1, &LensType2 = LensType2]
	(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		// Material stuff:
		Data->OptiXMaterial["importance_cutoff"]->setFloat(1e-2f);
		Data->OptiXMaterial["cutoff_color"]->setFloat(0.035f, 0.102f, 0.169f);
		Data->OptiXMaterial["fresnel_exponent"]->setFloat(3.0f);
		Data->OptiXMaterial["fresnel_minimum"]->setFloat(0.1f);
		Data->OptiXMaterial["fresnel_maximum"]->setFloat(1.0f);
		//Data->OptiXMaterial["refraction_index"]->setFloat(1.4f);
		Data->OptiXMaterial["refraction_color"]->setFloat(1.0f, 1.0f, 1.0f);
		Data->OptiXMaterial["reflection_color"]->setFloat(1.0f, 1.0f, 1.0f);
		Data->OptiXMaterial["refraction_maxdepth"]->setInt(10);
		Data->OptiXMaterial["reflection_maxdepth"]->setInt(5);
		Data->OptiXMaterial["extinction_constant"]->setFloat(FMath::Loge(0.83f), FMath::Loge(0.83f), FMath::Loge(0.83f));

		Data->OptiXMaterial["lens_id"]->setInt(OptiXCubemapId); // todo
		Data->OptiXMaterial["refraction_index"]->setFloat(Index);

		// General stuff

		Data->OptiXGeometryInstance["center"]->setFloat(0.0f, 0.0f, 0.0f);		
		Data->OptiXGeometryInstance["orientation"]->setFloat(0, 0, -1); 
		Data->OptiXGeometryInstance["radius"]->setFloat(Radius1);
		Data->OptiXGeometryInstance["radius2"]->setFloat(Radius2);
		Data->OptiXGeometryInstance["lensRadius"]->setFloat(LensRadius);
		Data->OptiXGeometryInstance["halfCylinderLength"]->setFloat(HalfCylinderHeight);
		Data->OptiXGeometryInstance["side1Type"]->setInt(static_cast<int32>(LensType1));
		Data->OptiXGeometryInstance["side2Type"]->setInt(static_cast<int32>(LensType2));
		Data->OptiXTransform->setMatrix(true, &Transform.M[0][0], &Inverse.M[0][0]);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

		// Texture Sampler and Buffer
		TextureSampler->setWrapMode(0, RT_WRAP_CLAMP_TO_EDGE);
		TextureSampler->setWrapMode(1, RT_WRAP_CLAMP_TO_EDGE);
		TextureSampler->setWrapMode(2, RT_WRAP_CLAMP_TO_EDGE);
		TextureSampler->setIndexingMode(RT_TEXTURE_INDEX_NORMALIZED_COORDINATES);
		TextureSampler->setReadMode(RT_TEXTURE_READ_NORMALIZED_FLOAT);
		TextureSampler->setMaxAnisotropy(1.0f);
		TextureSampler->setMipLevelCount(1u);
		TextureSampler->setArraySize(1u);

		optix::Buffer TextureBuffer = *Buffers->Find("texture_buffer");

		TextureSampler->setBuffer(0u, 0u, TextureBuffer);
		TextureSampler->setFilteringModes(RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE);

		FOptiXModule::Get().GetOptiXContextManager()->AddCubemapToBuffer(OptiXCubemapId, TextureSampler->getId());

		UE_LOG(LogTemp, Display, TEXT("Finished Init Cubemap"));

	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueInitFunction(GetUniqueID(), InitFunction);

	RequestCubemapUpdate();

	UE_LOG(LogTemp, Display, TEXT("Enqueued Cubemap Init"));

}

void UOptiXLensComponent::InitFromData(const FLensData& Data)
{
	SetLensRadius(Data.LensRadius * 10);
	SetRadius1(Data.Radius1 * 10);
	SetRadius2(Data.Radius2 * 10);
	SetThickness(Data.Thickness);
	SetLensType1(Data.LensTypeSide1);
	SetLensType2(Data.LensTypeSide2);
	SetGlassType(Data.GlassType);
}

void UOptiXLensComponent::SetThickness(float Thickness)
{
	UE_LOG(LogTemp, Display, TEXT("Setting Thickness: %f"), Thickness);
	LensThickness = Thickness;
	float HalfCylinderHeight = GetCylinderLength(LensThickness) / 2.0f;

	OptiXObjectUpdateFunction UpdateFunction =
		[HalfCylinderHeight](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["halfCylinderLength"]->setFloat(HalfCylinderHeight);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();
	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);

	OnLensThicknessChanged.Broadcast(LensThickness);
}

float UOptiXLensComponent::GetThickness() const
{
	// No silly conversions...
	return LensThickness;
}

float UOptiXLensComponent::GetThicknessForCylinderLength(float Length) const
{
	// With silly conversions:
		// Halfsphere thickness
	float HalfThickness1;
	float HalfThickness2;

	// Side 1
	if (LensType1 == ELensSideType::PLANE)
	{
		HalfThickness1 = 0;
	}
	else
	{
		HalfThickness1 = Radius1 - FMath::Sqrt(-LensRadius * LensRadius + Radius1 * Radius1);
		if (LensType1 == ELensSideType::CONCAVE)
		{
			HalfThickness1 *= -1;
		}
	}

	// Side 2
	if (LensType2 == ELensSideType::PLANE)
	{
		HalfThickness2 = 0;
	}
	else
	{
		HalfThickness2 = Radius2 - FMath::Sqrt(-LensRadius * LensRadius + Radius2 * Radius2);
		if (LensType2 == ELensSideType::CONCAVE)
		{
			HalfThickness2 *= -1;
		}
	}
	return Length + HalfThickness1 + HalfThickness2;
}

void UOptiXLensComponent::SetRadius1(float Radius)
{
	UE_LOG(LogTemp, Display, TEXT("Setting Radius 1: %f"), Radius);
	Radius1 = Radius;
	OptiXObjectUpdateFunction UpdateFunction =
		[&Radius1 = Radius1](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["radius"]->setFloat(Radius1);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);

}

float UOptiXLensComponent::GetRadius1() const
{
	return Radius1;
}

void UOptiXLensComponent::SetRadius2(float Radius)
{

	UE_LOG(LogTemp, Display, TEXT("Setting Radius 2: %f"), Radius);
	Radius2 = Radius;
	OptiXObjectUpdateFunction UpdateFunction =
		[&Radius2 = Radius2](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["radius2"]->setFloat(Radius2);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
}

float UOptiXLensComponent::GetRadius2() const
{
	return Radius2;
}

void UOptiXLensComponent::SetLensRadius(float Radius)
{
	UE_LOG(LogTemp, Display, TEXT("Setting Lens Radius: %f"), Radius);
	LensRadius = Radius;
	
	OptiXObjectUpdateFunction UpdateFunction =
		[&LensRadius = LensRadius](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["lensRadius"]->setFloat(LensRadius);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);

	OnLensRadiusChanged.Broadcast(Radius);
}

float UOptiXLensComponent::GetLensRadius() const
{
	return LensRadius;
}

void UOptiXLensComponent::SetLensType1(ELensSideType Type)
{
	UE_LOG(LogTemp, Display, TEXT("Setting Lens Side 1 Type: %i"), static_cast<int>(Type));

	LensType1 = Type;
	OptiXObjectUpdateFunction UpdateFunction =
		[&LensType1 = LensType1](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["side1Type"]->setInt(static_cast<int>(LensType1));
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
	SetThickness(LensThickness);
}

ELensSideType UOptiXLensComponent::GetLensType1() const
{
	return LensType1;
}

void UOptiXLensComponent::SetLensType2(ELensSideType Type)
{
	UE_LOG(LogTemp, Display, TEXT("Setting Lens Side 2 Type: %i"), static_cast<int>(Type));

	LensType2 = Type;
	OptiXObjectUpdateFunction UpdateFunction =
		[&LensType2 = LensType2](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["side2Type"]->setInt(static_cast<int>(LensType2));
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
	SetThickness(LensThickness);
}

ELensSideType UOptiXLensComponent::GetLensType2() const
{
	return LensType2;
}

void UOptiXLensComponent::SetGlassType(FString Type)
{
	UE_LOG(LogTemp, Display, TEXT("Setting Glass Type: %s"), *Type);

	GlassType = Type;
	SetWavelength(CurrentWavelength); // ???
}

FString UOptiXLensComponent::GetGlassType() const
{
	return GlassType;
}

void UOptiXLensComponent::SetWavelength(float WL)
{
	UE_LOG(LogTemp, Display, TEXT("Setting new WL in lens: %s"), *GetName());
	CurrentWavelength = WL;

	double WL2 = FMath::Pow(CurrentWavelength / 1000.0, 2.0f);
	FGlassDefinition Def = FOptiXModule::Get().GetGlassDefinitions()[GlassType];
	//UE_LOG(LogTemp, Display, TEXT("Glass Def: %f, %f, %F"), Def.B.X, Def.B.Y, Def.B.Z);

	float Index = FMath::Sqrt(1 +
		Def.B.X * WL2 / (WL2 - Def.C.X) +
		Def.B.Y * WL2 / (WL2 - Def.C.Y) +
		Def.B.Z * WL2 / (WL2 - Def.C.Z));

	OptiXObjectUpdateFunction UpdateFunction =
		[Index](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		Data->OptiXGeometryInstance["refraction_index"]->setFloat(Index);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
}

float UOptiXLensComponent::GetWavelength() const
{
	return CurrentWavelength;
}


float UOptiXLensComponent::GetCylinderLength(float Thickness) const
{
	// Halfsphere thickness
	float HalfThickness1;
	float HalfThickness2;

	// Side 1
	if (LensType1 == ELensSideType::PLANE)
	{
		HalfThickness1 = 0;
	}
	else
	{
		HalfThickness1 = Radius1 - FMath::Sqrt(-LensRadius * LensRadius + Radius1 * Radius1);
		if (LensType1 == ELensSideType::CONCAVE)
		{
			HalfThickness1 *= -1;
		}
	}

	// Side 2
	if (LensType2 == ELensSideType::PLANE)
	{
		HalfThickness2 = 0;
	}
	else
	{
		HalfThickness2 = Radius2 - FMath::Sqrt(-LensRadius * LensRadius + Radius2 * Radius2);
		if (LensType2 == ELensSideType::CONCAVE)
		{
			HalfThickness2 *= -1;
		}
	}
	return Thickness - HalfThickness1 - HalfThickness2;
}


TArray<FString> UOptiXLensComponent::GetGlassDefinitionNames()
{
	TArray<FString> Names;

	for (const TPair<FString, FGlassDefinition>& Pair : FOptiXModule::Get().GetGlassDefinitions())
	{
		Names.Add(Pair.Key);
	}

	return Names;
}

void UOptiXLensComponent::OnWavelengthChangedEvent(float WL)
{
	SetWavelength(WL);
}
