// Fill out your copyright notice in the Description page of Project Settings.


#include "OptiXGameModeBase.h"

#include "OptiXModule.h"
#include "OptiXPlayerController.h"

AOptiXGameModeBase::AOptiXGameModeBase(const FObjectInitializer& ObjectInitializer)
{
	PlayerControllerClass = AOptiXPlayerController::StaticClass();
}

void AOptiXGameModeBase::InitGame(const FString & MapName, const FString & Options, FString & ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	// Init the context
	if (GetWorld() == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("GetWorld was null in gamemode"));
	}
	FOptiXModule::Get().Init();
}

void AOptiXGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	FOptiXModule::Get().GetOptiXContextManager()->EndPlay();
}

const TArray<FSceneData>& AOptiXGameModeBase::GetOptiXSceneDataArray()
{
	return FOptiXModule::Get().GetSceneDataArray();
}