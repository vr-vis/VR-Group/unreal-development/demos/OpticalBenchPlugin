//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "prd.h"
#include "helpers.h"
#include "random.h"


#define PERCENTILE 1.47579f

using namespace optix;

rtDeclareVariable(uint3, launch_index, rtLaunchIndex, );
rtDeclareVariable(uint3, launch_dim,   rtLaunchDim, );
rtDeclareVariable(rtObject,      top_object, , );
rtDeclareVariable(float,         scene_epsilon, , );
rtDeclareVariable(int,           max_depth_laser, , );

rtDeclareVariable(unsigned int,           random_frame_seed, , );

rtDeclareVariable(float3,         laser_origin, , );
rtDeclareVariable(float3,         laser_forward, , );
rtDeclareVariable(float3,         laser_right, , );
rtDeclareVariable(float3,         laser_up, , );
rtDeclareVariable(Matrix4x4,      laser_rot, , );

rtDeclareVariable(float,          laserBeamWidth, , );
rtDeclareVariable(float,          laserSize, , );

rtBuffer<int, 2>   			laserIndex;
rtBuffer<float3, 2>   		laserDir;
rtBuffer<float4, 2>			result_laser;


RT_PROGRAM void laser_caster(){

	float2 d = make_float2(launch_index.x, launch_index.y) / make_float2(launch_dim.x, launch_dim.y) - make_float2(0.5f, 0.5f);
	float3 ray_origin = laser_origin + laser_right * laserSize * d.x + laser_up * laserSize * d.y;

	//Uniform random
	unsigned int seed = tea<16>(launch_dim.x*launch_index.y+launch_index.x*launch_index.z, random_frame_seed);
	float2 random = make_float2(rnd(seed), rnd(seed));
	//convert to normal distrubution
	float r = sqrtf(-2*log(random.x));
	float theta = 2*3.141592654f*random.y;
	random = clamp(make_float2(r*cosf(theta), r*sinf(theta)), -4.5f, 4.5f) * laserBeamWidth * 0.5 /PERCENTILE;
	ray_origin += (launch_index.z != 0) * (laser_right * random.x + laser_up * random.y);
	
	PerRayData_radiance_iterative prd;
	optix::Ray ray(ray_origin, laser_forward, /*ray type*/ 1, scene_epsilon );
	prd.depth = 0;
	prd.done = false;
	prd.hit_lens = 0; //track if the ray ever hit the lens
	prd.power = (launch_index.z > 0) * 1; //No power for launch index 0
	
	// next ray to be traced
	prd.origin = ray_origin;

	Matrix3x3 laser_rot3x3 = make_matrix3x3(laser_rot);

	//prd.direction = laser_rot3x3 * normalize(make_float3(1,1,-1)*laserDir[make_uint2(launch_index)]);		   
	prd.direction = laser_rot3x3 * laserDir[make_uint2(launch_index)];		   


	unsigned int widthIndex = launch_index.y * 50 + launch_index.x;
	//unsigned int startIndex = widthIndex * max_depth_laser * 2;
	
	bool cast_ray = laserIndex[make_uint2(launch_index)] < 0;
	for(int i = 0; i < max_depth_laser * 2; i += 2){
		//Determine if this launch_index, depth or last ray should trigger new cast
		if(cast_ray || prd.done || prd.depth >= max_depth_laser){ // just write rest of data as "invalid"
			if(launch_index.z == 0) result_laser[make_uint2(widthIndex, i)] = make_float4(0,-1,0,1);
			if(launch_index.z == 0) result_laser[make_uint2(widthIndex, i + 1)] = make_float4(0,-1,0,1);
			continue;
		}
		if(launch_index.z == 0) result_laser[make_uint2(widthIndex, i)] = make_float4(prd.origin,1);
		
		ray.origin = prd.origin;
		ray.direction = prd.direction;
		rtTrace(top_object, ray, prd);
		
		// Update ray data for the next path segment
		prd.depth++;
		if(launch_index.z == 0) result_laser[make_uint2(widthIndex, i + 1)] = make_float4(prd.origin,1);
	}
}