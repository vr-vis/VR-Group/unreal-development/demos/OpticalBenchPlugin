//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#ifndef OPTICAL_BENCH_LENS_INTERSECTIONS_H_
#define OPTICAL_BENCH_LENS_INTERSECTIONS_H_

#include <optix.h>
#include <optix_world.h>

struct perHitData{
	float 	t;
	float3  normal;
	float3 	p;
};

struct maxMinSet{
	float3 max;
	float3 min;
};

__device__ perHitData operator*(perHitData a, const int& b){
	a.t *= b;
	a.normal *= b;
	a.p *= b;
	return a;
}

__device__ perHitData operator*(const int& b, perHitData a){
	a.t *= b;
	a.normal *= b;
	a.p *= b;
	return a;
}

__device__ perHitData operator+(perHitData a, const perHitData& b){
	a.t += b.t;
	a.normal += b.normal;
	a.p += b.p;
	return a;
}

static __device__ float smallerButPositiv(float a, float b, float scene_epsilon){
	return (a > scene_epsilon && b > scene_epsilon) * fminf(a,b) +
	(a > scene_epsilon && b < -scene_epsilon) * a +
	(a < -scene_epsilon && b > scene_epsilon) * b +
	(a < scene_epsilon && b < scene_epsilon) * -1.0f; // all rest
}

static __device__ perHitData nearestButPositivHit(perHitData h1, perHitData h2, float scene_epsilon){
	perHitData r;
	r.t = -1.0f;
	
	return (h1.t > scene_epsilon && h2.t > scene_epsilon) *	(h1 * (h1.t <= h2.t) + h2 * (h1.t > h2.t))
	+ (h1.t > scene_epsilon && h2.t < -scene_epsilon) * h1
	+ (h1.t < -scene_epsilon && h2.t > scene_epsilon) * h2
	+ (h1.t < -scene_epsilon && h2.t < -scene_epsilon) * r;
}

static __device__ float circleIntersect(float radius, float3 center, optix::Ray ray, float3 orientation, float lensRadius, float scene_epsilon){
	using namespace optix;
	
	float3 L = ray.origin - center;
	float3 D = ray.direction;

	float tca = dot(L, D);
	
	float tch2 = tca*tca - dot(L, L) + radius*radius;
	
	if(tch2 > 0.0f){
		float t1 = -tca - sqrtf(tch2);
		if(t1 > 0.0f){ //check for actual hit in lens front, else discard hit
			float3 p1 = ray.origin + t1*ray.direction;
			float projection1 = dot(p1 - center, orientation);
			float3 belowP1 = projection1 * orientation + center;
			if(projection1 < 0.0f || length(p1 - belowP1) >= lensRadius) t1 = -1.0f;
		}
		float t2 = -tca + sqrtf(tch2);
		if(t2 > 0.0f){ //check for actual hit in lens front, else discard hit
			float3 p2 = ray.origin + t2*ray.direction;
			float projection2 = dot(p2 - center, orientation);
			float3 belowP2 = projection2 * orientation + center;
			if(projection2 < 0.0f || length(p2 - belowP2) >= lensRadius) t2 = -1.0f;
		}
	
		float t = smallerButPositiv(t1, t2, scene_epsilon);
		return (t < 0.0f) * -1.0f + (t >= 0.0f) * t;
	}
	return -1.0f;
}

static __device__ float cylinderIntersect(float length, float3 center, optix::Ray ray, float3 orientation, float lensRadius, float scene_epsilon){
	using namespace optix;
	
	float3 dp = ray.origin - center;
	
	float3 A = ray.direction - dot(ray.direction, orientation) * orientation;
	float a = dot(A,A);
	float b = dot(ray.direction - dot(ray.direction, orientation)*orientation, dp - dot(dp, orientation)*orientation);
	float3 C = (dp - dot(dp, orientation)*orientation);
	float c = dot(C,C) - lensRadius*lensRadius;
	
	float discriminant = b*b - a*c;
	if(discriminant > 0.0f){
		float t1 = (-b - sqrtf(discriminant))/a;
		if(fabs(dot((ray.origin + t1 * ray.direction) - center, orientation)) > length / 2) t1 = -1.0f;
		float t2 = (-b + sqrtf(discriminant))/a;
		if(fabs(dot((ray.origin + t2 * ray.direction) - center, orientation)) > length / 2) t2 = -1.0f;
		
		return smallerButPositiv(t1, t2, scene_epsilon);
	}
	
	return -1.0f;
}

static __device__ float intersectPlane(float3 center, optix::Ray ray, float3 orientation, float lensRadius, float scene_epsilon){
	using namespace optix;
	
	float denom = dot(orientation, ray.direction);
	
	if(fabs(denom) > scene_epsilon){
		float t = dot(center - ray.origin, orientation) / denom;
		float3 p = ray.origin + t*ray.direction;
		return (length(p - center) <= lensRadius) * t + (length(p - center) > lensRadius) * -1.0f;
	}

	return -1.0f; 
}

static __device__ maxMinSet getAABBFromCylinder(float3 center, float3 orientation, float halfLength1, float halfLength2, float radius){
	using namespace optix;
	
	float3 sideVector = normalize(cross(orientation, make_float3(0.0f, 1.0f, 0.0f)));
	float3 newUp = normalize(cross(sideVector, orientation));
	sideVector = sideVector * radius;
	newUp = newUp * radius;
	float3 depthVector = normalize(orientation);
	
	maxMinSet r;
	r.min = make_float3(+10000000000); //+INFINITY
	r.max = make_float3(-10000000000); //-INFINITY
	float3 testVector = make_float3(0.0f);
	
	testVector = center + depthVector*halfLength1 + sideVector + newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	testVector = center + depthVector*halfLength1 + sideVector - newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	testVector = center + depthVector*halfLength1 - sideVector + newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	testVector = center + depthVector*halfLength1 - sideVector - newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	
	testVector = center - depthVector*halfLength2 + sideVector + newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	testVector = center - depthVector*halfLength2 + sideVector - newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	testVector = center - depthVector*halfLength2 - sideVector + newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	testVector = center - depthVector*halfLength2 - sideVector - newUp; r.max = fmaxf(r.max, testVector); r.min = fminf(r.min, testVector);
	
	return r;
}

#endif  // OPTICAL_BENCH_LENS_INTERSECTIONS_H_