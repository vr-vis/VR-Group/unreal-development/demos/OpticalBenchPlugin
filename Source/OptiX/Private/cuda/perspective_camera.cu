//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "helpers.h"
#include "random.h"
#include "prd.h"

using namespace optix;

rtDeclareVariable(Matrix4x4,    invViewProjectionLeft, , );
rtDeclareVariable(Matrix4x4,    viewProjectionLeft, , );
rtDeclareVariable(Matrix4x4,	invViewProjectionRight, , );
rtDeclareVariable(Matrix4x4,	viewProjectionRight, , );

rtDeclareVariable(float,         scene_epsilon, , );
rtDeclareVariable(int,			 is_mono, , );

rtDeclareVariable(rtObject,      top_object, , );

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtDeclareVariable(uint2, launch_dim,   rtLaunchDim, );

rtBuffer<float4, 2>   result_color;
rtBuffer<float, 2>   result_depth;

//Do not need to be the real far/near values, just need to be the same as in the opengl rendering
__device__ float convertZToLinear(float depth)
{	
	float n = 0.1f; // camera z near
	float f = 1000.0f; // camera z far
	return (2.0f * n) / (f + n - depth * (f - n));	
}

RT_PROGRAM void pinhole_camera()
{
	float2 d = make_float2(launch_index) / make_float2(launch_dim);
	float dx = d.x * 2 - 1.0f;
	float dy = ((1.0f - d.y) - 0.5f) * 2.0f;

	// shooting two rays per pass, one for left and one for right

	float4 ray_start_4_left = invViewProjectionLeft * make_float4(dx, dy, 1.0f, 1.0f);
	float4 ray_end_4_left = invViewProjectionLeft * make_float4(dx, dy, 0.5f, 1.0f);

	float3 ray_origin_left = make_float3(ray_start_4_left.x, ray_start_4_left.y, ray_start_4_left.z);
	float3 ray_end_left = make_float3(ray_end_4_left.x, ray_end_4_left.y, ray_end_4_left.z);
	
	if (ray_start_4_left.w != 0)
	{
		ray_origin_left = (ray_origin_left / ray_start_4_left.w);
	}
	if (ray_end_4_left.w != 0)
	{
		ray_end_left = (ray_end_left / ray_end_4_left.w);
	}
	   	 
	float3 ray_direction_left = normalize(ray_end_left - ray_origin_left);

	optix::Ray ray_left(ray_origin_left, ray_direction_left, 0, scene_epsilon);

	// Right

	float4 ray_start_4_right = invViewProjectionRight * make_float4(dx, dy, 1.0f, 1.0f);
	float4 ray_end_4_right = invViewProjectionRight * make_float4(dx, dy, 0.5f, 1.0f);

	float3 ray_origin_right = make_float3(ray_start_4_right.x, ray_start_4_right.y, ray_start_4_right.z);
	float3 ray_end_right = make_float3(ray_end_4_right.x, ray_end_4_right.y, ray_end_4_right.z);

	if (ray_start_4_right.w != 0)
	{
		ray_origin_right = (ray_origin_right / ray_start_4_right.w);
	}
	if (ray_end_4_right.w != 0)
	{
		ray_end_right = (ray_end_right / ray_end_4_right.w);
	}

	float3 ray_direction_right = normalize(ray_end_right - ray_origin_right);

	optix::Ray ray_right(ray_origin_right, ray_direction_right, 0, scene_epsilon);

	PerRayData_radiance prd_left;
	prd_left.importance = 1.f;
	prd_left.depth = 0;
	prd_left.hit_depth = 900.0f;
	prd_left.flags = 0;
	prd_left.last_lens_id = 0;

	PerRayData_radiance prd_right;
	prd_right.importance = 1.f;
	prd_right.depth = 0;
	prd_right.hit_depth = 900.0f;
	prd_right.flags = 0;
	prd_right.last_lens_id = 0;

	
	{
		rtTrace(top_object, ray_left, prd_left);

		float4 hitpoint_left = viewProjectionLeft * make_float4(ray_origin_left + ray_direction_left * prd_left.hit_depth, 1.0f);
		hitpoint_left = hitpoint_left / hitpoint_left.w;
	
		result_color[launch_index] = make_float4(
			prd_left.result.x,
			prd_left.result.y,
			prd_left.result.z,
			static_cast<float>(1 - static_cast<unsigned int>(prd_left.flags & 1)) // 1 - miss
		);

		result_depth[launch_index] = hitpoint_left.z; //convertZToLinear(hitpoint.z *0.5f + 0.5f);
	}
	if (is_mono != 1)
	{
		rtTrace(top_object, ray_right, prd_right);

		float4 hitpoint_right = viewProjectionRight * make_float4(ray_origin_right + ray_direction_right * prd_right.hit_depth, 1.0f);
		hitpoint_right = hitpoint_right / hitpoint_right.w;

		result_color[launch_index + make_uint2(0, launch_dim.y)] = make_float4(
			prd_right.result.x,
			prd_right.result.y,
			prd_right.result.z,
			static_cast<float>(1 - (static_cast<unsigned int>(prd_right.flags & 1))) // 1 - miss
		);
		result_depth[launch_index + make_uint2(0, launch_dim.y)] = hitpoint_right.z; //convertZToLinear(hitpoint.z *0.5f + 0.5f);

	}
}