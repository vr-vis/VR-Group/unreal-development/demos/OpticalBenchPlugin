//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optixu_math_namespace.h>

using namespace optix;

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtBuffer<uchar4, 2>   result_color;
rtBuffer<float, 2>   result_depth;

rtDeclareVariable(float3,                draw_color, , );

// Convert a float4 in [0,1)^4 to a uchar4 in [0,255]^4
#ifdef __CUDACC__
static __device__ __inline__ optix::uchar4 make_color(const optix::float4& c)
{
    return optix::make_uchar4( static_cast<unsigned char>(__saturatef(c.z)*255.99f),  /* B */
                               static_cast<unsigned char>(__saturatef(c.y)*255.99f),  /* G */
                               static_cast<unsigned char>(__saturatef(c.x)*255.99f),  /* R */
                               static_cast<unsigned char>(__saturatef(c.w)*255.99f)); /* A */
}
#endif

RT_PROGRAM void draw_solid_color()
{
  //rtPrintf("Running (%d,%d)\n", launch_index.x, launch_index.y);
  
  float alpha = 0.0f;
  if(launch_index.y < 350) alpha = 1.0f;
  result_color[launch_index] = make_color(make_float4(draw_color, alpha));
  result_depth[launch_index] = 0.5f;
}

