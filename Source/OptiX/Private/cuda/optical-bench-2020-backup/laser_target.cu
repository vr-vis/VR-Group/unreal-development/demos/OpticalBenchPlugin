//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optix_world.h>

using namespace optix;

rtDeclareVariable(float3,  p1, , );
rtDeclareVariable(float3,  p2, , );
rtDeclareVariable(float2,  stretchXY1, , );
rtDeclareVariable(float2,  stretchXZ2, , );

rtDeclareVariable(float,   scene_epsilon, , );

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

rtDeclareVariable(float2, texture_coord, attribute texture_coord, );
rtDeclareVariable(int, writeable_surface, attribute writeable_surface, );

RT_PROGRAM void intersect(int primIdx)
{
	//Hit Z?
	float t_1 = (p1.x - ray.origin.x) * (1.0f / ray.direction.x);
	float3 hp = ray.origin + ray.direction * t_1;
	float2 rel_size_z = (make_float2(hp.y, hp.z) - (make_float2(p1.y, p1.z) - stretchXY1/2))/stretchXY1;
	bool hit_z = rel_size_z.x < 1 && rel_size_z.y < 1 && rel_size_z.x >= 0 && rel_size_z.y >= 0;
	
	//Hit X? - ignore this for now
	//float t_2 = (p2.x - ray.origin.x) * (1.0f / ray.direction.x);
	//float3 hp = ray.origin + ray.direction * t_2;
	//float2 rel_size_x = (make_float2(hp.z, hp.y) - (make_float2(p2.z, p2.y) - stretchXZ2/2))/stretchXZ2;
	//bool hit_x = rel_size_x.x < 1 && rel_size_x.y < 1 && rel_size_x.x >= 0 && rel_size_x.y >= 0;
	
	//Which one is closer
	//float tmin = fminf(t_1 + (!hit_z > 0)*0x7f800000 , t_2 + (!hit_x > 0)*100000); //0x7f800000 == +INFINITY
	//float2 rel_size = (tmin == t_1) * rel_size_z + (tmin == t_2) * rel_size_x;
	if((hit_z) && rtPotentialIntersection(t_1)) {
		texture_coord = make_float2(1 - rel_size_z.x, 1 - rel_size_z.y);
		writeable_surface = 1; // don't need this as well
		rtReportIntersection(0);
	}
}

RT_PROGRAM void bounds (int, optix::Aabb* aabb)
{

	// Make this a plane with x == 0

	//float min_x = fminf(p1.x - stretchXY1.x/2, p2.x);
	//float min_y = fminf(p1.y, p2.y - stretchXY1.x / 2);
	//float min_z = fminf(p1.z - stretchXY1.y / 2, p2.z - stretchXZ2.y / 2);
	////
	//float max_x = fmaxf(p1.x + stretchXY1.x/2, p2.x);
	//float max_y = fmaxf(p1.y, p2.y - stretchXY1.y / 2);
	//float max_z = fmaxf(p1.z - stretchXY1.y / 2, p2.z - stretchXZ2.y / 2);

	aabb->m_min = make_float3(-0.01, p1.y - stretchXY1.x / 2, p1.z - stretchXY1.y / 2);
	aabb->m_max = make_float3(0.01, p1.y + stretchXY1.x / 2, p1.z + stretchXY1.y / 2);
}
