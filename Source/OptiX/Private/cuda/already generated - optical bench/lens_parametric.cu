//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optix_world.h>
#include "lens_intersections.h"

using namespace optix;

rtDeclareVariable(float,  lensRadius, , );
rtDeclareVariable(float3,  orientation, , );
rtDeclareVariable(float3,  center, , );
rtDeclareVariable(float,   radius, , );
rtDeclareVariable(float,   radius2, , );
rtDeclareVariable(float,   halfCylinderLength, , );
rtDeclareVariable(float,   scene_epsilon, , );

//1==convex, 2==concave, 0==plane
rtDeclareVariable(int,   side1Type, , );
rtDeclareVariable(int,   side2Type, , );

rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, ); 
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );
rtDeclareVariable(float3, front_hit_point, attribute front_hit_point, ); 
rtDeclareVariable(float3, back_hit_point, attribute back_hit_point, );

RT_PROGRAM void intersect(int primIdx)
{
	perHitData h1;
	if(side1Type != 0){
		int mult = side1Type;
		if (side1Type == 2) mult = -1;

		float dist = -sqrtf(radius*radius - powf(lensRadius,2)) + mult*halfCylinderLength;
		float3 nCenter = center + mult *orientation*dist;
		h1.t = circleIntersect(radius, nCenter, ray, mult*orientation, lensRadius, scene_epsilon);
		h1.p = ray.origin + h1.t*ray.direction;
		h1.normal = mult *(h1.p - nCenter) / radius;
	} else{
		h1.t = intersectPlane(center + orientation*(halfCylinderLength), ray, -orientation, lensRadius, scene_epsilon);
		h1.p = ray.origin + h1.t*ray.direction;
		h1.normal = orientation;
	}	
	
	perHitData h2;
	if(side2Type != 0){
		int mult = side2Type;
		if (side2Type == 2) mult = -1;
		float dist = -sqrtf(radius2*radius2 - powf(lensRadius,2)) + mult *halfCylinderLength;
		float3 nCenter = center - mult *orientation*dist;
		h2.t = circleIntersect(radius2, nCenter, ray, -mult *orientation, lensRadius, scene_epsilon);
		h2.p = ray.origin + h2.t*ray.direction;
		h2.normal = mult *(h2.p - nCenter) / radius2;
	} else{
		h2.t = intersectPlane(center - orientation*(halfCylinderLength), ray, orientation, lensRadius, scene_epsilon);
		h2.p = ray.origin + h2.t*ray.direction;
		h2.normal = -orientation;
	}

	perHitData h3;
	h3.t = cylinderIntersect(halfCylinderLength*2, center, ray, orientation, lensRadius, scene_epsilon);	
	h3.p = ray.origin + h3.t*ray.direction;
	float3 inner = dot(h3.p - center, orientation) * orientation + center;
	h3.normal = normalize(h3.p - inner);
	
	perHitData closest = nearestButPositivHit(h1, h2, scene_epsilon);
	closest = nearestButPositivHit(closest, h3, scene_epsilon);
	
	if(rtPotentialIntersection(closest.t) ) {
		int b = (dot(closest.p - ray.origin, closest.normal) > 0.0f) * 2 - 1; //look from inside out yes == 1, no == -1
	
		front_hit_point = closest.p + -b * closest.normal * scene_epsilon;
		back_hit_point = closest.p + b * closest.normal * scene_epsilon;
		
		shading_normal = geometric_normal = closest.normal;
		rtReportIntersection( 0 );
	}
}

RT_PROGRAM void bounds (int, optix::Aabb* aabb)
{
	//Size for double convex case should be the biggest
	float halfSphere1 = (radius - sqrtf(radius*radius - lensRadius*lensRadius))*(side1Type == 1);
	float halfSphere2 = (radius2 - sqrtf(radius2*radius2 - lensRadius*lensRadius))*(side2Type == 1);
	maxMinSet res = getAABBFromCylinder(center, orientation, halfCylinderLength + halfSphere1 + scene_epsilon, halfCylinderLength + halfSphere2 + scene_epsilon, lensRadius);
	
	aabb->m_min = res.min;
	aabb->m_max = res.max;
}
