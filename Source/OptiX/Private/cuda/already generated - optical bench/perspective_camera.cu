//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix.h>
#include <optixu/optixu_math_namespace.h>
#include <optixu/optixu_matrix_namespace.h>
#include "helpers.h"
#include "random.h"
#include "prd.h"

using namespace optix;

rtDeclareVariable(Matrix4x4,     invViewProjection, , );
rtDeclareVariable(Matrix4x4,     viewProjection, , );

rtDeclareVariable(float,         scene_epsilon, , );
rtDeclareVariable(rtObject,      top_object, , );

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );
rtDeclareVariable(uint2, launch_dim,   rtLaunchDim, );

rtBuffer<float4, 2>   result_color;
rtBuffer<float, 2>   result_depth;

//Do not need to be the real far/near values, just need to be the same as in the opengl rendering
__device__ float convertZToLinear(float depth)
{	
	float n = 0.1f; // camera z near
	float f = 1000.0f; // camera z far
	return (2.0f * n) / (f + n - depth * (f - n));	
}

RT_PROGRAM void pinhole_camera()
{

	float2 d = make_float2(launch_index) / make_float2(launch_dim);
	float dx = d.x * 2 - 1.0f;
	float dy = ((1.0f - d.y) - 0.5f) * 2.0f;

	float4 ray_start_4 = invViewProjection * make_float4(dx, dy, 1.0f, 1.0f);
	float4 ray_end_4 = invViewProjection * make_float4(dx, dy, 0.5f, 1.0f);

	float3 ray_origin = make_float3(ray_start_4.x, ray_start_4.y, ray_start_4.z);
	float3 ray_end = make_float3(ray_end_4.x, ray_end_4.y, ray_end_4.z);
	
	if (ray_start_4.w != 0)
	{
		ray_origin = (ray_origin / ray_start_4.w);
	}
	if (ray_end_4.w != 0)
	{
		ray_end = (ray_end / ray_end_4.w);
	}

	float3 ray_direction = normalize(ray_end - ray_origin);

	optix::Ray ray(ray_origin, ray_direction, 0, scene_epsilon);

	PerRayData_radiance prd;
	prd.importance = 1.f;
	prd.depth = 0;
	prd.miss = 0;
	prd.hit_depth = 900.0f;
	prd.hit_lens = 0; //track if the ray ever hit the lens
	prd.last_lens_id = 0;

	rtTrace(top_object, ray, prd);
	
	float4 hitpoint = viewProjection * make_float4(ray_origin + ray_direction * prd.hit_depth, 1.0f);
	hitpoint = hitpoint / hitpoint.w;

	//unsigned char a = 255u;

	//rtPrintf("miss-top: %d\n", (prd.miss));

	//if (prd.miss)
	//{
	//	a = 0u;
	//}

	//uchar4 final_result =
	//	make_uchar4(			
	//		static_cast<unsigned char>(__saturatef(prd.result.x)*255.99f),  /* R */
	//		static_cast<unsigned char>(__saturatef(prd.result.y)*255.99f),  /* G */
	//		static_cast<unsigned char>(__saturatef(prd.result.z)*255.99f),  /* B */			
	//		a);

	float4 final_result = make_float4(
		prd.result.x,
		prd.result.y,
		prd.result.z,
		static_cast<float>(1 - prd.miss)
	);

	
	result_color[launch_index] = final_result;
	result_depth[launch_index] = hitpoint.z; //convertZToLinear(hitpoint.z *0.5f + 0.5f);
}