//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix_world.h>
#include "prd.h"

rtDeclareVariable(PerRayData_radiance, prd_radiance, rtPayload, );
rtDeclareVariable(PerRayData_radiance_iterative, prd_radiance_it, rtPayload, );
rtTextureSampler<float4, 2> frameTexture;
rtDeclareVariable(float2, texture_coord, attribute texture_coord, );
rtDeclareVariable(float, hit_depth, rtIntersectionDistance, );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );

RT_PROGRAM void closest_hit_radiance()
{ 
	if(texture_coord.x >= 0 && texture_coord.y >= 0){
		prd_radiance.result = make_float3(tex2D(frameTexture, texture_coord.x, texture_coord.y)); 	
	}else{
		prd_radiance.result = make_float3(1.0f);
	}
	
	prd_radiance.hit_depth = hit_depth;
}

RT_PROGRAM void closest_hit_iterative()
{ 
	float3 hit_point = ray.origin + hit_depth * ray.direction;
	prd_radiance_it.origin = hit_point;
	prd_radiance_it.done = true;
}
