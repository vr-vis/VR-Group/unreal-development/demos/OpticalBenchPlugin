//------------------------------------------------------------------------------
// Project Phoenix
//
// Copyright (c) 2017-2018 RWTH Aachen University, Germany,
// Virtual Reality & Immersive Visualization Group.
//------------------------------------------------------------------------------
//                                 License
//
// Licensed under the 3-Clause BSD License (the "License");
// you may not use this file except in compliance with the License.
// See the file LICENSE for the full text.
// You may obtain a copy of the License at
//
//     https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

#include <optix_world.h>
#include "prd.h"

rtDeclareVariable(PerRayData_radiance, prd_radiance, rtPayload, );
rtDeclareVariable(int, skybox, , );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtBuffer<int> skyboxBuffer;

RT_PROGRAM void skyboxLookup()
{ 
	//rtPrintf("hit_lens: %d\n", prd_radiance.hit_lens);
	//rtPrintf("!hit_lens: %d\n", (!prd_radiance.hit_lens));

	unsigned int hit_lens = static_cast<unsigned int>((prd_radiance.flags >> 1));
	//prd_radiance.miss = 1 - prd_radiance.hit_lens;//(prd_radiance.hit_lens == prd_radiance.hit_lens);
	prd_radiance.flags = prd_radiance.flags | ( 1 - hit_lens); // miss = 1 - hit_lens

	float3 temp =  make_float3(optix::rtTexCubemap<float4>(skyboxBuffer[prd_radiance.last_lens_id], ray.direction.x, ray.direction.y, ray.direction.z));  
	//rtPrintf("miss: %d\n", (prd_radiance.miss));
	prd_radiance.result = make_float3(temp.z, temp.y, temp.x);/* prd_radiance.hit_lens * */

}
