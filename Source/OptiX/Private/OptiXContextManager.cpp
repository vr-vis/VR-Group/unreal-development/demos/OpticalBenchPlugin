//#undef UpdateResource

#include "OptiXContextManager.h"
#include "OptiXModule.h"

#include "RenderCore.h"
#include "EngineUtils.h"

#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Runtime/Engine/Public/SceneView.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"

#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Public/TextureResource.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTarget2D.h"
#include "Runtime/Engine/Classes/Engine/TextureRenderTargetCube.h"

// VR
#include "Runtime/HeadMountedDisplay/Public/IHeadMountedDisplay.h"
#include "Runtime/HeadMountedDisplay/Public/IXRTrackingSystem.h"
#include "Runtime/HeadMountedDisplay/Public/IXRCamera.h"


#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"

#include "Async.h"

#include "StatsDefines.h"

//#include "Runtime/Windows/D3D11RHI/Private/Windows/D3D11RHIBasePrivate.h
#include "Runtime/Windows/D3D11RHI/Private/D3D11StateCachePrivate.h"
#include "Runtime/Windows/D3D11RHI/Public/D3D11State.h"
typedef FD3D11StateCacheBase FD3D11StateCache;
#include "Runtime/Windows/D3D11RHI/Public/D3D11Resources.h"

// Console variables todo

DEFINE_LOG_CATEGORY(OptiXContextManagerLog);


static TAutoConsoleVariable<int32> CVarDisableTrace(
	TEXT("optix.DisableTrace"),
	0,
	TEXT("Defines if Optix should perform a constant trace.\n"),
	ECVF_Scalability | ECVF_RenderThreadSafe);

static TAutoConsoleVariable<int32> CVarDisableLaserTrace(
	TEXT("optix.DisableLaserTrace"),
	0,
	TEXT("Defines if Optix should perform a constant trace.\n"),
	ECVF_Scalability | ECVF_RenderThreadSafe);


FOptiXContextUpdateManager::FOptiXContextUpdateManager(const FAutoRegister& AutoRegister)
	: FSceneViewExtensionBase(AutoRegister)
{}

void FOptiXContextUpdateManager::SetupViewFamily(FSceneViewFamily& InViewFamily)
{
}
void FOptiXContextUpdateManager::SetupView(FSceneViewFamily& InViewFamily, FSceneView& InView)
{
}
void FOptiXContextUpdateManager::BeginRenderViewFamily(FSceneViewFamily& InViewFamily)
{
}
void FOptiXContextUpdateManager::PreRenderView_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneView& InView)
{
}
void FOptiXContextUpdateManager::PreRenderViewFamily_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily)
{
	// Hacky hacky
	FOptiXModule::Get().GetOptiXContextManager()->ExecuteOptiXUpdate_PreLateUpdate(RHICmdList);
}
bool FOptiXContextUpdateManager::IsActiveThisFrame(class FViewport* InViewport) const
{
	return bIsActive;
}
int32 FOptiXContextUpdateManager::GetPriority() const
{
	return 100;
}


FOptiXContextManager::FOptiXContextManager(const FAutoRegister& AutoRegister)
	: FSceneViewExtensionBase(AutoRegister)
{
	UE_LOG(OptiXContextManagerLog, Display, TEXT("FOptiXContextManager, is in rendering thread: %i"), static_cast<int32>(IsInRenderingThread()));

	//RTXOn = 0;

	LaserMaxDepth = 20;
	LaserEntryPoint = 1; // Default, will be overwritten anyway

	LaserBufferWidth = 50 * 50;
	LaserBufferHeight = LaserMaxDepth * 2;

	LaserBufferSize = LaserBufferHeight * LaserBufferWidth;

	OnSceneChangedDelegate.AddRaw(this, &FOptiXContextManager::SceneChangedCallback);

	OptiXPTXDir = FOptiXModule::Get().OptiXPTXDir;

	OptiXContextUpdateManager = FSceneViewExtensions::NewExtension<FOptiXContextUpdateManager>();
}

int32 FOptiXContextManager::GetPriority() const
{
	// FDefaultXRCamera: 0 (does the basic lateupdate)
	// OculusHMD : -1
	// SteamVR: seems to be default (0)
	return 10;
}


// gamethread
void FOptiXContextManager::SetupViewFamily(FSceneViewFamily & InViewFamily)
{}

void FOptiXContextManager::SetupView(FSceneViewFamily & InViewFamily, FSceneView & InView)
{}

// gamethread
void FOptiXContextManager::BeginRenderViewFamily(FSceneViewFamily & InViewFamily)
{}

// Called on render thread at the start of rendering, for each view, after PreRenderViewFamily_RenderThread call.
void FOptiXContextManager::PreRenderView_RenderThread(FRHICommandListImmediate & RHICmdList, FSceneView & InView)
{}

// Called on render thread at the start of rendering.
void FOptiXContextManager::PreRenderViewFamily_RenderThread(FRHICommandListImmediate & RHICmdList, FSceneViewFamily & InViewFamily)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::PreRenderViewFamily_RenderThread"))

	if (bEndPlayReceived)
	{
		Cleanup_RenderThread();
		return;
	}

	// launch the laser trace if needed (todo)
	LaunchLaser(RHICmdList);
	//CopyLaserCudaTexture(RHICmdList);

}

void FOptiXContextManager::PostRenderViewFamily_RenderThread(FRHICommandListImmediate & RHICmdList, FSceneViewFamily & InViewFamily)
{
	// check if compiler optimizes if away
	if (bWithHMD)
		LaunchStandardTrace(RHICmdList, InViewFamily);
	else
		LaunchMonoscopicTrace(RHICmdList, InViewFamily);

	// If an ortho pass was previously requested, execute it now and clear the request
	if (bRequestOrthoPass)
	{
		RenderOrthoPass();
		bRequestOrthoPass.AtomicSet(false);
	}
}

void FOptiXContextManager::PostRenderView_RenderThread(FRHICommandListImmediate & RHICmdList, FSceneView & InView)
{}

void FOptiXContextManager::ExecuteOptiXUpdate_PreLateUpdate(FRHICommandListImmediate & RHICmdList)
{
	// New functions
	ParseCreationQueue(); // Create new stuff
	ParseDestroyQueue(); // Get rid of removed objects - this order such that just created objects can be destroyed in the same tick and no ghost objects happen
	ParseInitQueue();
	ParseUpdateQueue(RHICmdList);
	ParseCubemapUpdateQueue(RHICmdList);
}

void FOptiXContextManager::LaunchStandardTrace(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::LaunchStandardTrace")


		if (bEndPlayReceived)
		{
			return;
		}

	if (InViewFamily.Views.Num() < 2)
		return;

	// Get the views for the respective eyes:
	EStereoscopicPass LeftEye = EStereoscopicPass::eSSP_LEFT_EYE;
	EStereoscopicPass RightEye = EStereoscopicPass::eSSP_RIGHT_EYE;

	const FSceneView& LeftEyeView = InViewFamily.GetStereoEyeView(LeftEye);
	const FSceneView& RightEyeView = InViewFamily.GetStereoEyeView(RightEye);

	// Set the required matrices
	NativeContext["invViewProjectionLeft"]->setMatrix4x4fv(true, &LeftEyeView.ViewMatrices.GetInvViewProjectionMatrix().M[0][0]);
	NativeContext["viewProjectionLeft"]->setMatrix4x4fv(true, &LeftEyeView.ViewMatrices.GetViewProjectionMatrix().M[0][0]);
	NativeContext["invViewProjectionRight"]->setMatrix4x4fv(true, &RightEyeView.ViewMatrices.GetInvViewProjectionMatrix().M[0][0]);
	NativeContext["viewProjectionRight"]->setMatrix4x4fv(true, &RightEyeView.ViewMatrices.GetViewProjectionMatrix().M[0][0]);
	//NativeContext->validate();

	{
		TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::Trace")
			//bIsTracing.AtomicSet(true);
			// Execute the actual trace
			NativeContext->launch(0, Width, Height);
		//bIsTracing.AtomicSet(false);
		//return;
	}
	{
		// Check cuda resources for NULL. Shouldn't be needed as they *should* never be NULL.
		if (Resources[0] == NULL || Resources[1] == NULL || Resources[2] == NULL || Resources[3] == NULL)
		{
			UE_LOG(OptiXContextManagerLog, Error, TEXT("CUDA Resources are NULL"));
			return;
		}

		// Map the four graphics resources corresponding to color, depth for both eyes.
		cudaGraphicsMapResources(4, Resources, 0);
		PrintLastCudaError("cudaGraphicsMapResources");

		// Map the left eye color resource to a cudaArray 
		cudaArray *CuArrayColorLeft;
		cudaGraphicsSubResourceGetMappedArray(&CuArrayColorLeft, CudaResourceColorLeft, 0, 0);
		PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

		// Copy the result of the optix 2D color buffer into the mapped array. 
		// As both passes are written into the same buffer, this copies only the first half corresponding to the left eye.
		cudaMemcpy2DToArray(
			CuArrayColorLeft, // dst array
			0, 0,    // offset
			CudaLinearMemoryColor, Width * 4 * sizeof(float),       // src
			Width * 4 * sizeof(float), Height, // extent
			cudaMemcpyDeviceToDevice); // kind
		PrintLastCudaError("cudaMemcpy2DToArray");


		// Copy Color Right
		cudaArray *CuArrayColorRight;
		cudaGraphicsSubResourceGetMappedArray(&CuArrayColorRight, CudaResourceColorRight, 0, 0);
		PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

		// Copy the result of the optix 2D color buffer into the mapped array. 
		// As this copies the into the right eye, the buffer pointer needs to be offset by (Height * Width)
		// to copy the second half.
		cudaMemcpy2DToArray(
			CuArrayColorRight, // dst array
			0, 0,    // offset
			CudaLinearMemoryColor + (Height * Width), Width * 4 * sizeof(float),       // src
			Width * 4 * sizeof(float), Height, // extent
			cudaMemcpyDeviceToDevice); // kind
		PrintLastCudaError("cudaMemcpy2DToArray");


		// Copy Depth Left
		cudaArray *CuArrayDepthLeft;
		cudaGraphicsSubResourceGetMappedArray(&CuArrayDepthLeft, CudaResourceDepthLeft, 0, 0);
		PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

		cudaMemcpy2DToArray(
			CuArrayDepthLeft, // dst array
			0, 0,    // offset
			CudaLinearMemoryDepth, Width * sizeof(float),       // src
			Width * sizeof(float), Height, // extent
			cudaMemcpyDeviceToDevice); // kind
		PrintLastCudaError("cudaMemcpy2DToArray");

		// Copy Depth Right
		cudaArray *CuArrayDepthRight;
		cudaGraphicsSubResourceGetMappedArray(&CuArrayDepthRight, CudaResourceDepthRight, 0, 0);
		PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

		cudaMemcpy2DToArray(
			CuArrayDepthRight, // dst array
			0, 0,    // offset
			CudaLinearMemoryDepth + (Height * Width), Width * sizeof(float),       // src
			Width * sizeof(float), Height, // extent
			cudaMemcpyDeviceToDevice); // kind
		PrintLastCudaError("cudaMemcpy2DToArray");


		cudaGraphicsUnmapResources(4, Resources, 0);
		PrintLastCudaError("cudaGraphicsUnmapResources");

		//D3DDeviceContext->Flush();
		//UpdateCubemapBuffer(RHICmdList);
	}
	//CopyLaserCudaTexture(RHICmdList);
}

void FOptiXContextManager::LaunchMonoscopicTrace(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& InViewFamily)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::LaunchMonoscopicTrace")


	if (bEndPlayReceived)
		{
			return;
		}

	if (InViewFamily.Views.Num() < 2)
		return;

	// Get the views for the respective eyes:
	EStereoscopicPass Mono = EStereoscopicPass::eSSP_FULL;

	const FSceneView& MonoView = InViewFamily.GetStereoEyeView(Mono);

	// Set the required matrices
	NativeContext["invViewProjectionLeft"]->setMatrix4x4fv(true, &MonoView.ViewMatrices.GetInvViewProjectionMatrix().M[0][0]);
	NativeContext["viewProjectionLeft"]->setMatrix4x4fv(true, &MonoView.ViewMatrices.GetViewProjectionMatrix().M[0][0]);

	//NativeContext->validate();

	{
		TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::Trace")
			//bIsTracing.AtomicSet(true);
			// Execute the actual trace
		NativeContext->launch(0, Width, Height);
		//bIsTracing.AtomicSet(false);
		//return;
	}
	{
		// Check cuda resources for NULL. Shouldn't be needed as they *should* never be NULL.
		if (Resources[0] == NULL || Resources[1] == NULL)
		{
			UE_LOG(OptiXContextManagerLog, Error, TEXT("CUDA Resources are NULL"));
			return;
		}

		// Map the two graphics resources corresponding to color, depth
		cudaGraphicsMapResources(2, Resources, 0);
		PrintLastCudaError("cudaGraphicsMapResources");

		// Map the left eye color resource to a cudaArray 
		cudaArray* CuArrayColorLeft;
		cudaGraphicsSubResourceGetMappedArray(&CuArrayColorLeft, CudaResourceColorLeft, 0, 0);
		PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

		// Copy the result of the optix 2D color buffer into the mapped array. 
		// As both passes are written into the same buffer, this copies only the first half corresponding to the left eye.
		cudaMemcpy2DToArray(
			CuArrayColorLeft, // dst array
			0, 0,    // offset
			CudaLinearMemoryColor, Width * 4 * sizeof(float),       // src
			Width * 4 * sizeof(float), Height, // extent
			cudaMemcpyDeviceToDevice); // kind
		PrintLastCudaError("cudaMemcpy2DToArray");

		// Copy Depth Left
		cudaArray* CuArrayDepthLeft;
		cudaGraphicsSubResourceGetMappedArray(&CuArrayDepthLeft, CudaResourceDepthLeft, 0, 0);
		PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

		cudaMemcpy2DToArray(
			CuArrayDepthLeft, // dst array
			0, 0,    // offset
			CudaLinearMemoryDepth, Width * sizeof(float),       // src
			Width * sizeof(float), Height, // extent
			cudaMemcpyDeviceToDevice); // kind
		PrintLastCudaError("cudaMemcpy2DToArray");

		cudaGraphicsUnmapResources(2, Resources, 0);
		PrintLastCudaError("cudaGraphicsUnmapResources");
	}

	// If an ortho pass was previously requested, execute it now and clear the request
	if (bRequestOrthoPass)
	{
		RenderOrthoPass();
		bRequestOrthoPass.AtomicSet(false);
	}
}

void FOptiXContextManager::CopyLaserCudaTexture(FRHICommandListImmediate & RHICmdList)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::CopyLaserCudaTexture")
	if (/*bSceneChanged &&*/ bIsInitializedLaser && !CVarDisableLaserTrace.GetValueOnRenderThread())
	{
		if (Resources[4] == NULL)
		{
			return;
		}
		{
			TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::LaunchLaser::CudaScope::MapAndCopy"))

				cudaGraphicsMapResources(1, Resources + 4, 0);
			PrintLastCudaError("cudaGraphicsMapResources");

			if (CudaResourceIntersections == NULL)
			{
				cudaGraphicsUnmapResources(1, Resources + 4, 0);
				return;
			}

			cudaArray *CuArrayIntersections;
			cudaGraphicsSubResourceGetMappedArray(&CuArrayIntersections, CudaResourceIntersections, 0, 0);
			PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

			{
				TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::LaunchLaser::CudaScope::Memcpy"))

					cudaMemcpy2DToArray(
						CuArrayIntersections, // dst array
						0, 0,    // offset
						CudaLinearMemoryIntersections, LaserBufferWidth * 4 * sizeof(float),       // src
						LaserBufferWidth * 4 * sizeof(float), LaserBufferHeight, // extent
						cudaMemcpyDeviceToDevice); // kind
				PrintLastCudaError("cudaMemcpy2DToArray");
			}
		}
		TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::LaunchLaser::CudaScope::Unmap"))

		cudaGraphicsUnmapResources(1, Resources + 4, 0);
		PrintLastCudaError("cudaGraphicsUnmapResources");
		{
			TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::LaunchLaser::BroadcastFinish")

			//bSceneChanged.AtomicSet(false);
			for (const auto& Pair : LaserTraceFinishedCallbacks)
			{
				optix::TextureSampler Sampler = nullptr;
				TMap<FString, optix::Buffer>* BufferMap = nullptr;
				FOptiXObjectData* Data = nullptr;
				if (OptiXTextureSamplers.Contains(Pair.Key))
				{
					Sampler = OptiXTextureSamplers[Pair.Key];
				}
				if (OptiXBuffers.Contains(Pair.Key))
				{
					BufferMap = &OptiXBuffers[Pair.Key];
				}
				if (OptiXObjectData.Contains(Pair.Key))
				{
					Data = &OptiXObjectData[Pair.Key];
				}
				Pair.Value(Data, BufferMap, Sampler, RHICmdList);
			}
		}
	}
}

void FOptiXContextManager::LaunchLaser(FRHICommandListImmediate & RHICmdList)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::LaunchLaser")
	if (/*bSceneChanged &&*/ bIsInitializedLaser && !CVarDisableLaserTrace.GetValueOnRenderThread())
	{
		static uint32 RandomSeed = 0;
		NativeContext["random_frame_seed"]->setUint(RandomSeed++);

		TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::NativeContext::launch")
		NativeContext->launch(1, 50, 50, 20);		
		if (Resources[4] == NULL)
		{
			return;
		}
		{
			TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::LaunchLaser::CudaScope::MapAndCopy"))

			cudaGraphicsMapResources(1, Resources + 4, 0);
			PrintLastCudaError("cudaGraphicsMapResources");

			if (CudaResourceIntersections == NULL)
			{
				cudaGraphicsUnmapResources(1, Resources + 4, 0);
				return;
			}

			cudaArray *CuArrayIntersections;
			cudaGraphicsSubResourceGetMappedArray(&CuArrayIntersections, CudaResourceIntersections, 0, 0);
			PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

			{
				TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::LaunchLaser::CudaScope::Memcpy"))

					cudaMemcpy2DToArray(
						CuArrayIntersections, // dst array
						0, 0,    // offset
						CudaLinearMemoryIntersections, LaserBufferWidth * 4 * sizeof(float),       // src
						LaserBufferWidth * 4 * sizeof(float), LaserBufferHeight, // extent
						cudaMemcpyDeviceToDevice); // kind
				PrintLastCudaError("cudaMemcpy2DToArray");
			}
		}
		TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::LaunchLaser::CudaScope::Unmap"))

		cudaGraphicsUnmapResources(1, Resources + 4, 0);
		PrintLastCudaError("cudaGraphicsUnmapResources");
		{
			TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::LaunchLaser::BroadcastFinish")

			//bSceneChanged.AtomicSet(false);
			for (const auto& Pair : LaserTraceFinishedCallbacks)
			{
				optix::TextureSampler Sampler = nullptr;
				TMap<FString, optix::Buffer>* BufferMap = nullptr;
				FOptiXObjectData* Data = nullptr;
				if (OptiXTextureSamplers.Contains(Pair.Key))
				{
					Sampler = OptiXTextureSamplers[Pair.Key];
				}
				if (OptiXBuffers.Contains(Pair.Key))
				{
					BufferMap = &OptiXBuffers[Pair.Key];
				}
				if (OptiXObjectData.Contains(Pair.Key))
				{
					Data = &OptiXObjectData[Pair.Key];
				}
				Pair.Value(Data, BufferMap, Sampler, RHICmdList);
			}
		}
	}
}

bool FOptiXContextManager::IsActiveThisFrame(FViewport * InViewport) const
{
	//UE_LOG(OptiXContextManagerLog, Warning, TEXT("IsActiveThisFrame"));
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::IsActiveThisFrame")

	bool bDisableTrace = static_cast<bool>(CVarDisableTrace.GetValueOnGameThread()); // Bad naming fix me
	return NativeContext != NULL && !bDisableTrace && bIsInitializedAll /*&& !bEndPlayReceived*/ /* && !bEndPlay*//* && TrackingSystem->IsHeadTrackingAllowed()*/;
}

void FOptiXContextManager::RenderOrthoPass()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::RenderOrthoPass")

	if(bWithHMD)
		NativeContext["is_mono"]->setInt(1);

	NativeContext["invViewProjectionLeft"]->setMatrix4x4fv(true, &OrthoMatrix.Inverse().M[0][0]);
	NativeContext["viewProjectionLeft"]->setMatrix4x4fv(true, &OrthoMatrix.M[0][0]);

	//FIntPoint Size = OptiXContext->GetBuffer("result_color")->GetSize2D();

	//bIsTracing.AtomicSet(true);
	NativeContext->launch(0, Width, Height);
	//bIsTracing.AtomicSet(false);

	if (Resources[5] == NULL && Resources[6] == NULL)
	{
		return;
	}

	cudaGraphicsMapResources(2, Resources + 5, 0);
	PrintLastCudaError("cudaGraphicsMapResources");

	if (CudaResourceDepthOrtho == NULL)
	{
		cudaGraphicsUnmapResources(2, Resources + 5, 0);
		return;
	}

	// Copy Depth
	cudaArray *CuArrayDepth;
	cudaGraphicsSubResourceGetMappedArray(&CuArrayDepth, CudaResourceDepthOrtho, 0, 0);
	PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

	cudaMemcpy2DToArray(
		CuArrayDepth, // dst array
		0, 0,    // offset
		CudaLinearMemoryDepth, Width * sizeof(float),       // src
		Width * sizeof(float), Height, // extent
		cudaMemcpyDeviceToDevice); // kind
	PrintLastCudaError("cudaMemcpy2DToArray");

	// Copy Color

	cudaArray *CuArrayColor;
	cudaGraphicsSubResourceGetMappedArray(&CuArrayColor, CudaResourceColorOrtho, 0, 0);
	PrintLastCudaError("cudaGraphicsSubResourceGetMappedArray");

	cudaMemcpy2DToArray(
		CuArrayColor, // dst array
		0, 0,    // offset
		CudaLinearMemoryColor, Width * 4 * sizeof(float),       // src
		Width * 4 * sizeof(float), Height, // extent
		cudaMemcpyDeviceToDevice); // kind
	PrintLastCudaError("cudaMemcpy2DToArray");


	cudaGraphicsUnmapResources(2, Resources + 5, 0);
	PrintLastCudaError("cudaGraphicsUnmapResources");

	if (bWithHMD)
		NativeContext["is_mono"]->setInt(0);
}

void FOptiXContextManager::Init()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::Init")

	// TODO Fix me there's still an optix error in there somewhere
	//if (CubemapSampler.IsValid())
	//{
	//	CubemapSampler->RemoveFromRoot();
	//	CubemapSampler->GetNativeTextureSampler()->destroy();
	//	CubemapSampler->MarkPendingKill();
	//	CubemapSampler.Reset();
	//}
	//if (CubemapBuffer.IsValid())
	//{
	//	CubemapBuffer->RemoveFromRoot();
	//	CubemapBuffer->GetNativeBuffer()->destroy();
	//	CubemapBuffer->MarkPendingKill();
	//	CubemapBuffer.Reset();
	//}

	// Probably don't need this at all
	//if (GEngine)
	//{
		//GEngine->ForceGarbageCollection();
	//}

	// Shouldn't be anything in the queues but clean up anyway just to be sure.
	//DestroyOptiXObjects();

	//TODO: Shut this thing down correctly - for now just clean up anything when restarting
	//CleanupOptiXOnEnd();

	bEndPlayReceived = false;
	bValidCubemap = false;
	bIsInitializedCuda = false;
	bIsInitializedLaser = false;
	bIsInitializedAll = false;
	bSceneChanged = true;
	bRequestOrthoPass = false;

	InitRendering();
	InitContext();

	InitBuffers();
	InitPrograms();
	InitLaser();
	InitCubemap();

	InitCUDADX();

	bIsInitializedAll.AtomicSet(true);
	OptiXContextUpdateManager->bIsActive = true;
}

void FOptiXContextManager::SceneChangedCallback()
{
	//bSceneChanged.AtomicSet(true);
}

void FOptiXContextManager::InitContext()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::InitContext")

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Initializing Context in ContextManager"));

	// Needs to be called BEFORE the context is created!
	//rtGlobalSetAttribute(RT_GLOBAL_ATTRIBUTE_ENABLE_RTX, sizeof(RTXOn), &RTXOn);

	NativeContext =	optix::Context::create();

	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_PAYLOAD_ACCESS_OUT_OF_BOUNDS, false);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_USER_EXCEPTION_CODE_OUT_OF_BOUNDS, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_TRACE_DEPTH_EXCEEDED, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_PROGRAM_ID_INVALID, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_TEXTURE_ID_INVALID, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_BUFFER_ID_INVALID, true);
	
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_INDEX_OUT_OF_BOUNDS, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_STACK_OVERFLOW, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_INVALID_RAY, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_INTERNAL_ERROR, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_BUFFER_INDEX_OUT_OF_BOUNDS, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_USER, true);
	//OptiXContext->SetExceptionEnabled(RTexception::RT_EXCEPTION_USER_MAX, true);

	NativeContext->setExceptionEnabled(RTexception::RT_EXCEPTION_ALL, false);

	NativeContext->setPrintEnabled(false);
	//NativeContext->setPrintLaunchIndex(100, 100);
	// Set some default values, they can (and should) be overwritten in the game mode as they're scene specific
	NativeContext->setRayTypeCount(2);
	NativeContext->setEntryPointCount(1);
	//OptiXContext->SetStackSize(4000);
	NativeContext->setMaxTraceDepth(31);

	NativeContext["max_depth"]->setInt(10);
	NativeContext["scene_epsilon"]->setFloat(1.e-2f);

	TopObject = NativeContext->createGroup();
	TopAcceleration = NativeContext->createAcceleration("Trbvh"); // Here the accel structure seems to be actually needed
	TopAcceleration->setProperty("refit", "1");

	TopObject->setAcceleration(TopAcceleration);

	NativeContext["top_object"]->set(TopObject);
	
	NativeContext["is_mono"]->setInt(static_cast<int>(!bWithHMD));
}


void FOptiXContextManager::InitRendering()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::InitRendering")

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Initializing Rendering in ContextManager"));


	// Are we using an HMD?
	if (GEngine->XRSystem.IsValid() && GEngine->XRSystem->GetHMDDevice() != nullptr)
	{
		UE_LOG(OptiXContextManagerLog, Display, TEXT("Got HMD in ContextManager"));

		bWithHMD = GEngine->XRSystem->GetHMDDevice()->IsHMDEnabled();
	}
	else
	{
		UE_LOG(OptiXContextManagerLog, Display, TEXT("Running without HMD in ContextManager"));

		bWithHMD = false;
	}

	// Viewport size:
	FViewport* CurrentViewport = GEngine->GameViewport->Viewport;

	Width = CurrentViewport->GetSizeXY().X / 2.0;
	Height = CurrentViewport->GetSizeXY().Y;

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Got viewport sizes: %i, %i"), Width, Height);
	UE_LOG(OptiXContextManagerLog, Warning, TEXT("Full Res: %i %i"), Width * 2, Height);


	// Apparently those can be 0 in a packaged build? 
	// Catch that case:
	if (Width == 0 || Height == 0)
	{
		UGameUserSettings* GameSettings = GEngine->GetGameUserSettings();
		Width = GameSettings->GetScreenResolution().X;
		Height = GameSettings->GetScreenResolution().Y;
		UE_LOG(OptiXContextManagerLog, Display, TEXT("Fallback to viewport size in settings: %i, %i"), Width, Height);
	}

	// Create the textures:

	OutputTexture2 = UTexture2D::CreateTransient(Width, Height, PF_A32B32G32R32F);
	OutputTexture2->AddToRoot();
	//// Allocate the texture HRI
	OutputTexture2->UpdateResource();

	DepthTexture2 = UTexture2D::CreateTransient(Width, Height, PF_R32_FLOAT);
	DepthTexture2->AddToRoot();
	//// Allocate the texture HRI
	DepthTexture2->UpdateResource();

	if (bWithHMD)
	{
		OutputTexture = UTexture2D::CreateTransient(Width, Height, PF_A32B32G32R32F);
		OutputTexture->AddToRoot();
		//// Allocate the texture HRI
		OutputTexture->UpdateResource();

		DepthTexture = UTexture2D::CreateTransient(Width, Height, PF_R32_FLOAT);
		DepthTexture->AddToRoot();
		//// Allocate the texture HRI
		DepthTexture->UpdateResource();
	}

	OutputTextureOrtho = UTexture2D::CreateTransient(Width, Height, PF_A32B32G32R32F);
	OutputTextureOrtho->AddToRoot();
	//// Allocate the texture HRI
	OutputTextureOrtho->UpdateResource();
	
	DepthTextureOrtho = UTexture2D::CreateTransient(Width, Height, PF_R32_FLOAT);
	DepthTextureOrtho->AddToRoot();
	//// Allocate the texture HRI
	DepthTextureOrtho->UpdateResource();

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Created the Textures"));

	// Laser Texture
	LaserIntersectionTexture = UTexture2D::CreateTransient(LaserBufferWidth, LaserBufferHeight, PF_A32B32G32R32F); // TODO Hardcoded values
	LaserIntersectionTexture->AddToRoot();
	//// Allocate the texture HRI
	LaserIntersectionTexture->UpdateResource();

	// Set up the material

	// Load the materials
	MonoMaterial = LoadObject<UMaterial>(GetTransientPackage(), TEXT("Material'/OptiX/PPMaterials/TextureMaterial.TextureMaterial'"));
	VRMaterial = LoadObject<UMaterial>(GetTransientPackage(), TEXT("Material'/OptiX/PPMaterials/TextureMaterialVR.TextureMaterialVR'"));
	LaserMaterial = LoadObject<UMaterial>(GetTransientPackage(), TEXT("Material'/OptiX/Laser/LaserMaterial.LaserMaterial'"));
	LaserMaterialDynamic = UMaterialInstanceDynamic::Create(LaserMaterial.Get(), GetTransientPackage(), "DynamicLaserMaterial");

	LaserMaterialDynamic->SetTextureParameterValue("IntersectionTexture", LaserIntersectionTexture.Get());
	LaserMaterialDynamic->SetScalarParameterValue("Lines", 50);
	LaserMaterialDynamic->SetScalarParameterValue("Segments", 20);


	if(MonoMaterial == nullptr || VRMaterial == nullptr)
	{
		UE_LOG(OptiXContextManagerLog, Error, TEXT("Couldn't load dummy Material!"));
	}

	if (bWithHMD)
	{
		DynamicMaterial = UMaterialInstanceDynamic::Create(VRMaterial.Get(), GetTransientPackage(), "DynamicVRMaterial");
		DynamicMaterial->SetTextureParameterValue("TextureRight", OutputTexture.Get());
		DynamicMaterial->SetTextureParameterValue("DepthRight", DepthTexture.Get());
		DynamicMaterial->SetTextureParameterValue("TextureLeft", OutputTexture2.Get());
		DynamicMaterial->SetTextureParameterValue("DepthLeft", DepthTexture2.Get());
	}
	else
	{
		DynamicMaterial = UMaterialInstanceDynamic::Create(MonoMaterial.Get(), GetTransientPackage(), "DynamicNonVRMaterial");
		DynamicMaterial->SetTextureParameterValue("Texture", OutputTexture2.Get());
		DynamicMaterial->SetTextureParameterValue("Depth", DepthTexture2.Get());
	}

	DynamicMaterialOrtho = UMaterialInstanceDynamic::Create(MonoMaterial.Get(), GetTransientPackage(), "DynamicOrthoMaterial");
	DynamicMaterialOrtho->SetTextureParameterValue("Texture", OutputTextureOrtho.Get());
	DynamicMaterialOrtho->SetTextureParameterValue("Depth", DepthTextureOrtho.Get());

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Finished Initializing Rendering in ContextManager"));
	FlushRenderingCommands();

}

void FOptiXContextManager::InitBuffers()
{
	const int32 Mult = bWithHMD ? 2 : 1;
	OutputBuffer = NativeContext->createBufferForCUDA(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT4, Width, Height * Mult);
	OutputDepthBuffer = NativeContext->createBufferForCUDA(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT, Width, Height * Mult);

	NativeContext["result_color"]->setBuffer(OutputBuffer);
	NativeContext["result_depth"]->setBuffer(OutputDepthBuffer);
}

void FOptiXContextManager::InitPrograms()
{
	//FString OptiXPTXDir = FOptiXModule::Get().OptiXPTXDir;

	std::string Dir = std::string(TCHAR_TO_ANSI(*OptiXPTXDir));

	// Generation Program
	RayGenerationProgram = NativeContext->createProgramFromPTXFile
	(
		Dir + "generated/perspective_camera.ptx",
		"pinhole_camera"
	);
	NativeContext->setRayGenerationProgram(0, RayGenerationProgram);

	// Exception program
	ExceptionProgram = NativeContext->createProgramFromPTXFile
	(
		Dir + "generated/exception.ptx",
		"exception"
	);
	NativeContext->setExceptionProgram(0, ExceptionProgram);

	// Miss Program
	MissProgram = NativeContext->createProgramFromPTXFile
	(
		Dir + "generated/skybox.ptx",
		"skyboxLookup"
	);
	NativeContext->setMissProgram(0, MissProgram);
	NativeContext["bg_color"]->setFloat(1.0, 1.0, 1.0);
}

void FOptiXContextManager::InitLaser()
{
	std::string Dir = std::string(TCHAR_TO_ANSI(*OptiXPTXDir));

	LaserEntryPoint = NativeContext->getEntryPointCount();

	int32 RayTypeCount = NativeContext->getRayTypeCount();
	NativeContext->setRayTypeCount(RayTypeCount + 1);

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Setting Laser Entry Point to %i"), LaserEntryPoint);
	UE_LOG(OptiXContextManagerLog, Display, TEXT("Setting Ray Type Index to %i"), RayTypeCount);


	// Increase EntryPointCount by 1
	NativeContext->setEntryPointCount(LaserEntryPoint + 1);

	// TODO maybe do this explicitely - loads the same program twice, but at least it's clear which one is used then.

	LaserExceptionProgram = NativeContext->createProgramFromPTXFile
	(
		Dir + "generated/exception.ptx",
		"exception"
	);

	NativeContext->setExceptionProgram(1 /* todo- diff between raytypeindex and entrypointcount, this is 1 in the original app*/, LaserExceptionProgram);

	LaserRayGenerationProgram = NativeContext->createProgramFromPTXFile
	(
		Dir + "generated/laser_caster.ptx",
		"laser_caster"
	);
	NativeContext->setRayGenerationProgram(LaserEntryPoint, LaserRayGenerationProgram);

	LaserMissProgram = NativeContext->createProgramFromPTXFile
	(
		Dir + "generated/miss.ptx",
		"miss_iterative"
	);

	NativeContext->setMissProgram(1 /*LaserEntryPoint /* this is 1 in the original application, why? TODO*/, LaserMissProgram);

	LaserOutputBuffer = NativeContext->createBufferForCUDA(RT_BUFFER_INPUT_OUTPUT | RT_BUFFER_GPU_LOCAL, RT_FORMAT_FLOAT4, LaserBufferWidth, LaserBufferHeight);
	NativeContext["result_laser"]->setBuffer(LaserOutputBuffer);

	NativeContext["max_depth_laser"]->setInt(LaserMaxDepth);

	optix::Buffer LaserIndexBuffer = NativeContext->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_INT, 50, 50);
	optix::Buffer LaserDirectionBuffer = NativeContext->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, 50, 50);
	NativeContext["laserIndex"]->setBuffer(LaserIndexBuffer);
	NativeContext["laserDir"]->setBuffer(LaserDirectionBuffer);
	NativeContext["laserSize"]->setFloat(3.5f); // Hardcode this for now

	bIsInitializedLaser.AtomicSet(true);

}

void FOptiXContextManager::InitCubemap()
{
	// todo max # cubemaps
	for (int32 i = 1; i < 10; i++) // 0 is reserved for this (player camera)
	{
		UnallocatedCubemapIds.Enqueue(i);
	}

	// TODO: Try and see if destroying/creating the whole thing and doing a memcpy on the GPU only is 
	// quicker than updating the cubemap each frame.

	CubemapsInputBuffer = NativeContext->createBuffer(RT_BUFFER_INPUT, RTformat::RT_FORMAT_INT, 10);
	NativeContext["skyboxBuffer"]->setBuffer(CubemapsInputBuffer);
	
	CubemapSampler = NativeContext->createTextureSampler();
	CubemapSampler->setWrapMode(0, RT_WRAP_CLAMP_TO_EDGE);
	CubemapSampler->setWrapMode(1, RT_WRAP_CLAMP_TO_EDGE);
	CubemapSampler->setWrapMode(2, RT_WRAP_CLAMP_TO_EDGE);
	CubemapSampler->setIndexingMode(RT_TEXTURE_INDEX_NORMALIZED_COORDINATES);
	CubemapSampler->setReadMode(RT_TEXTURE_READ_NORMALIZED_FLOAT);
	CubemapSampler->setMaxAnisotropy(1.0f);
	CubemapSampler->setMipLevelCount(1u);
	CubemapSampler->setArraySize(1u);


	CubemapBuffer = NativeContext->createBuffer(RT_BUFFER_INPUT | RT_BUFFER_CUBEMAP, RT_FORMAT_UNSIGNED_BYTE4, 1024, 1024, 6); // 6 slices

	CubemapSampler->setBuffer(0u, 0u, CubemapBuffer);
	CubemapSampler->setFilteringModes(RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE);

	NativeContext["skybox0"]->setInt(CubemapSampler->getId());

	AddCubemapToBuffer(0, CubemapSampler->getId());
	

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Successfully initialized cubemap."));
}

int32 FOptiXContextManager::RequestCubemapId()
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::RequestCubemapId"))

	if (UnallocatedCubemapIds.IsEmpty())
	{
		return 0;
	}
	int32 Id;
	UnallocatedCubemapIds.Dequeue(Id);
	return Id;
}

void FOptiXContextManager::DeleteCubemapId(int32 Id)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::DeleteCubemapId"))

	if (Id <= 10)
	{
		UE_LOG(OptiXContextManagerLog, Warning, TEXT("Trying to free a cubemap that isn't there."));
		return;
	}
	// The Component itself should handle deletion of the sampler.
	UnallocatedCubemapIds.Enqueue(Id);
}

void FOptiXContextManager::UpdateCubemapBuffer(FRHICommandListImmediate & RHICmdList)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::UpdateCubemapBuffer"))

	if (!CameraActor.IsValid() || bValidCubemap)
	{
		return;
	}

	if (!CameraActor->bCubemapCaptured)
	{
		return;
	}


	int32 X = CameraActor->CubeRenderTarget->SizeX;
	int32 Y = X;

	SurfaceDataCube.Empty();
	SurfaceDataCube.SetNumZeroed(6);

	optix::uchar4* BufferData = static_cast<optix::uchar4*>(CubemapBuffer->map());

	FTextureRenderTargetCubeResource* RenderTargetCube = static_cast<FTextureRenderTargetCubeResource*>(CameraActor->CubeRenderTarget->GetRenderTargetResource());

	FIntRect InRectCube = FIntRect(0, 0, RenderTargetCube->GetSizeXY().X, RenderTargetCube->GetSizeXY().Y);
	FReadSurfaceDataFlags FlagsCube0(RCM_UNorm, CubeFace_PosX);
	FReadSurfaceDataFlags FlagsCube1(RCM_UNorm, CubeFace_NegX);
	FReadSurfaceDataFlags FlagsCube2(RCM_UNorm, CubeFace_PosY);
	FReadSurfaceDataFlags FlagsCube3(RCM_UNorm, CubeFace_NegY);
	FReadSurfaceDataFlags FlagsCube4(RCM_UNorm, CubeFace_PosZ);
	FReadSurfaceDataFlags FlagsCube5(RCM_UNorm, CubeFace_NegZ);

	RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[0], FlagsCube0);
	RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[1], FlagsCube1);
	RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[2], FlagsCube2);
	RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[3], FlagsCube3);
	RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[4], FlagsCube4);
	RHICmdList.ReadSurfaceData(RenderTargetCube->GetTextureRHI(), InRectCube, SurfaceDataCube[5], FlagsCube5);

	uint32 MemSize = (X * Y * sizeof(FColor));
	FMemory::Memcpy(BufferData, SurfaceDataCube[0].GetData(), MemSize); // front
	FMemory::Memcpy(BufferData + X * Y * 1, SurfaceDataCube[1].GetData(), MemSize); // back
	FMemory::Memcpy(BufferData + X * Y * 2, SurfaceDataCube[2].GetData(), MemSize); // 
	FMemory::Memcpy(BufferData + X * Y * 3, SurfaceDataCube[3].GetData(), MemSize); // 
	FMemory::Memcpy(BufferData + X * Y * 4, SurfaceDataCube[4].GetData(), MemSize); // 
	FMemory::Memcpy(BufferData + X * Y * 5, SurfaceDataCube[5].GetData(), MemSize); //

	CubemapBuffer->unmap();
	bValidCubemap.AtomicSet(true);
}

void FOptiXContextManager::AddCubemapToBuffer(int32 CubemapId, int32 SamplerId)
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::AddCubemapToBuffer"))

	int32* Data = static_cast<int32*>(CubemapsInputBuffer->map());
	Data[CubemapId] = SamplerId;
	CubemapsInputBuffer->unmap();
}

void FOptiXContextManager::InitCUDADX()
{
	
	// Setup DX:

	check(IsInGameThread());

	{
		FD3D11TextureBase* D3D11TextureDepthLeft = GetD3D11TextureFromRHITexture(DepthTexture2->Resource->TextureRHI);
		cudaGraphicsD3D11RegisterResource(&CudaResourceDepthLeft, D3D11TextureDepthLeft->GetResource(), cudaGraphicsRegisterFlagsNone);
		PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
	}
	{
		FD3D11TextureBase* D3D11TextureColorLeft = GetD3D11TextureFromRHITexture(OutputTexture2->Resource->TextureRHI);
		cudaGraphicsD3D11RegisterResource(&CudaResourceColorLeft, D3D11TextureColorLeft->GetResource(), cudaGraphicsRegisterFlagsNone);
		PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
	}
	if (bWithHMD)
	{
		{
			FD3D11TextureBase* D3D11TextureDepthRight = GetD3D11TextureFromRHITexture(DepthTexture->Resource->TextureRHI);
			cudaGraphicsD3D11RegisterResource(&CudaResourceDepthRight, D3D11TextureDepthRight->GetResource(), cudaGraphicsRegisterFlagsNone);
			PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
		}
		{
			FD3D11TextureBase* D3D11TextureColorRight = GetD3D11TextureFromRHITexture(OutputTexture->Resource->TextureRHI);
			cudaGraphicsD3D11RegisterResource(&CudaResourceColorRight, D3D11TextureColorRight->GetResource(), cudaGraphicsRegisterFlagsNone);
			PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
		}
	}
	// Intersection

	{
		FD3D11TextureBase* D3D11TextureIntersections = GetD3D11TextureFromRHITexture(LaserIntersectionTexture->Resource->TextureRHI);
		cudaGraphicsD3D11RegisterResource(&CudaResourceIntersections, D3D11TextureIntersections->GetResource(), cudaGraphicsRegisterFlagsNone);
		PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
	}

	// Ortho
	{
		FD3D11TextureBase* D3D11TextureOrthoDepth = GetD3D11TextureFromRHITexture(DepthTextureOrtho->Resource->TextureRHI);
		cudaGraphicsD3D11RegisterResource(&CudaResourceDepthOrtho, D3D11TextureOrthoDepth->GetResource(), cudaGraphicsRegisterFlagsNone);
		PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
	}
	{
		FD3D11TextureBase* D3D11TextureOrthoOutput = GetD3D11TextureFromRHITexture(OutputTextureOrtho->Resource->TextureRHI);
		cudaGraphicsD3D11RegisterResource(&CudaResourceColorOrtho, D3D11TextureOrthoOutput->GetResource(), cudaGraphicsRegisterFlagsNone);
		PrintLastCudaError("cudaGraphicsD3D11RegisterResource");
	}
	
	cudaMalloc((void**)&CudaLinearMemoryDepth, Width * Height * sizeof(float) * 2);
	PrintLastCudaError("cudaMalloc");

	cudaMalloc((void**)&CudaLinearMemoryColor, Width * Height * 4 * sizeof(float) * 2);
	PrintLastCudaError("cudaMalloc");

	cudaMalloc(&CudaLinearMemoryIntersections, LaserBufferWidth * LaserBufferHeight * 4 * sizeof(float));
	PrintLastCudaError("cudaMalloc");
	
	NativeContext["result_depth"]->getBuffer()->setDevicePointer(0, CudaLinearMemoryDepth);
	NativeContext["result_color"]->getBuffer()->setDevicePointer(0, CudaLinearMemoryColor);
	NativeContext["result_laser"]->getBuffer()->setDevicePointer(0, CudaLinearMemoryIntersections);

	FString DeviceName = FString(NativeContext->getDeviceName(0).c_str());

	UE_LOG(OptiXContextManagerLog, Display, TEXT("Device Count: %i"), NativeContext->getDeviceCount());
	UE_LOG(OptiXContextManagerLog, Display, TEXT("Device Name 0: %s"), *DeviceName);
	

	Resources[0] = CudaResourceDepthLeft;
	Resources[1] = CudaResourceColorLeft;
	Resources[2] = CudaResourceDepthRight;
	Resources[3] = CudaResourceColorRight;
	Resources[4] = CudaResourceIntersections;
	Resources[5] = CudaResourceColorOrtho;
	Resources[6] = CudaResourceDepthOrtho;

	bIsInitializedCuda.AtomicSet(true);
}

void FOptiXContextManager::RequestNewOptiXBuffer(UniqueId ObjectId, FOptiXBufferData BufferData)
{
	OptiXBuffersToCreate.Enqueue(TPair<UniqueId, FOptiXBufferData>(ObjectId, BufferData));
}


void FOptiXContextManager::RequestNewOptiXGroup(UniqueId ObjectId)
{

}

void FOptiXContextManager::RequestNewOptiXTextureSampler(UniqueId ObjectId)
{
	OptiXTextureSamplersToCreate.Enqueue(ObjectId);
}

void FOptiXContextManager::RequestDestroyOptiXObjects(UniqueId ObjectId)
{
	if(bIsInitializedAll)
		OptiXObjectsToDestroy.Enqueue(ObjectId);
}

void FOptiXContextManager::RequestNewOptiXObject(UniqueId ObjectId, FString Program)
{
	OptiXObjectsToCreate.Enqueue(TPair<UniqueId, FString>(ObjectId, Program));
}

void FOptiXContextManager::RequestLensCubemapUpdate(UniqueId ObjectId, CubemapUpdateFunction UpdateFunction)
{
	CubemapsToUpdate.Enqueue(TPair<UniqueId, CubemapUpdateFunction>(ObjectId, UpdateFunction));
}

void FOptiXContextManager::EnqueueUpdateFunction(UniqueId ObjectId, OptiXObjectUpdateFunction UpdateFunction)
{
	OptiXObjectUpdateFunctions.Enqueue(TPair<UniqueId, OptiXObjectUpdateFunction>(ObjectId, UpdateFunction));
}

void FOptiXContextManager::EnqueueContextUpdateFunction(OptiXContextUpdateFunction UpdateFunction)
{
	OptiXContextUpdateQueue.Enqueue(UpdateFunction);
}

void FOptiXContextManager::EnqueueUpdateFunctionRHI(UniqueId ObjectId, OptiXObjectUpdateFunctionRHI UpdateFunction)
{
	OptiXObjectUpdateFunctionsRHI.Enqueue(TPair<UniqueId, OptiXObjectUpdateFunctionRHI>(ObjectId, UpdateFunction));
}

void FOptiXContextManager::EnqueueInitFunction(UniqueId ObjectId, OptiXObjectInitFunction InitFuntion)
{
	OptiXObjectInitFunctions.Enqueue(TPair<UniqueId, OptiXObjectUpdateFunction>(ObjectId, InitFuntion));
}

void FOptiXContextManager::RegisterLaserTraceCallback(UniqueId ObjectId, LaserTraceFinishedCallback Callback)
{
	LaserTraceFinishedCallbacks.Add(ObjectId, Callback);
}

void FOptiXContextManager::CreateNewOptiXObject(UniqueId ObjectId, FString Program)
{
	// Geometry
	//NativeContext->validate();

	FOptiXObjectData Data;

	Data.OptiXGeometry = NativeContext->createGeometry();
	Data.OptiXGeometry->setPrimitiveCount(1u);


	FString PathGeometryPTX = OptiXPTXDir + "generated/" + Program + ".ptx";

	std::string PG = std::string(TCHAR_TO_ANSI(*PathGeometryPTX));
	optix::Program NewBoundingBoxProg = NativeContext->createProgramFromPTXFile(PG, "bounds");
	optix::Program NewIntersectionProg = NativeContext->createProgramFromPTXFile(PG, "intersect");

	// Material 
	Data.OptiXMaterial = NativeContext->createMaterial();

	FString PathMaterialPTX = OptiXPTXDir + "generated/" + Program + "_material.ptx";

	std::string PM = std::string(TCHAR_TO_ANSI(*PathMaterialPTX));

	optix::Program CHPerspective = NativeContext->createProgramFromPTXFile(PM, "closest_hit_radiance");
	optix::Program CHIterative = NativeContext->createProgramFromPTXFile(PM, "closest_hit_iterative");

	Data.OptiXPrograms =
		TTuple<optix::Program, optix::Program, optix::Program, optix::Program>(NewBoundingBoxProg, NewIntersectionProg, CHPerspective, CHIterative);

	Data.OptiXGeometry->setBoundingBoxProgram(NewBoundingBoxProg);
	Data.OptiXGeometry->setIntersectionProgram(NewIntersectionProg);
	Data.OptiXMaterial->setClosestHitProgram(0, CHPerspective);
	Data.OptiXMaterial->setClosestHitProgram(1, CHIterative);
	// Instance
	Data.OptiXGeometryInstance = NativeContext->createGeometryInstance();
	Data.OptiXGeometryInstance->setGeometry(Data.OptiXGeometry);
	Data.OptiXGeometryInstance->addMaterial(Data.OptiXMaterial);

	// Transform
	Data.OptiXTransform = NativeContext->createTransform();

	// Acceleration
	Data.OptiXAcceleration = NativeContext->createAcceleration("NoAccel");

	Data.OptiXGeometryGroup = NativeContext->createGeometryGroup();
	Data.OptiXGeometryGroup->addChild(Data.OptiXGeometryInstance);
	Data.OptiXGeometryGroup->setAcceleration(Data.OptiXAcceleration);

	Data.OptiXTransform->setChild(Data.OptiXGeometryGroup);

	TopObject->addChild(Data.OptiXTransform);
	//TopObject->getAcceleration()->markDirty();

	OptiXObjectData.Add(ObjectId, Data);
}

void FOptiXContextManager::CreateNewOptiXBuffer(UniqueId ObjectId, FOptiXBufferData BufferData)
{
	if (!OptiXBuffers.Contains(ObjectId))
	{
		OptiXBuffers.Add(ObjectId, TMap<FString, optix::Buffer>());
	}
	optix::Buffer NewBuffer;
	if (BufferData.BufferHeight == 0)
	{
		NewBuffer = NativeContext->createBuffer(BufferData.Type, BufferData.Format, BufferData.BufferWidth);
	}
	else if (BufferData.BufferDepth == 0)
	{
		NewBuffer = NativeContext->createBuffer(BufferData.Type, BufferData.Format, BufferData.BufferWidth, BufferData.BufferHeight);
	}
	else
	{
		NewBuffer = NativeContext->createBuffer(BufferData.Type, BufferData.Format, BufferData.BufferWidth, BufferData.BufferHeight, BufferData.BufferDepth);
	}

	OptiXBuffers[ObjectId].Add(BufferData.Name, NewBuffer);
}

void FOptiXContextManager::CreateNewOptiXTextureSampler(UniqueId ObjectId)
{
	// Find the corresponding sampler
	optix::TextureSampler Sampler = nullptr;
	if (OptiXTextureSamplers.Contains(ObjectId))
	{
		UE_LOG(OptiXContextManagerLog, Warning, TEXT("A Texture Sampler already exists for %i"), ObjectId);
		OptiXTextureSamplers[ObjectId]->destroy();
		OptiXTextureSamplers[ObjectId] = NativeContext->createTextureSampler();
	}
	else
	{
		optix::TextureSampler NewSampler = NativeContext->createTextureSampler();
		OptiXTextureSamplers.Add(ObjectId, NewSampler);
	}
}

void FOptiXContextManager::ExecuteOptiXUpdate(UniqueId ObjectId, OptiXObjectUpdateFunction UpdateFunction)
{
	// Get the required parameters

	optix::TextureSampler Sampler = nullptr;
	TMap<FString, optix::Buffer>* BufferMap = nullptr;
	FOptiXObjectData* Data = nullptr;
	if (OptiXTextureSamplers.Contains(ObjectId))
	{
		Sampler = OptiXTextureSamplers[ObjectId];
	}
	if (OptiXBuffers.Contains(ObjectId))
	{
		BufferMap = &OptiXBuffers[ObjectId];
	}
	if (OptiXObjectData.Contains(ObjectId))
	{
		Data = &OptiXObjectData[ObjectId];
	}
	UpdateFunction(Data, BufferMap, Sampler);
}

void FOptiXContextManager::ExecuteOptiXUpdateRHI(UniqueId ObjectId, FRHICommandListImmediate& RHICmdList, OptiXObjectUpdateFunctionRHI UpdateFunction)
{
	// Get the required parameters

	optix::TextureSampler Sampler = nullptr;
	TMap<FString, optix::Buffer>* BufferMap = nullptr;
	FOptiXObjectData* Data = nullptr;
	if (OptiXTextureSamplers.Contains(ObjectId))
	{
		Sampler = OptiXTextureSamplers[ObjectId];
	}
	if (OptiXBuffers.Contains(ObjectId))
	{
		BufferMap = &OptiXBuffers[ObjectId];
	}
	if (OptiXObjectData.Contains(ObjectId))
	{
		Data = &OptiXObjectData[ObjectId];
	}
	UpdateFunction(Data, BufferMap, Sampler, RHICmdList);
}

void FOptiXContextManager::ExecuteContextUpdate(OptiXContextUpdateFunction UpdateFunction)
{
	UpdateFunction(NativeContext);
}


void FOptiXContextManager::DestroyOptiXObjects(UniqueId ObjectId)
{
	// Does the order matter?

	// Remove buffers first, as they can have no children
	if (OptiXBuffers.Contains(ObjectId))
	{
		TMap<FString, optix::Buffer> Buffers = OptiXBuffers.FindAndRemoveChecked(ObjectId);
		for (TPair<FString, optix::Buffer>& Pair : Buffers)
		{
			Pair.Value->destroy();
			Pair.Value = NULL;
		}
	}

	// Remove callbacks
	if (LaserTraceFinishedCallbacks.Contains(ObjectId))
	{
		LaserTraceFinishedCallbacks.FindAndRemoveChecked(ObjectId);
	}

	// Remove any texture samplers
	if (OptiXTextureSamplers.Contains(ObjectId))
	{
		optix::TextureSampler Sampler = OptiXTextureSamplers.FindAndRemoveChecked(ObjectId);
		Sampler->destroy();
	}

	// Clean up the ObjectData
	if (OptiXObjectData.Contains(ObjectId))
	{
		FOptiXObjectData Data = OptiXObjectData.FindAndRemoveChecked(ObjectId);
		

		TopObject->removeChild(Data.OptiXTransform);

		Data.OptiXTransform->destroy();
		Data.OptiXTransform = NULL;

		Data.OptiXGeometryGroup->removeChild(0);

		Data.OptiXGeometryGroup->destroy();
		Data.OptiXGeometryGroup = NULL;

		Data.OptiXGeometryInstance->destroy();
		Data.OptiXGeometryInstance = NULL;

		Data.OptiXAcceleration->destroy();
		Data.OptiXAcceleration = NULL;

		Data.OptiXMaterial->destroy();
		Data.OptiXMaterial = NULL;

		Data.OptiXGeometry->destroy();
		Data.OptiXGeometry = NULL;

		Data.OptiXPrograms.Get<0>()->destroy();
		Data.OptiXPrograms.Get<0>() = NULL;

		Data.OptiXPrograms.Get<1>()->destroy();
		Data.OptiXPrograms.Get<1>() = NULL;

		Data.OptiXPrograms.Get<2>()->destroy();
		Data.OptiXPrograms.Get<2>() = NULL;

		Data.OptiXPrograms.Get<3>()->destroy();
		Data.OptiXPrograms.Get<3>() = NULL;

		TopAcceleration->markDirty();

	}
	// This should never happen
	if (OptiXGroups.Contains(ObjectId))
	{
		optix::Group Group = OptiXGroups.FindAndRemoveChecked(ObjectId);
		Group->destroy();
	}
}

void FOptiXContextManager::InitializeOptiXObject(UniqueId ObjectId, OptiXObjectInitFunction InitFunction)
{
	// Get the required parameters

	optix::TextureSampler Sampler = nullptr;
	TMap<FString, optix::Buffer>* BufferMap = nullptr;
	FOptiXObjectData* Data = nullptr;
	if (OptiXTextureSamplers.Contains(ObjectId))
	{
		Sampler = OptiXTextureSamplers[ObjectId];
	}
	if (OptiXBuffers.Contains(ObjectId))
	{
		BufferMap = &OptiXBuffers[ObjectId];
	}
	if (OptiXObjectData.Contains(ObjectId))
	{
		Data = &OptiXObjectData[ObjectId];
	}
	InitFunction(Data, BufferMap, Sampler);
}

void FOptiXContextManager::UpdateCubemap(UniqueId ObjectId, FRHICommandListImmediate & RHICmdList, CubemapUpdateFunction UpdateFunction)
{
	if (OptiXBuffers.Contains(ObjectId))
	{
		optix::Buffer Buffer = OptiXBuffers[ObjectId]["texture_buffer"];
		UpdateFunction(Buffer, RHICmdList);
	}		
}


void FOptiXContextManager::ParseCreationQueue()
{
	for (uint32 i = 0; i < 100 && !OptiXObjectsToCreate.IsEmpty(); i++)
	{
		TPair<UniqueId, FString> QueueItem;
		if (OptiXObjectsToCreate.Dequeue(QueueItem))
		{
			CreateNewOptiXObject(QueueItem.Get<0>(), QueueItem.Get<1>());
		}
	}
	for (uint32 i = 0; i < 100 && !OptiXBuffersToCreate.IsEmpty(); i++)
	{
		TPair<UniqueId, FOptiXBufferData> QueueItem;
		if (OptiXBuffersToCreate.Dequeue(QueueItem))
		{
			CreateNewOptiXBuffer(QueueItem.Get<0>(), QueueItem.Get<1>());
		}
	}
	for (uint32 i = 0; i < 100 && !OptiXTextureSamplersToCreate.IsEmpty(); i++)
	{
		UniqueId QueueItem;
		if (OptiXTextureSamplersToCreate.Dequeue(QueueItem))
		{
			CreateNewOptiXTextureSampler(QueueItem);
		}
	}
}
void FOptiXContextManager::ParseInitQueue()
{
	for (uint32 i = 0; i < 100 && !OptiXObjectInitFunctions.IsEmpty(); i++)
	{
		TPair<UniqueId, OptiXObjectInitFunction> QueueItem;
		if (OptiXObjectInitFunctions.Dequeue(QueueItem))
		{
			InitializeOptiXObject(QueueItem.Key, QueueItem.Value);
		}
	}
}
void FOptiXContextManager::ParseUpdateQueue(FRHICommandListImmediate & RHICmdList)
{
	for (uint32 i = 0; i < 100 && !OptiXObjectUpdateFunctions.IsEmpty(); i++)
	{
		TPair<UniqueId, OptiXObjectUpdateFunction> QueueItem;
		if (OptiXObjectUpdateFunctions.Dequeue(QueueItem))
		{
			ExecuteOptiXUpdate(QueueItem.Key, QueueItem.Value);
		}
	}
	for (uint32 i = 0; i < 100 && !OptiXObjectUpdateFunctionsRHI.IsEmpty(); i++)
	{
		TPair<UniqueId, OptiXObjectUpdateFunctionRHI> QueueItem;
		if (OptiXObjectUpdateFunctionsRHI.Dequeue(QueueItem))
		{
			ExecuteOptiXUpdateRHI(QueueItem.Key, RHICmdList, QueueItem.Value);
		}
	}
	for (uint32 i = 0; i < 100 && !OptiXContextUpdateQueue.IsEmpty(); i++)
	{
		OptiXContextUpdateFunction QueueItem;
		if (OptiXContextUpdateQueue.Dequeue(QueueItem))
		{
			ExecuteContextUpdate(QueueItem);
		}
	}
}
void FOptiXContextManager::ParseDestroyQueue()
{
	for (uint32 i = 0; i < 100 && !OptiXObjectsToDestroy.IsEmpty(); i++)
	{
		UniqueId QueueItem;
		if (OptiXObjectsToDestroy.Dequeue(QueueItem))
		{
			DestroyOptiXObjects(QueueItem);
		}
	}
}

void FOptiXContextManager::ParseCubemapUpdateQueue(FRHICommandListImmediate & RHICmdList)
{
	for (uint32 i = 0; i < 100 && !CubemapsToUpdate.IsEmpty(); i++)
	{
		TPair<UniqueId, CubemapUpdateFunction> QueueItem;
		if (CubemapsToUpdate.Dequeue(QueueItem))
		{
			UpdateCubemap(QueueItem.Key, RHICmdList, QueueItem.Value);
		}
	}
}


void FOptiXContextManager::LaserPositionLateUpdate_RenderThread(const FTransform LateUpdateTransform)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::LaserPositionLateUpdate_RenderThread")

	FVector Translation = LateUpdateTransform.GetTranslation();
	FVector Forward = LateUpdateTransform.GetUnitAxis(EAxis::X);
	FVector Right = LateUpdateTransform.GetUnitAxis(EAxis::Y);
	FVector Up = LateUpdateTransform.GetUnitAxis(EAxis::Z);
	FMatrix Rotation = LateUpdateTransform.ToMatrixNoScale();

	// Only late-update the origin. Grabbing something with the motion controller should only change the translational part anyway,
	// NEVER the rotation. 

	NativeContext["laser_origin"]->setFloat(Translation.X, Translation.Y, Translation.Z);
	NativeContext["laser_forward"]->setFloat(Forward.X, Forward.Y, Forward.Z);
	NativeContext["laser_right"]->setFloat(Right.X, Right.Y, Right.Z);
	NativeContext["laser_up"]->setFloat(Up.X, Up.Y, Up.Z);

	NativeContext["laser_rot"]->setMatrix4x4fv(true, &Rotation.M[0][0]);
}

void FOptiXContextManager::ObjectPositionLateUpdate_RenderThread(UniqueId ObjectId, const FMatrix LateUpdateTransform)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("FOptiXContextManager::ObjectPositionLateUpdate_RenderThread")	
	if (OptiXObjectData.Contains(ObjectId))
	{
		FMatrix Update = FMatrix(LateUpdateTransform);
		FMatrix Inverse = Update.Inverse();
		FOptiXObjectData Data = OptiXObjectData[ObjectId];
		Data.OptiXTransform->setMatrix(true, &Update.M[0][0], &Inverse.M[0][0]);
		Data.OptiXAcceleration->markDirty();
		Data.OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();
	}
}

// Cleanup functions

void FOptiXContextManager::CleanupCuda()
{
	TRACE_CPUPROFILER_EVENT_SCOPE(TEXT("FOptiXContextManager::CleanupCuda"))

	if (CudaResourceDepthLeft != NULL)
		cudaGraphicsUnregisterResource(CudaResourceDepthLeft);
	PrintLastCudaError("cudaGraphicsUnregisterResource");

	if (CudaResourceColorLeft != NULL)
		cudaGraphicsUnregisterResource(CudaResourceColorLeft);
	PrintLastCudaError("cudaGraphicsUnregisterResource");

	if (bWithHMD)
	{
		if (CudaResourceDepthRight != NULL)
			cudaGraphicsUnregisterResource(CudaResourceDepthRight);
		PrintLastCudaError("cudaGraphicsUnregisterResource");

		if (CudaResourceColorRight != NULL)
			cudaGraphicsUnregisterResource(CudaResourceColorRight);
		PrintLastCudaError("cudaGraphicsUnregisterResource");
	}
		
	if (CudaResourceDepthOrtho != NULL)
		cudaGraphicsUnregisterResource(CudaResourceDepthOrtho);
	PrintLastCudaError("cudaGraphicsUnregisterResource");

	if (CudaResourceColorOrtho != NULL)
		cudaGraphicsUnregisterResource(CudaResourceColorOrtho);
	PrintLastCudaError("cudaGraphicsUnregisterResource");

	if (CudaResourceIntersections != NULL)
		cudaGraphicsUnregisterResource(CudaResourceIntersections);
	PrintLastCudaError("cudaGraphicsUnregisterResource");

	if (CudaLinearMemoryDepth != NULL)
		cudaFree(CudaLinearMemoryDepth);
	if (CudaLinearMemoryColor != NULL)
		cudaFree(CudaLinearMemoryColor);
	if (CudaLinearMemoryIntersections != NULL)
		cudaFree(CudaLinearMemoryIntersections);
	PrintLastCudaError("cudaFree");
}

void FOptiXContextManager::CleanupLocalOptiXObjects()
{
	if (LaserOutputBuffer != NULL)
	{
		LaserOutputBuffer->destroy();
		LaserOutputBuffer = NULL;
	}
	if (LaserRayGenerationProgram != NULL)
	{
		LaserRayGenerationProgram->destroy();
		LaserRayGenerationProgram = NULL;
	}
	if (LaserMissProgram != NULL)
	{
		LaserMissProgram->destroy();
		LaserMissProgram = NULL;
	}
	if (LaserExceptionProgram != NULL)
	{
		LaserExceptionProgram->destroy();
		LaserExceptionProgram = NULL;
	}


	if (RayGenerationProgram != NULL)
	{
		RayGenerationProgram->destroy();
		RayGenerationProgram = NULL;
	}
	if (MissProgram != NULL)
	{
		MissProgram->destroy();
		MissProgram = NULL;
	}
	if (ExceptionProgram != NULL)
	{
		ExceptionProgram->destroy();
		ExceptionProgram = NULL;
	}

	if (CubemapSampler != NULL)
	{
		CubemapSampler->destroy();
		CubemapSampler = NULL;
	}
	if (CubemapBuffer != NULL)
	{
		CubemapBuffer->destroy();
		CubemapBuffer = NULL;
	}
	if (CubemapsInputBuffer != NULL)
	{
		CubemapsInputBuffer->destroy();
		CubemapsInputBuffer = NULL;
	}

	if (OutputBuffer != NULL)
	{
		OutputBuffer->destroy();
		OutputBuffer = NULL;
	}
	if (OutputDepthBuffer != NULL)
	{
		OutputDepthBuffer->destroy();
		OutputDepthBuffer = NULL;
	}

	if (TopAcceleration != NULL)
	{
		TopAcceleration->destroy();
		TopAcceleration = NULL;
	}
	if (TopObject != NULL)
	{
		TopObject->destroy();
		TopObject = NULL;
	}

	if (NativeContext != NULL)
	{
		NativeContext->destroy();
		NativeContext = NULL;
	}
}

void FOptiXContextManager::Cleanup_RenderThread()
{
	bIsInitializedAll.AtomicSet(false);
	OptiXContextUpdateManager->bIsActive = false;
	CleanupCuda();


	for (auto& Pair : OptiXBuffers)
	{
		for (TPair<FString, optix::Buffer>& BufferPair : Pair.Value)
		{
			BufferPair.Value->destroy();
			BufferPair.Value = NULL;
		}
		Pair.Value.Empty();
	}
	OptiXBuffers.Empty();

	for (auto& Pair : OptiXTextureSamplers)
	{
		Pair.Value->destroy();
		Pair.Value = NULL;
	}
	OptiXTextureSamplers.Empty();

	for (auto& Pair : OptiXObjectData)
	{
		Pair.Value.OptiXTransform->destroy();
		Pair.Value.OptiXTransform = NULL;

		Pair.Value.OptiXGeometryGroup->destroy();
		Pair.Value.OptiXGeometryGroup = NULL;

		Pair.Value.OptiXGeometryInstance->destroy();
		Pair.Value.OptiXGeometryInstance = NULL;

		Pair.Value.OptiXAcceleration->destroy();
		Pair.Value.OptiXAcceleration = NULL;

		Pair.Value.OptiXMaterial->destroy();
		Pair.Value.OptiXMaterial = NULL;

		Pair.Value.OptiXGeometry->destroy();
		Pair.Value.OptiXGeometry = NULL;

		Pair.Value.OptiXPrograms.Get<0>()->destroy();
		Pair.Value.OptiXPrograms.Get<0>() = NULL;

		Pair.Value.OptiXPrograms.Get<1>()->destroy();
		Pair.Value.OptiXPrograms.Get<1>() = NULL;

		Pair.Value.OptiXPrograms.Get<2>()->destroy();
		Pair.Value.OptiXPrograms.Get<2>() = NULL;

		Pair.Value.OptiXPrograms.Get<3>()->destroy();
		Pair.Value.OptiXPrograms.Get<3>() = NULL;
	}
	OptiXObjectData.Empty();

	for (auto& Pair : OptiXGroups)
	{		
		Pair.Value->destroy();
		Pair.Value = NULL;
	}
	OptiXGroups.Empty();

	CleanupLocalOptiXObjects();

	// Empty queues
	OptiXObjectsToCreate.Empty();
	OptiXObjectInitFunctions.Empty();
	OptiXObjectUpdateFunctions.Empty();
	OptiXObjectUpdateFunctionsRHI.Empty();

	OptiXBuffersToCreate.Empty();
	OptiXTextureSamplersToCreate.Empty();
	CubemapsToUpdate.Empty();
	OptiXContextUpdateQueue.Empty();
	OptiXObjectsToDestroy.Empty();
}

void FOptiXContextManager::Cleanup_GameThread()
{
	// Laser stuff
	IntersectionData.Empty();
	OldIntersectionData.Empty();
	LaserIntersectionQueue.Empty();
	PreviousLaserResults.Empty();
	
	LaserIntersectionTexture->RemoveFromRoot();
	LaserMaterialDynamic->RemoveFromRoot();
	LaserMaterial->RemoveFromRoot();

	LaserIntersectionTexture->ConditionalBeginDestroy();
	//LaserMaterialDynamic->ConditionalBeginDestroy();
	//LaserMaterial->ConditionalBeginDestroy();

	LaserIntersectionTexture.Reset();
	LaserMaterialDynamic.Reset();
	LaserMaterial.Reset();

	// Rendering

	if (bWithHMD)
	{
		OutputTexture->RemoveFromRoot();
		DepthTexture->RemoveFromRoot();
	}
	OutputTexture2->RemoveFromRoot();
	DepthTexture2->RemoveFromRoot();
	OutputTextureOrtho->RemoveFromRoot();
	DepthTextureOrtho->RemoveFromRoot();
	DynamicMaterial->RemoveFromRoot();
	DynamicMaterialOrtho->RemoveFromRoot();
	MonoMaterial->RemoveFromRoot();
	VRMaterial->RemoveFromRoot();

	if (bWithHMD)
	{
		OutputTexture->ConditionalBeginDestroy();
		DepthTexture->ConditionalBeginDestroy();
	}
	OutputTexture2->ConditionalBeginDestroy();
	DepthTexture2->ConditionalBeginDestroy();
	OutputTextureOrtho->ConditionalBeginDestroy();
	DepthTextureOrtho->ConditionalBeginDestroy();
	//DynamicMaterial->ConditionalBeginDestroy();
	//DynamicMaterialOrtho->ConditionalBeginDestroy();
	//RegularMaterial->ConditionalBeginDestroy();
	//VRMaterial->ConditionalBeginDestroy();

	OutputTexture.Reset();
	DepthTexture.Reset();
	OutputTexture2.Reset();
	DepthTexture2.Reset();
	OutputTextureOrtho.Reset();
	DepthTextureOrtho.Reset();
	DynamicMaterial.Reset();
	DynamicMaterialOrtho.Reset();
	MonoMaterial.Reset();
	VRMaterial.Reset();

	// Other
	LaserActor.Reset();
	CameraActor.Reset();

	UnallocatedCubemapIds.Empty();
	SurfaceDataCube.Empty();

	LaserTraceFinishedCallbacks.Empty();

}

