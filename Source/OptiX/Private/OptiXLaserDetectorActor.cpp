// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserDetectorActor.h"


#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "OptiXModule.h"
#include "OptiXLaserActor.h"
#include "StatsDefines.h"

#include "Materials/MaterialInstance.h"

#include "ImageUtils.h"
#include "Misc/DateTime.h"
#include "HAL/FileManager.h"
#include "Serialization/BufferArchive.h"



AOptiXLaserDetectorActor::AOptiXLaserDetectorActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Detector Actor Constructor"));

	PrimaryActorTick.bCanEverTick = true;
	bIsEnabled = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/OptiX/Targets/target.target'"));
	UStaticMesh* Asset = MeshAsset.Object;

	GetStaticMeshComponent()->SetStaticMesh(Asset);
	//SetActorEnableCollision(false);

	OptiXLaserTargetComponent = CreateDefaultSubobject<UOptiXLaserTargetComponent>(TEXT("LaserTargetComponent"));
	OptiXLaserTargetComponent->SetupAttachment(GetStaticMeshComponent());

	HighlightColor = FColor(255, 255, 255);

}

void AOptiXLaserDetectorActor::BeginPlay()
{
	Super::BeginPlay();
	Init();
}

void AOptiXLaserDetectorActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	// Might need to remove the delegate thingy here?
}

void AOptiXLaserDetectorActor::Tick(float DeltaTime)
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::Tick")

	Super::Tick(DeltaTime);
	//OnLaserTraceFinished(); // todo
}

void AOptiXLaserDetectorActor::Init()
{
	int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);


	TextureRegion = MakeUnique<FUpdateTextureRegion2D>();
	TextureRegion->Height = Size;
	TextureRegion->Width = Size;
	TextureRegion->SrcX = 0;
	TextureRegion->SrcY = 0;
	TextureRegion->DestX = 0;
	TextureRegion->DestY = 0;


	// Subscribe to the laser actor:
	TArray<AActor*> FoundLaserActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AOptiXLaserActor::StaticClass(), FoundLaserActors);
	
	//FOptiXModule::Get().GetOptiXContextManager()->LaserTraceFinishedEvent.AddUFunction(this, "OnLaserTraceFinished");

	// Setup DIM

	DynamicScreenMaterial = UMaterialInstanceDynamic::Create(GetStaticMeshComponent()->GetMaterial(1), this);
	GetStaticMeshComponent()->SetMaterial(1, DynamicScreenMaterial);

	// Set up the texture

	DetectorResultTexture = UTexture2D::CreateTransient(Size, Size, PF_R32_FLOAT);
	//OutputTexture->AddToRoot();
	//// Allocate the texture HRI
	DetectorResultTexture->UpdateResource();

	ResultRenderTarget = NewObject<UTextureRenderTarget2D>();
	ResultRenderTarget->InitCustomFormat(512, 512, EPixelFormat::PF_B8G8R8A8, true);
	ResultRenderTarget->bAutoGenerateMips = 0;
	//ResultRenderTarget->SRGB = 0;

	//ResultRenderTarget->bForceLinearGamma = 1;
	//ResultRenderTarget->OverrideFormat = EPixelFormat::PF_R8G8B8A8;
	//ResultRenderTarget->RenderTargetFormat = ETextureRenderTargetFormat::RTF_RGBA8;
	//ResultRenderTarget->SizeX = 512;
	//ResultRenderTarget->SizeY = 512;



	LaserTraceFinishedCallback Callback =
		[&MaxRef = Max, &bIsColorMode = bIsColorMode, &HighlightColor = HighlightColor, TargetRes = Size, &DynamicScreenMaterial = DynamicScreenMaterial, TextureRegion = TextureRegion.Get(), DetectorResultTexture = DetectorResultTexture]
	(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler, FRHICommandListImmediate& RHICmdList)
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::LaserTraceFinishedCallback")

			// Get Max
		float Max;
		{
			TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::LaserTraceFinishedCallback::GetMax")

			optix::Buffer BufferMax = *Buffers->Find("target_buffer_max");
			Max = (float)*static_cast<unsigned int*>(BufferMax->map(0, RT_BUFFER_MAP_READ));
			BufferMax->unmap();
			if (Max == 0)
			{
				Max = 1; // TODO WHY?
			}
		}
		MaxRef = Max;
		//UE_LOG(LogTemp, Display, TEXT("MAX: %f"), Max);

		FTexture2DRHIRef TextureRef = ((FTexture2DResource*)DetectorResultTexture->Resource)->GetTexture2DRHI();

		{
			TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::LaserTraceFinishedCallback::GetTargetBuffer")

			optix::Buffer Buffer = *Buffers->Find("target_buffer");
			float* DataPtr = static_cast<float*>(Buffer->map(0, RT_BUFFER_MAP_READ));

			//float M2 = 0;

			//for (int32 i = 0; i < DetectorResultTexture->GetSizeX(); ++i)
			//{
			//	for (int32 j = 0; j < DetectorResultTexture->GetSizeY(); ++j)
			//	{
			//		M2 = FMath::Max(M2, DataPtr[i * j]);
			//	}
			//}
			//UE_LOG(LogTemp, Display, TEXT("MAX2: %f"), M2);


			//DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion, TargetRes * 4, 4, (uint8*)DataPtr);
			{
				TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::LaserTraceFinishedCallback::GetTargetBuffer::UpdateTexture2D")

				RHICmdList.UpdateTexture2D(TextureRef, 0, *TextureRegion, TargetRes * 4, (uint8*)DataPtr);
			}
			Buffer->unmap();
		}
		// todo
		//DynamicScreenMaterial->Resource->RenderThread_UpdateParameter()

		if (DynamicScreenMaterial != nullptr)
		{
			TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::LaserTraceFinishedCallback::UpdateMaterial")

			DynamicScreenMaterial->SetScalarParameterValue("ColorMode", bIsColorMode ? 1.0f : 0.0f);
			DynamicScreenMaterial->SetVectorParameterValue("ColorMode", HighlightColor);
			DynamicScreenMaterial->SetTextureParameterValue("ResultTexture", DetectorResultTexture);
			DynamicScreenMaterial->SetScalarParameterValue("ResultMax", Max);
		}
	};
	FOptiXModule::Get().GetOptiXContextManager()->RegisterLaserTraceCallback(OptiXLaserTargetComponent->GetUniqueID(), Callback);
	//FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
}

void AOptiXLaserDetectorActor::OnLaserTraceFinished()
{
	//UE_LOG(LogTemp, Warning, TEXT("lasertracefinished"));
	TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::OnLaserTraceFinished")

	if (bIsEnabled)
	{
		//OptiXLaserTargetComponent->UpdateBufferData();
		RenderDataToTarget();
	}
}

void AOptiXLaserDetectorActor::RenderDataToTarget()
{		
	TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::RenderDataToTarget")
			   
	//int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);

	//Max = OptiXLaserTargetComponent->GetMaxFromBuffer();

	//float* Data = static_cast<float*>(OptiXLaserTargetComponent->TargetBuffer->MapNative(0, RT_BUFFER_MAP_READ));
	//DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)Data);
	//OptiXLaserTargetComponent->TargetBuffer->Unmap();



	//int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);
	//DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)OptiXLaserTargetComponent->ColorData.GetData());

	//if (DynamicScreenMaterial != nullptr)
	//{
	//	DynamicScreenMaterial->SetTextureParameterValue("Texture", DetectorResultTexture);
	//}

	//if (DynamicScreenMaterial != nullptr)
	//{
	//	DynamicScreenMaterial->SetScalarParameterValue("ColorMode", bIsColorMode ? 1.0f : 0.0f);
	//	DynamicScreenMaterial->SetVectorParameterValue("ColorMode", HighlightColor);
	//}




	//FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunctionRHI(OptiXLaserTargetComponent->GetUniqueID(), UpdateFunction);

}

void AOptiXLaserDetectorActor::Clear()
{
	TRACE_CPUPROFILER_EVENT_SCOPE("AOptiXLaserDetectorActor::Clear")

		//uint32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);

		//TArray<FColor> Black;
		//Black.AddZeroed(Size * Size);

		//DetectorResultTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)Black.GetData());

		//if (DynamicScreenMaterial != nullptr)
		//{
		//	DynamicScreenMaterial->SetTextureParameterValue("ResultTexture", DetectorResultTexture);
		//	DynamicScreenMaterial->SetScalarParameterValue("ResultMax", 0);
		//}
	Max = 0.0f;
	OptiXLaserTargetComponent->ClearOptiXBuffer();
}

void AOptiXLaserDetectorActor::Save()
{
	//// Hope this makes a copy - it doesn't
	//UTexture2D* NewTexture = UTexture2D::CreateTransient(DetectorResultTexture->GetSizeX(), DetectorResultTexture->GetSizeY(), DetectorResultTexture->GetPixelFormat());
	//NewTexture->UpdateResource();
	//
	//int32 Size = static_cast<int32>(OptiXLaserTargetComponent->TargetRes);
	////Max = OptiXLaserTargetComponent->GetMaxFromBuffer();

	//float* Data = static_cast<float*>(OptiXLaserTargetComponent->TargetBuffer->MapNative(0, RT_BUFFER_MAP_READ));
	//NewTexture->UpdateTextureRegions(0, 1, TextureRegion.Get(), Size * 4, 4, (uint8*)Data);
	//OptiXLaserTargetComponent->TargetBuffer->Unmap();

	//SavedDetectorTextures.Add(NewTexture);


	// For some reason just calling the export to png method doesn't save it as a working png.
	// This is ugly, but read data manually.

	FString SavedDir = FPaths::ProjectSavedDir();
	FDateTime Time = FDateTime::Now();

	FString FileName;
	FileName.Append("DetectorScreen_");
	FileName.Append(FString::Printf(TEXT("%d-%d_%d-%d.bmp"), Time.GetMonth(), Time.GetDay(), Time.GetHour(), Time.GetMinute()));
	FString Path = SavedDir / FileName;


	TArray<FColor> Pixels;

	FTextureRenderTarget2DResource* Resource = (FTextureRenderTarget2DResource*)ResultRenderTarget->Resource;

	if (Resource != NULL)
	{
		if (Resource->ReadPixels(Pixels))
		{
			UE_LOG(LogTemp, Display, TEXT("Successfully read pixel data from detector."));
			FFileHelper::CreateBitmap(
				*Path,
				ResultRenderTarget->SizeX,
				ResultRenderTarget->SizeY,
				Pixels.GetData(), //const struct FColor* Data, 
				nullptr,//struct FIntRect* SubRectangle = NULL, 
				&IFileManager::Get(),
				nullptr, //out filename info only 
				false //bool bInWriteAlpha 
			);
			UE_LOG(LogTemp, Display, TEXT("Saved Detector Texture to %s."), *Path);
			return;
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("Grabbed detector texture resource, but failed to read pixels."));
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("Failed to grab detector texture resource."));

	UE_LOG(LogTemp, Warning, TEXT("Failed to save detector texture."));
}
