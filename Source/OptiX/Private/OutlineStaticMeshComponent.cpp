// Fill out your copyright notice in the Description page of Project Settings.


#include "OutlineStaticMeshComponent.h"
#include "PrimitiveSceneProxy.h"
#include "StaticMeshResources.h"
#include "Engine/StaticMesh.h"


FPrimitiveSceneProxy* UOutlineStaticMeshComponent::CreateSceneProxy()
{

	class FOutlineStaticMeshProxy final : public FStaticMeshSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		/** Initialization constructor. */
		FOutlineStaticMeshProxy(UOutlineStaticMeshComponent* InComponent)
			: FStaticMeshSceneProxy(InComponent, false)
		{}

		virtual void ApplyLateUpdateTransform(const FMatrix& LateUpdateTransform) override
		{
			// As the SetTransform function is private, this needs to be a little hacky

			FMatrix NewTransform = GetLocalToWorld() * LateUpdateTransform;
			FVector OriginChange = NewTransform.GetOrigin() - GetLocalToWorld().GetOrigin();
			FMatrix ReducedLateUpdate = FMatrix::Identity;
			ReducedLateUpdate.SetOrigin(OriginChange);

			FStaticMeshSceneProxy::ApplyLateUpdateTransform(ReducedLateUpdate);
		}
	};

	if (GetStaticMesh() == nullptr || GetStaticMesh()->RenderData == nullptr)
	{
		return nullptr;
	}

	const TIndirectArray<FStaticMeshLODResources>& LODResources = GetStaticMesh()->RenderData->LODResources;
	if (LODResources.Num() == 0 || LODResources[FMath::Clamp<int32>(GetStaticMesh()->MinLOD.Default, 0, LODResources.Num() - 1)].VertexBuffers.StaticMeshVertexBuffer.GetNumVertices() == 0)
	{
		return nullptr;
	}
	LLM_SCOPE(ELLMTag::StaticMesh);

	FPrimitiveSceneProxy* Proxy = ::new FOutlineStaticMeshProxy(this);
#if STATICMESH_ENABLE_DEBUG_RENDERING
	SendRenderDebugPhysics(Proxy);
#endif

	return Proxy;
}