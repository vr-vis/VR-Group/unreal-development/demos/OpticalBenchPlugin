// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLensActor.h"

#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"

AOptiXLensActor::AOptiXLensActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Target Actor Constructor"));

	PrimaryActorTick.bCanEverTick = false;

	//static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/OptiX/target.target'"));
	//UStaticMesh* Asset = MeshAsset.Object;

	//GetStaticMeshComponent()->SetStaticMesh(Asset);
	//SetActorEnableCollision(false);

	OptiXLensComponent = CreateDefaultSubobject<UOptiXLensComponent>(TEXT("LensComponent"));
	OptiXLensComponent->SetupAttachment(RootComponent);

	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	SetActorLocationAndRotation({ 0, 0, OptiXLensComponent->GetLensRadius() }, FRotationMatrix::MakeFromZ({ 1, 0, 0 }).ToQuat());

	//RootComponent = OptiXTargetComponent;
	//SM->SetupAttachment(OptiXTargetComponent);
}

