// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserActor.h"

#include "OptixModule.h"

#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"



AOptiXLaserActor::AOptiXLaserActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Actor Constructor"));

	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UStaticMesh>MeshAsset(TEXT("StaticMesh'/OptiX/Laser/laser3.laser3'"));
	UStaticMesh* Asset = MeshAsset.Object;

	GetStaticMeshComponent()->SetStaticMesh(Asset);
	//SetActorEnableCollision(false);

	// Okay, after a quick search there seem to be two/three ways to draw the laser lines that aren't completely idiotic:
	// 1. Use the line dispatcher (apparently it is *NOT* performant in production, tho I have not found why. 
	// 2. Use some kind of instanced mesh component that draws maybe a very thin cylinder with the respective transforms adjusted for each laser beam. 
	//    This would work well if there was some kind of line mesh, but there seems to be none.
	//    The most promising thing seems to be a simple (custom) line mesh with 2 vertices and a material, which then gets transformed to fit between each intersection.
	//    Then for each segment, an instanced mesh gets drawn with the respective transform.
	// TODO: Maybe look into how the spline mesh component gets drawn - maybe it's possible to use a simple instanced mesh (no cylinder) and do some fancy shader magic in the material.
	// 3. Use the procedural mesh component - I am however not sure how this works with instanced drawings, and might generate a loooot of draw calls (50*50 max possible rays are a lot of draw calls!)
	// (https://wiki.unrealengine.com/index.php?title=Procedural_Mesh_Component_in_C%2B%2B:Getting_Started)
	// For now, use the line dispatcher to debug things:

	//static ConstructorHelpers::FObjectFinder<UStaticMesh>LineMeshAsset(TEXT("StaticMesh'/OptiX/Laser/cylinder.cylinder'"));
	//UStaticMesh* LineAsset = LineMeshAsset.Object;
	//LineAsset->

	LineInstancedStaticMeshComponent = CreateDefaultSubobject<ULineInstancedStaticMeshComponent>(TEXT("LineInstancedStaticMeshComponent"));
	LineInstancedStaticMeshComponent->SetupAttachment(GetStaticMeshComponent());
	
	OptiXLaserComponent = CreateDefaultSubobject<UOptiXLaserComponent>(TEXT("LaserComponent"));
	OptiXLaserComponent->SetupAttachment(LineInstancedStaticMeshComponent);

	bLaserTraceEnabled = true;

}

void AOptiXLaserActor::BeginPlay()
{
	Super::BeginPlay();

	FOptiXModule::Get().GetOptiXContextManager()->SetActiveLaserActor(this);
	
	LineInstancedStaticMeshComponent->InitLineSegments(OptiXLaserComponent->LaserIndices, 20);
	LineInstancedStaticMeshComponent->UpdateLUT(OptiXLaserComponent->LaserIndexColorMap);


}

void AOptiXLaserActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Actor EndPlay")); 
}


void AOptiXLaserActor::UpdateLaserPattern(EPatternTypes Pattern)
{

	OptiXLaserComponent->PreparePatternChange(Pattern);
	LineInstancedStaticMeshComponent->InitLineSegments(OptiXLaserComponent->LaserIndices, 20);
	OptiXLaserComponent->SetLaserPattern(Pattern);

	UE_LOG(LogTemp, Display, TEXT("#Colors: %i"), OptiXLaserComponent->LaserIndexColorMap.Num());


	LineInstancedStaticMeshComponent->UpdateLUT(OptiXLaserComponent->LaserIndexColorMap);
}

void AOptiXLaserActor::UpdateLaserWidth(float NewWidth)
{
	OptiXLaserComponent->SetLaserWidth(NewWidth);
	LineInstancedStaticMeshComponent->DynamicLaserMaterial->SetScalarParameterValue("Width", NewWidth);
}
