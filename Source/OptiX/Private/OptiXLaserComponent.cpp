// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXLaserComponent.h"

#include "UObject/ConstructorHelpers.h"

#include "OptiXModule.h"
#include "StatsDefines.h"


// Sets default values for this component's properties
UOptiXLaserComponent::UOptiXLaserComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false; 
	bWantsOnUpdateTransform = false; // why the hell do I need this here but not on the others?

	// ...

	LaserMaxDepth = 20;
	LaserEntryPoint = 1; // Default, will be overwritten anyway

	LaserBufferWidth = 50 * 50;
	LaserBufferHeight = LaserMaxDepth * 2;

	LaserBufferSize = LaserBufferHeight * LaserBufferWidth;


	RayTIR = false;
	TargetBufferWrite = 1; //todo
	TargetColorMode = 0; // todo
	Wavelength = 450.0f;
	LaserWidth = 0.025f;
	CurrentLaserPattern = EPatternTypes::POINTER;
	LaserTracesPerFrame = 20;

	// Find and load default patterns:

	static ConstructorHelpers::FObjectFinder<UTexture2D> CrossTexture(TEXT("Texture2D'/OptiX/Laser/Cross.Cross'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrossTextureDir(TEXT("Texture2D'/OptiX/Laser/Cross_dir.Cross_dir'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> PointerTexture(TEXT("Texture2D'/OptiX/Laser/Pointer.Pointer'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> PointerTextureDir(TEXT("Texture2D'/OptiX/Laser/Pointer_dir.Pointer_dir'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> CircleTexture(TEXT("Texture2D'/OptiX/Laser/Circle.Circle'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> CircleTextureDir(TEXT("Texture2D'/OptiX/Laser/Circle_dir.Circle_dir'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> QuadTexture(TEXT("Texture2D'/OptiX/Laser/Quad.Quad'"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> QuadTextureDir(TEXT("Texture2D'/OptiX/Laser/Quad_dir.Quad_dir'"));



	if (CrossTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::CROSS, CrossTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Cross Pattern."));
	}
	if (CrossTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::CROSS, CrossTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Cross Dir Pattern."));
	}
	if (PointerTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::POINTER, PointerTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Pointer Pattern."));
	}
	if (PointerTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::POINTER, PointerTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Pointer Dir Pattern."));
	}	
	if (CircleTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::CIRCLE, CircleTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Circle Pattern."));
	}	
	if (CircleTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::CIRCLE, CircleTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Circle Dir Pattern."));
	}
	if (QuadTexture.Object != NULL)
	{
		Patterns.Add(EPatternTypes::QUAD, QuadTexture.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Quad Pattern."));
	}
	if (QuadTextureDir.Object != NULL)
	{
		PatternDirections.Add(EPatternTypes::QUAD, QuadTextureDir.Object);
		UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Added Quad Dir Pattern."));
	}	

	// Init the # of rays and their indices

	// This crashes on packaging for some reason, apparently Mips has length 0.

	if (Patterns[CurrentLaserPattern]->PlatformData->Mips.Num() != 0)
	{
		FTexture2DMipMap& IndexMip = Patterns[CurrentLaserPattern]->PlatformData->Mips[0];

		UE_LOG(LogTemp, Display, TEXT("Got Texture mips"));


		FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_WRITE));


		int X = Patterns[CurrentLaserPattern]->GetSizeX();
		int Y = Patterns[CurrentLaserPattern]->GetSizeY();


		// Save the texture indices for the direction:

		// Texture index conversion is a real pain...
		for (int32 i = 0; i < X; ++i) {
			for (int32 j = 0; j < Y; ++j) {

				int32 TextureIndex = (X * Y - 1) - i * X - j;
				int32 BufferIndex = ((j)*(X)+i);

				if (TextureData[TextureIndex].R || TextureData[TextureIndex].G || TextureData[TextureIndex].B)
				{
					LaserIndices.Add(BufferIndex);
					LaserIndexColorMap.Add(TextureData[TextureIndex]);
				}
			}
		}

		IndexMip.BulkData.Unlock();

		UE_LOG(LogTemp, Display, TEXT("Finished Laser Component texture conversion."));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Texture Mip array is empty in OptiXLaserComponent Constructor. Skipping laser texture reading, hopefully fixed on beginplay()."));
	}
}


FPrimitiveSceneProxy* UOptiXLaserComponent::CreateSceneProxy()
{
	class FLaserComponentSceneProxy final : public FPrimitiveSceneProxy
	{
	public:
		SIZE_T GetTypeHash() const override
		{
			static size_t UniquePointer;
			return reinterpret_cast<size_t>(&UniquePointer);
		}

		/** Initialization constructor. */
		FLaserComponentSceneProxy(const UOptiXLaserComponent* InComponent)
			: FPrimitiveSceneProxy(InComponent)
		{}

		// FPrimitiveSceneProxy interface.

		virtual void ApplyLateUpdateTransform(const FMatrix& LateUpdateTransform) override
		{
			FMatrix CachedTransform = GetLocalToWorld();
			FPrimitiveSceneProxy::ApplyLateUpdateTransform(LateUpdateTransform);
			CachedTransform.SetOrigin(GetLocalToWorld().GetOrigin());
			FTransform UpdatedTransform(CachedTransform.GetMatrixWithoutScale());
			//UE_LOG(LogTemp, Display, TEXT("Transform on late update: %s"), *UpdatedTransform.ToString());
			FOptiXModule::Get().GetOptiXContextManager()->LaserPositionLateUpdate_RenderThread(UpdatedTransform);
		}

		virtual uint32 GetMemoryFootprint(void) const override { return(sizeof(*this) + GetAllocatedSize()); }
		uint32 GetAllocatedSize(void) const { return(FPrimitiveSceneProxy::GetAllocatedSize()); }
	};

	return new FLaserComponentSceneProxy(this);
}


// Called when the game starts
void UOptiXLaserComponent::BeginPlay()
{
	Super::BeginPlay();
	bWantsOnUpdateTransform = true;

	UE_LOG(LogTemp, Display, TEXT("Initializing OptiX Laser"));

	SetLaserPattern(CurrentLaserPattern);

	SetRayTIR(RayTIR);
	SetTargetBufferWrite(TargetBufferWrite);
	SetTargetColorMode(TargetColorMode);
	SetWavelength(Wavelength);
	SetLaserWidth(LaserWidth);

	FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
	//FOptiXModule::Get().GetOptiXContextManager()->bIsInitializedLaser.AtomicSet(true);

	UpdateLaserPosition();

}

void UOptiXLaserComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Component EndPlay"));

	CleanOptiXObjects();
}

void UOptiXLaserComponent::CleanOptiXObjects()
{
	UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Component Cleaning up")); // TODO
	FOptiXModule::Get().GetOptiXContextManager()->RequestDestroyOptiXObjects(GetUniqueID());
}


void UOptiXLaserComponent::OnUpdateTransform(EUpdateTransformFlags UpdateTransformFlags, ETeleportType Teleport/* = ETeleportType::None*/)
{
	Super::OnUpdateTransform(EUpdateTransformFlags::SkipPhysicsUpdate, Teleport);
	UpdateLaserPosition();
}

void UOptiXLaserComponent::SetRayTIR(bool Active)
{
	RayTIR = Active;
	OptiXContextUpdateFunction UpdateFunction =
		[RayTIR = RayTIR](optix::Context Context)
	{
		Context["allowTir"]->setInt(static_cast<int>(RayTIR));
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);
}

bool UOptiXLaserComponent::GetRayTIR() const
{
	return RayTIR;
}

void UOptiXLaserComponent::SetTargetBufferWrite(bool Flag)
{
	TargetBufferWrite = Flag;
	OptiXContextUpdateFunction UpdateFunction =
		[TargetBufferWrite = TargetBufferWrite](optix::Context Context)
	{
		Context["targetBufferWrite"]->setInt(static_cast<int>(TargetBufferWrite));
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);
}

bool UOptiXLaserComponent::GetTargetBufferWrite() const
{
	return TargetBufferWrite;
}

void UOptiXLaserComponent::SetTargetColorMode(bool Flag)
{
	TargetColorMode = Flag;
	//OptiXContextUpdateFunction UpdateFunction =
	//	[TargetBufferWrite = TargetBufferWrite](optix::Context Context)
	//{
	//	Context["targetBufferWrite"]->setInt(static_cast<int>(TargetBufferWrite));
	//};

	//FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);
}

bool UOptiXLaserComponent::GetTargetColorMode() const
{
	return TargetColorMode;
}

void UOptiXLaserComponent::SetWavelength(float WL)
{
	Wavelength = WL;
	FOptiXModule::Get().GetOptiXContextManager()->BroadcastWavelengthChange(WL);
	OptiXContextUpdateFunction UpdateFunction =
		[Wavelength = Wavelength](optix::Context Context)
	{
		Context["laserWaveLength"]->setInt(static_cast<int>(Wavelength));
	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);
}

float UOptiXLaserComponent::GetWavelength() const
{
	return Wavelength;
}

void UOptiXLaserComponent::SetLaserWidth(float Width)
{
	LaserWidth = Width;
	OptiXContextUpdateFunction UpdateFunction =
		[LaserWidth = LaserWidth](optix::Context Context)
	{
		Context["laserBeamWidth"]->setFloat(LaserWidth);
	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);
}

float UOptiXLaserComponent::GetLaserWidth() const
{
	return LaserWidth;
}

void UOptiXLaserComponent::SetLaserPattern(EPatternTypes Pattern)
{

	/*
	The original way of setting up the buffer and pattern seems a bit wasteful, as the whole 50 * 50 * 20 buffer gets allocated,
	even tho only a very tiny amount of rays actually get cast. For now, keep it that way on the optix side, but actually save the 
	number of rays and their index here, so we don't have to copy and draw the whole result buffer. 
	*/

	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Queuing Laser Pattern Change"));

	CurrentLaserPattern = Pattern;
	UE_LOG(LogTemp, Display, TEXT("New Pattern queued: %i"), static_cast<uint8>(CurrentLaserPattern));


	int X = Patterns[CurrentLaserPattern]->GetSizeX();
	int Y = Patterns[CurrentLaserPattern]->GetSizeY();

	UTexture2D* NewPattern = Patterns[CurrentLaserPattern];
	UTexture2D* NewDirectionPattern = PatternDirections[CurrentLaserPattern];

	OptiXContextUpdateFunction UpdateFunction =
		[X, Y, NewPattern, NewDirectionPattern](optix::Context Context)
	{
		TRACE_CPUPROFILER_EVENT_SCOPE("UOptiXLaserComponent::LaserPatternUpdate")
		//UE_LOG(LogTemp, Display, TEXT("Changing to new laser pattern: %i"), static_cast<uint8>(CurrentLaserPattern));

		{
			optix::Buffer LaserIndexBuffer = Context["laserIndex"]->getBuffer();

			int32* BufferIndexData = static_cast<int32*>(LaserIndexBuffer->map(0, RT_BUFFER_MAP_WRITE));
			// Reset the buffer explicitly 
			FMemory::Memset(BufferIndexData, 0u, X * Y * 4);

			FTexture2DMipMap& IndexMip = NewPattern->PlatformData->Mips[0];

			FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_WRITE));

			TArray<int32> TextureIndices;

			// Texture index conversion is a real pain...
			for (int32 i = 0; i < X; ++i) {
				for (int32 j = 0; j < Y; ++j) {

					int32 TextureIndex = (X * Y - 1) - i * X - j;
					int32 BufferIndex = ((j)*(X)+i);

					BufferIndexData[BufferIndex] = 0;

					if (TextureData[TextureIndex].R || TextureData[TextureIndex].G || TextureData[TextureIndex].B)
					{
						BufferIndexData[BufferIndex] = 1;
						//LaserIndices.Add(BufferIndex);
						TextureIndices.Add(TextureIndex);
						//LaserIndexColorMap.Add(BufferIndex, TextureData[TextureIndex]);
					}
					else
					{
						BufferIndexData[BufferIndex] = -1; // do not cast ray
					}
				}
			}

			UE_LOG(LogTemp, Display, TEXT("# Ray Indices in Pattern %i"), static_cast<uint8>(TextureIndices.Num()));

			IndexMip.BulkData.Unlock();
			LaserIndexBuffer->unmap();
		}
		{
			optix::Buffer LaserDirectionBuffer = Context["laserDir"]->getBuffer();
			float* BufferDirectionData = static_cast<float*>(LaserDirectionBuffer->map(0, RT_BUFFER_MAP_WRITE));

			FTexture2DMipMap& DirectionMip = NewDirectionPattern->PlatformData->Mips[0];

			FColor* TextureData = static_cast<FColor*>(DirectionMip.BulkData.Lock(LOCK_READ_WRITE));

			// Texture index conversion is a real pain...
			for (int32 i = 0; i < X; ++i) {
				for (int32 j = 0; j < Y; ++j) {

					int32 TextureIndex = (X * Y - 1) - i * X - j;
					int32 BufferIndex = ((j)*(X)+i) * 3;

					//UE_LOG(LogTemp, Display, TEXT("Values: %i"), TextureData[BufferIndex]);

					FVector DirNormalSpace = FVector();
					DirNormalSpace.X = (TextureData[TextureIndex].R / 256.0f) * 2.0f - 1.0f;
					DirNormalSpace.Y = (TextureData[TextureIndex].G / 256.0f) * 2.0f - 1.0f;
					DirNormalSpace.Z = (TextureData[TextureIndex].B / 256.0f);
					DirNormalSpace.Normalize();

					// We need to rotate this to face forward (1, 0, 0):

					//FMatrix Mat = FRotationMatrix::MakeFromX(FVector(0, 0, 1));
					//FVector Dir = Mat.TransformVector(DirNormalSpace);

					FVector Dir;
					Dir.X = DirNormalSpace.Z;
					Dir.Y = DirNormalSpace.Y;
					Dir.Z = DirNormalSpace.X;

					BufferDirectionData[BufferIndex] = Dir.X;
					BufferDirectionData[BufferIndex + 1] = Dir.Y;
					BufferDirectionData[BufferIndex + 2] = Dir.Z;

				}
			}
			DirectionMip.BulkData.Unlock();
			LaserDirectionBuffer->unmap();
		}
	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);
}

void UOptiXLaserComponent::PreparePatternChange(EPatternTypes Pattern)
{
	UE_LOG(LogTemp, Display, TEXT("OptiX Laser Component Preparing Laser Pattern Change"));

	if (Patterns[Pattern]->PlatformData->Mips.Num() != 0)
	{

		LaserIndices.Empty();
		LaserIndexColorMap.Empty();

		FTexture2DMipMap& IndexMip = Patterns[Pattern]->PlatformData->Mips[0];
		UE_LOG(LogTemp, Display, TEXT("Got Texture mips"));
		FColor* TextureData = static_cast<FColor*>(IndexMip.BulkData.Lock(LOCK_READ_WRITE));

		int X = Patterns[Pattern]->GetSizeX();
		int Y = Patterns[Pattern]->GetSizeY();

		for (int32 i = 0; i < X; ++i) {
			for (int32 j = 0; j < Y; ++j) {

				int32 TextureIndex = (X * Y - 1) - i * X - j;
				int32 BufferIndex = ((j)*(X)+i);

				if (TextureData[TextureIndex].R || TextureData[TextureIndex].G || TextureData[TextureIndex].B)
				{
					LaserIndices.Add(BufferIndex);
					LaserIndexColorMap.Add(TextureData[TextureIndex]);
				}
			}
		}
		IndexMip.BulkData.Unlock();
	}
}

EPatternTypes UOptiXLaserComponent::GetLaserPattern() const
{
	return CurrentLaserPattern;
}


void UOptiXLaserComponent::UpdateLaserPosition()
{
	//UE_LOG(LogTemp, Warning, TEXT("OptiX Laser Component Updating Position"));
	FTransform CurrentTransform = GetComponentTransform();
	FVector Translation = CurrentTransform.GetTranslation();
	FVector Forward = GetForwardVector();
	FVector Right = GetRightVector();
	FVector Up = GetUpVector();
	FMatrix Rotation = CurrentTransform.ToMatrixNoScale();
	Rotation = FMatrix::Identity;
	//UE_LOG(LogTemp, Display, TEXT("Transform on update: %s"), *GetComponentTransform().ToString());


	// Hard code this for now: laser is around 10x10x10 cube
	   	  
	OptiXContextUpdateFunction UpdateFunction =
		[Translation, Forward, Right, Up, Rotation](optix::Context Context)
	{
		Context["laser_origin"]->setFloat(Translation.X, Translation.Y, Translation.Z);
		Context["laser_forward"]->setFloat(Forward.X, Forward.Y, Forward.Z);
		Context["laser_right"]->setFloat(Right.X, Right.Y, Right.Z);
		Context["laser_up"]->setFloat(Up.X, Up.Y, Up.Z);

		Context["laser_rot"]->setMatrix4x4fv(true, &Rotation.M[0][0]);		
	};
	FOptiXModule::Get().GetOptiXContextManager()->EnqueueContextUpdateFunction(UpdateFunction);


	FOptiXModule::Get().GetOptiXContextManager()->OnSceneChangedDelegate.Broadcast();
}

