// Fill out your copyright notice in the Description page of Project Settings.

#include "OptiXTargetComponent.h"

#include "UObject/ConstructorHelpers.h"

#include "OptiXModule.h"


UOptiXTargetComponent::UOptiXTargetComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{	
	CurrentTexturePattern = ETexturePattern::TP_CHECKER;
	//FString N = "Texture2D'/OptiX/Targets/Checker.Checker'";
	//Texture = LoadObject<UTexture2D>(this, *N);

	static ConstructorHelpers::FObjectFinder<UTexture2D> TextureLoader(TEXT("Texture2D'/OptiX/Targets/Checker.Checker'"));
	Texture = TextureLoader.Object;

	if (Texture != NULL)
	{
		TextureSize = FIntPoint(Texture->GetSizeX(), Texture->GetSizeY());
		TargetSize = FVector(2.0f, 20.0f, 20.0f * static_cast<float>(TextureSize.Y) / static_cast<float>(TextureSize.X));
	}
}

// God sometimes unreal is annoying
void UOptiXTargetComponent::BeginPlay()
{
	//CurrentTexturePattern = ETexturePattern::TP_CIRCLES;

	FString N = "Texture2D'/OptiX/Targets/Checker.Checker'";
	if (CurrentTexturePattern == ETexturePattern::TP_BLACK)
	{
		N = "Texture2D'/OptiX/Targets/Black.Black'";
	}
	else if (CurrentTexturePattern == ETexturePattern::TP_CIRCLES)
	{
		N = "Texture2D'/OptiX/Targets/Circles.Circles'";
	}
	else if (CurrentTexturePattern == ETexturePattern::TP_GRID)
	{
		N = "Texture2D'/OptiX/Targets/Raster.Raster'";
	}
	Texture = nullptr;
	Texture = LoadObject<UTexture2D>(this, *N);
	TextureSize = FIntPoint(Texture->GetSizeX(), Texture->GetSizeY());
	TargetSize = FVector(2.0f, 20.0f, 20.0f * static_cast<float>(TextureSize.Y) / static_cast<float>(TextureSize.X));

	Super::BeginPlay();


	// Request creation of the optix objects
	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXObject(GetUniqueID(), "box_intersect");

	// Init them
	SetSize(TargetSize);

	// Create the required buffer and sampler
	FOptiXBufferData BufferData;
	BufferData.Name = "texture_buffer";
	BufferData.Type = RT_BUFFER_INPUT;
	BufferData.Format = RT_FORMAT_UNSIGNED_BYTE4;
	BufferData.BufferWidth = TextureSize.X;
	BufferData.BufferHeight = TextureSize.Y;
	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXBuffer(GetUniqueID(), BufferData);

	TextureSize = FIntPoint(Texture->GetSizeX(), Texture->GetSizeY());
	TargetSize = FVector(2.0f, 20.0f, 20.0f * static_cast<float>(TextureSize.Y) / static_cast<float>(TextureSize.X));

	FOptiXModule::Get().GetOptiXContextManager()->RequestNewOptiXTextureSampler(GetUniqueID());

	FMatrix Transform = GetComponentToWorld().ToMatrixNoScale();
	FMatrix Inverse = Transform.Inverse();

	OptiXObjectInitFunction InitFunction =
		[&TextureSize = TextureSize, &TargetSize = TargetSize, &Texture = Texture, Transform, Inverse]
	(FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler)
	{
		TextureSampler->setWrapMode(0, RT_WRAP_CLAMP_TO_EDGE);
		TextureSampler->setWrapMode(1, RT_WRAP_CLAMP_TO_EDGE);
		TextureSampler->setWrapMode(2, RT_WRAP_CLAMP_TO_EDGE);
		TextureSampler->setIndexingMode(RT_TEXTURE_INDEX_NORMALIZED_COORDINATES);
		TextureSampler->setReadMode(RT_TEXTURE_READ_NORMALIZED_FLOAT);
		TextureSampler->setMaxAnisotropy(1.0f);
		TextureSampler->setMipLevelCount(1u);
		TextureSampler->setArraySize(1u);

		UE_LOG(LogTemp, Display, TEXT("Texture with Size: (%i, %i)"), TextureSize.X, TextureSize.Y);

		optix::Buffer TextureBuffer = *Buffers->Find("texture_buffer");
		optix::uchar4* BufferData = static_cast<optix::uchar4*>(TextureBuffer->map());

		FTexture2DMipMap& Mip = Texture->PlatformData->Mips[0];

		FColor* TextureData = static_cast<FColor*>(Mip.BulkData.Lock(LOCK_READ_WRITE));

		// Texture index conversion is a real pain...
		for (int32 i = 0; i < TextureSize.X; ++i) {
			for (int32 j = 0; j < TextureSize.Y; ++j) {

				int32 TextureIndex = (TextureSize.X * TextureSize.Y - 1) - i * TextureSize.X - j;
				int32 BufferIndex = ((j)*(TextureSize.X) + i);
				//UE_LOG(LogTemp, Display, TEXT("Values: %i"), TextureData[BufferIndex]);

				BufferData[BufferIndex].x = TextureData[TextureIndex].R;
				BufferData[BufferIndex].y = TextureData[TextureIndex].G;
				BufferData[BufferIndex].z = TextureData[TextureIndex].B;
				BufferData[BufferIndex].w = TextureData[TextureIndex].A;

			}
		}

		// TODO: Check if we can copy automatically via Memcpy
		//FMemory::Memcpy(BufferData + (X * Y), TextureData, (X * Y * sizeof(FColor))); // Try copying the buffer data directly here TODO
		Mip.BulkData.Unlock();
		TextureBuffer->unmap();

		TextureSampler->setBuffer(0u, 0u, TextureBuffer);
		TextureSampler->setFilteringModes(RT_FILTER_NEAREST, RT_FILTER_NEAREST, RT_FILTER_NONE);

		Data->OptiXGeometryInstance["frameTexture"]->setTextureSampler(TextureSampler);

		Data->OptiXTransform->setMatrix(true, &Transform.M[0][0], &Inverse.M[0][0]);
		Data->OptiXAcceleration->markDirty();
		Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();

	};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueInitFunction(GetUniqueID(), InitFunction);

}

void UOptiXTargetComponent::SetSize(FVector NewSize)
{
	TargetSize = NewSize;
	OptiXObjectUpdateFunction UpdateFunction = 
		[&TargetSize = TargetSize](FOptiXObjectData* Data, TMap<FString, optix::Buffer>* Buffers, optix::TextureSampler TextureSampler) 
		{
			//OptiXGeometryInstance->SetFloat3DVector("size", TargetSize);
			Data->OptiXGeometryInstance["size"]->setFloat(TargetSize.X, TargetSize.Y, TargetSize.Z);
			Data->OptiXAcceleration->markDirty();
			Data->OptiXAcceleration->getContext()["top_object"]->getGroup()->getAcceleration()->markDirty();
		};

	FOptiXModule::Get().GetOptiXContextManager()->EnqueueUpdateFunction(GetUniqueID(), UpdateFunction);
}

FVector UOptiXTargetComponent::GetSize()
{
	return TargetSize;
}
