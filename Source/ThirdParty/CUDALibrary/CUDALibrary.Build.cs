// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class CUDALibrary : ModuleRules
{
	public CUDALibrary(ReadOnlyTargetRules Target) : base(Target)
	{
		Type = ModuleType.External;

        PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "include"));
        PublicSystemIncludePaths.Add(Path.Combine(ModuleDirectory, "include"));
        PublicSystemIncludePaths.Add(Path.Combine(ModuleDirectory, "common/inc"));

        if (Target.Platform == UnrealTargetPlatform.Win64)
        {


            // Add the import library
            PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "x64", "Release"));

            PublicAdditionalLibraries.Add("cublas.lib");
            PublicAdditionalLibraries.Add("cublasLt.lib");
            //PublicAdditionalLibraries.Add("cublas_device.lib");
            PublicAdditionalLibraries.Add("cuda.lib");
            PublicAdditionalLibraries.Add("cudadevrt.lib");
            PublicAdditionalLibraries.Add("cudart.lib");
            PublicAdditionalLibraries.Add("cudart_static.lib");
            PublicAdditionalLibraries.Add("cufft.lib");
            PublicAdditionalLibraries.Add("cufftw.lib");
            PublicAdditionalLibraries.Add("curand.lib");
            PublicAdditionalLibraries.Add("cusolver.lib");
            PublicAdditionalLibraries.Add("cusparse.lib");
            PublicAdditionalLibraries.Add("nppc.lib");
            PublicAdditionalLibraries.Add("nppial.lib");
            PublicAdditionalLibraries.Add("nppicc.lib");
            PublicAdditionalLibraries.Add("nppicom.lib");
            PublicAdditionalLibraries.Add("nppidei.lib");
            PublicAdditionalLibraries.Add("nppif.lib");
            PublicAdditionalLibraries.Add("nppig.lib");
            PublicAdditionalLibraries.Add("nppim.lib");
            PublicAdditionalLibraries.Add("nppist.lib");
            PublicAdditionalLibraries.Add("nppisu.lib");
            PublicAdditionalLibraries.Add("nppitc.lib");
            PublicAdditionalLibraries.Add("npps.lib");
            PublicAdditionalLibraries.Add("nvblas.lib");
            PublicAdditionalLibraries.Add("nvgraph.lib");
            PublicAdditionalLibraries.Add("nvml.lib");
            PublicAdditionalLibraries.Add("nvrtc.lib");
            PublicAdditionalLibraries.Add("OpenCL.lib");

            PublicDelayLoadDLLs.Add("cublas64_10.dll");
            PublicDelayLoadDLLs.Add("cublasLt64_10.dll");
            PublicDelayLoadDLLs.Add("cudart64_101.dll");
            PublicDelayLoadDLLs.Add("cufft64_10.dll");
            PublicDelayLoadDLLs.Add("cufftw64_10.dll");
            PublicDelayLoadDLLs.Add("cuinj64_101.dll");
            PublicDelayLoadDLLs.Add("curand64_10.dll");
            PublicDelayLoadDLLs.Add("cusolver64_10.dll");
            PublicDelayLoadDLLs.Add("cuparse64_10.dll");
            PublicDelayLoadDLLs.Add("nppc64_10.dll");
            PublicDelayLoadDLLs.Add("nppial64_10.dll");
            PublicDelayLoadDLLs.Add("nppicc64_10.dll");
            PublicDelayLoadDLLs.Add("nppicom64_10.dll");
            PublicDelayLoadDLLs.Add("nppidei64_10.dll");
            PublicDelayLoadDLLs.Add("nppif64_10.dll");
            PublicDelayLoadDLLs.Add("nppig64_10.dll");
            PublicDelayLoadDLLs.Add("nppim64_10.dll");
            PublicDelayLoadDLLs.Add("nppist64_10.dll");
            PublicDelayLoadDLLs.Add("nppisu64_10.dll");
            PublicDelayLoadDLLs.Add("nppitc64_10.dll");
            PublicDelayLoadDLLs.Add("npps64_10.dll");
            PublicDelayLoadDLLs.Add("nvblas64_10.dll");
            PublicDelayLoadDLLs.Add("nvgraph64_10.dll");
            PublicDelayLoadDLLs.Add("nvrtc64_101_0.dll");
            PublicDelayLoadDLLs.Add("nvrtc-builtins64_101.dll");
            // Delay-load the DLL, so we can load it from the right place first

            string BaseBinDir = ModuleDirectory + "/../../../Binaries/ThirdParty";


            RuntimeDependencies.Add(BaseBinDir + "/Win64/cudart64_101.dll");


        }
    }
}
